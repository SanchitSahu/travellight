<?php 
error_reporting(E_ALL); 
ini_set('display_errors', 'OFF');
session_start(); ?>
<!DOCTYPE html>
<html lang="nl">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Travel Light</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/font-awesome.css" rel="stylesheet">
        <link href="css/datepicker.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
        <link href="css/bootstrap-select.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/responsive.css" rel="stylesheet">
        <link href="js/intl-tel-input/css/intlTelInput.css" rel="stylesheet">
        <link rel="stylesheet" href="css/reset.css"> <!-- CSS reset -->
		<link rel="stylesheet" href="css/style.css"> <!-- Resource style -->
		<script src="js/jquery-2.1.1.js"></script>
		<script src="js/main.js"></script>
		
		<script src="js/modernizr.js"></script> <!-- Modernizr -->

        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATqi_rqW6F1pi1AGlai3M5y8HPUyItub8&callback=initMap"></script>

        <script type="text/javascript">
            var country_code = '<?php echo $_GET['countryList']; ?>';
            var productList = '<?php echo $_GET['productList']; ?>';
            var dpd1Headerval = '<?php echo $_GET['dpd1Header']?>';
            var SKU = '<?php echo $_REQUEST['productList'] ?>';
            var valTo = '<?php echo $_REQUEST['toContry'] ?>';
            var valFrom = '<?php echo $_REQUEST['fromContry'] ?>';

            var esimatecheckOut = '<?php echo $_REQUEST['esimatecheckOut'] ?>';
            var esimateBackon = '<?php echo $_REQUEST['esimateBackon'] ?>';
            var returnQuick = '<?php echo $_REQUEST['VerzendtypeQuick']?>';

            var SKU_drp = '<?php echo implode($_REQUEST['SKU_Drp'],",") ?>';
            var SKU_qnt = '<?php echo implode($_REQUEST['SKU_Qnt'],",") ?>';
            SKU_drp = SKU_drp.split(',');
            SKU_qnt = SKU_qnt.split(',');
            var associative_array = {};
            for(var k=0; k<SKU_drp.length; k++) {
                    associative_array[SKU_drp[k]] = SKU_qnt[k];
            }


            var directBooking = '<?php echo $_REQUEST['directBooking'] ?>';
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 15,
                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(52.111687, 4.282254), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType": "water", "elementType": "geometry.fill", "stylers": [{"color": "#d3d3d3"}]}, {"featureType": "transit", "stylers": [{"color": "#808080"}, {"visibility": "off"}]}, {"featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{"visibility": "on"}, {"color": "#b3b3b3"}]}, {"featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}]}, {"featureType": "road.local", "elementType": "geometry.fill", "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"weight": 1.8}]}, {"featureType": "road.local", "elementType": "geometry.stroke", "stylers": [{"color": "#d7d7d7"}]}, {"featureType": "poi", "elementType": "geometry.fill", "stylers": [{"visibility": "on"}, {"color": "#ebebeb"}]}, {"featureType": "administrative", "elementType": "geometry", "stylers": [{"color": "#a7a7a7"}]}, {"featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}]}, {"featureType": "road.arterial", "elementType": "geometry.fill", "stylers": [{"color": "#ffffff"}]}, {"featureType": "landscape", "elementType": "geometry.fill", "stylers": [{"visibility": "on"}, {"color": "#efefef"}]}, {"featureType": "road", "elementType": "labels.text.fill", "stylers": [{"color": "#696969"}]}, {"featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{"visibility": "on"}, {"color": "#737373"}]}, {"featureType": "poi", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {"featureType": "poi", "elementType": "labels", "stylers": [{"visibility": "off"}]}, {"featureType": "road.arterial", "elementType": "geometry.stroke", "stylers": [{"color": "#d6d6d6"}]}, {"featureType": "road", "elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {}, {"featureType": "poi", "elementType": "geometry.fill", "stylers": [{"color": "#dadada"}]}]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(52.111687, 4.282254),
                    map: map
                });
            }
        </script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Begin Cookie Consent -->
        <script type="text/javascript">
            window.cookieconsent_options = {"message":"Om je zo goed mogelijk van dienst te zijn, gebruikt Travel-light.nl cookies.","dismiss":"Sluiten","learnMore":"Voor meer informatie bekijk hier ons privacy & cookiebeleid.","link":"http://www.travel-light.nl/cookies","theme":"dark-bottom"};
        </script>

        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
        <!-- End Cookie Consent -->
    </head>
    <body class="index bgGray">

        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">
                        <img src="img/logo.png" alt="travelLight" class="logo">
                    </a>
                </div>        
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <!-- <ul class="nav navbar-nav navbar-right profile-top">
                      <li class="dropdown profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                          <div class="profile-content">
                            <div class="image">
                                <img src="img/avtar.png">
                            </div>
                            <h4>Alan van Wijk <i class="fa fa-chevron-down"></i></h4>
                          </div>
                        </a>
                        <ul class="dropdown-menu">
                          <li><a href="">Edit Profile</a></li>
                          <li><a href="">Settings</a></li>
                          <li><a href="">Notifications</a></li>
                          <li><a href="">Logout</a></li>
                        </ul>
                      </li>
                    </ul> -->
                    <ul class="nav navbar-nav navbar-right menu">
                        <li><a href="index.php">Home</a></li>
                        <li><a href="Hoewerkthet.php">HOE WERKT HET</a></li>
                        <li><a href="booking.php">BOEKEN</a></li> 
                        <li><a href="faq.php">FAQ</a></li> 
                        <li><a href="contact.php">CONTACT</a></li> 
                    </ul>
                </div>
            </div>
        </nav>

        <?php
        //echo basename($_SERVER['PHP_SELF']); exit;
        if (basename($_SERVER['PHP_SELF']) == "index.php") {
            ?>  
            <div class="topBanner">
                <div class="content">  
					<div class="container">
		                <section class="cd-intro">
							<h2 class="cd-headline rotate-1">
								<span>NOOIT MEER SLEPEN MET JE</span>
								<span class="cd-words-wrapper">
									<b class="is-visible">KOFFER</b>
									<b>FIETS</b>
									<b>SNOWBOARD</b>
									<b>SKI'S</b>
								</span>
							</h2>
							<h4>BESPAAR TOT <span>120 MINUTEN</span> EN <span>€ 300,-</span> PER REIS!</h4>
						</section>
					</div>
                </div>
            </div>
        <?php } ?>  

        <div id="sticky" class="topSearch">
            <div class="container">
                <div class="col-sm-3 first">
                    <div id="countryDropdown">
                    </div>
                    <span class="dpd1Class errorclass"></span>
                </div>
                <div class="col-sm-3 second calender">
                    <?php //$dateVal = if(isset($_GET['dpd1Header']))?>
                    <?php //echo (isset($_GET['dpd1Header']))? $_GET['dpd1Header'] : 'Wanneer vertrek je?'; ?>
                    <input type="text" class="form-control calender" placeholder="Wanneer vertrek je?"  value="Wanneer vertrek je" id="dpd1Header">
                    <span class="dateHeaderClass errorclass"></span>
                </div>
                <!--<div class="col-sm-3 third">
                  <input type="text" class="form-control" placeholder="met een fiets">
                </div>-->
                <div class="col-sm-3 third">
                    <div id="productDropDown">
                    </div>
                    <span class="productClass errorclass"></span>
                </div>    
                <div class="col-sm-3 fourth">
                    <a href="#" class="btn btn-md btn-block btnOrange quickBooking">BEREKEN</a>
                </div>
            </div>
        </div>
