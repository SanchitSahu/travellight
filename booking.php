<?php include 'header.php'; ?>

<?php //echo "<pre>";
//print_r($_REQUEST);//exit; ?>
<section class="booking">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h2 class="MdTitle">JE BAGAGE VERSTUREN IN 3 STAPPEN</h2>

                <div class="BookingSteps">
                    <a href="#" data-id="step_1" class="step one active">1 <span>Persoonlijke gegevens</span></a>
                    <a href="#" data-id="step_2" class="step two">2 <span>Reisdetails</span></a>
                    <a href="#" data-id="step_3" class="step three">3 <span>Controle & bevestigen</span></a>
                </div>
                <div class="BookingFields BookingFieldsMainDiv booking1">
                    <div class="BookingFields">
                        <form action="#" class="" method="post" id="contact_form1">    
                           <!--<p class="black">Ben je al klant van ons?</p>

                           <div class="form-group form-fields has-feedback">
                               <label>Klant</label>  
                               <select class="form-control" id="Klant1" name="Klant1">
                                   <option value="Nieuwe klant">Nieuwe klant</option>
                                   <option value="Bestaande klant">Bestaande klant</option>
                               </select>
                           <span class="glyphicon form-control-feedback" id="Klant11"></span>    
                           </div>-->

                            <h3 class="title"><span>1</span> PERSOONLIJKE GEGEVENS</h3>

                            <div class="form-group form-fields transparent has-feedback">
                                <label>Aanhef</label>
                                <div data-toggle="buttons">
                                    <div class="ansNum">
                                        <label class="radio-inline btn ans-radio">
                                            <input type="radio" name="options" id="option1" >
                                            <i class="fa fa-check"></i>                
                                        </label>
                                        <span>De heer</span>
                                    </div>                
                                    <div class="ansNum">
                                        <label class="radio-inline btn ans-radio">
                                            <input type="radio" name="options" id="option2" >
                                            <i class="fa fa-check"></i>                
                                        </label>
                                        <span>Mevrouw</span>
                                    </div>
                                </div>
                                <span class="glyphicon form-control-feedback" id="options21"></span>    
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Voorletters</label>  
                                <input type="text" class="form-control"  id="Voorletters" name="Voorletters">
                                <span class="glyphicon form-control-feedback" id="Voorletters1"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Tussenvoegsels</label>  
                                <input type="text" class="form-control"  id="Tussenvoegsels" name="Tussenvoegsels">
                                <span class="glyphicon form-control-feedback" id="Tussenvoegsels1"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Achternaam</label>  
                                <input type="text" class="form-control"  id="Achternaam" name="Achternaam">
                                <span class="glyphicon form-control-feedback" id="Achternaam1"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>E-mailadres</label>  
                                <input type="text" class="form-control"  id="email" name="email">
                                <span class="glyphicon form-control-feedback" id="email1"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Telefoonnummer</label>  
                                <div class="input-group">
                                    <!--<select name="countries" id="countries" title="Select your surfboard" class="selectpicker flagpicker">
                                        <option value="" data-thumbnail="img/flag-0.jpg">Select Country</option>                     
                                        <option value="Austria" data-thumbnail="img/flag/01.png">Austria</option>
                                        <option value="Belgium" data-thumbnail="img/flag/02.png">Belgium</option>
                                        <option value="France" data-thumbnail="img/flag/03.png">France</option>
                                        <option value="Germany" data-thumbnail="img/flag/04.png">Germany</option>
                                        <option value="Italy" data-thumbnail="img/flag/05.png">Italy</option>
                                        <option value="Luxembourg" data-thumbnail="img/flag/06.png">Luxembourg</option>
                                        <option value="Netherlands" data-thumbnail="img/flag/07.png">Netherlands</option>
                                        <option value="Portugal" data-thumbnail="img/flag/08.png">Portugal</option>
                                        <option value="Spain" data-thumbnail="img/flag/09.png">Spain</option>
                                        <option value="UK" data-thumbnail="img/flag/10.png">United Kingdom</option>
                                    </select>-->
                                    <input name="countries" class=""  id="countries" type="tel">

                                    
                                    <!--<input type="text" name="phonenumber" id="phonenumber" class="form-control" /></span>-->

                                </div>
                                <span class="glyphicon form-control-feedback" id="countries1"></span>
                                <!--<span class="glyphicon form-control-feedback" id="phonenumber1"></span> -->

                            </div>

                            <div class="clearfix form-group">
                                <div class="button-arrow"><input type="submit" class="" value ="NAAR STAP 2"></div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="BookingFields BookingFieldsMainDiv booking2" style="display:none;">    
                    <!--                    <div class="BookingSteps">
                                            <a href="" class="step one">1 <span>Persoonlijke gegevens</span></a>
                                            <a href="" class="step two active">2 <span>Financiele gegevens</span></a>
                                            <a href="" class="step three">3 <span>Controle & bevestigen</span></a>
                                        </div>    -->

                    <div class="BookingFields" >
                        <form action="#" class="" method="post" id="contact_form2">
                            <h3 class="title"><span>2</span> REIS DETAILS</h3>
                            <h3 class="title">Wat zijn de check-in en check-out data van je hotel?</h3>

                            <div class="form-group form-fields calender has-feedback">
                                <label>Check-in</label>  
                                <input type="text" class="form-control" value="<?php echo (isset($_REQUEST['esimatecheckOut'])) ? $_REQUEST['esimatecheckOut'] : date('d-m-Y'); ?>" id="dpd1" readonly="readonly">
                                <span class="glyphicon form-control-feedback" id="dpd11"></span>  
                            </div>
                            <div class="form-group form-fields calender has-feedback">
                                <label>Check-out</label>  
                                <input type="text" class="form-control" value="<?php echo (isset($_REQUEST['esimateBackon'])) ? $_REQUEST['esimateBackon'] : date('d-m-Y'); ?>" id="dpd2" readonly="readonly">
                                <span class="glyphicon form-control-feedback" id="dpd21"></span>  
                            </div>

                            <h3 class="title">Waar kunnen wij je bagage ophalen?</h3>

                            <div class="form-group form-fields has-feedback">
                                <label>Land</label>
                                <div id="countryDropdown_3">
                                </div>
                                <!--<select class="form-control custom" name="Klant">
                                  <option>Nederland</option>
                                  <option>Nederland</option>
                                  <option>Nederland</option>
                                  <option>Nederland</option>
                                  <option>Nederland</option>
                                </select>-->
                                <span class="glyphicon form-control-feedback" id="countryDropdown_31"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Postcode</label>  
                                <input type="text" class="form-control"  name="Postcode" id="Postcode">
                                <span class="glyphicon form-control-feedback" id="Postcode1"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Huisnummer</label>  
                                <input type="text" class="form-control" name="Huisnummer" id="Huisnummer">
                                <span class="glyphicon form-control-feedback" id="Huisnummer1"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Toevoeging huisnummer</label>  
                                <input type="text" class="form-control"  name="Toevoeging" id="Toevoeging">
                                <span class="glyphicon form-control-feedback" id="Toevoeging1"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Straat</label>  
                                <input type="text" class="form-control"  name="Straat" id="Straat">
                                <span class="glyphicon form-control-feedback" id="Straat1"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Woonplaats</label>  
                                <input type="text" class="form-control" name="Woonplaats" id="Woonplaats">
                                <span class="glyphicon form-control-feedback" id="Woonplaats1"></span>  
                            </div>

                            <h3 class="title">Wil je de bagage enkele reis versturen of retour?</h3>

                            <div class="form-group form-fields has-feedback">
                                <label>Verzendtype</label>  
                                <select class="form-control custom" id="Verzendtype" name="Verzendtype">
                                    <option value="1">Retour</option>
                                    <option value="0">Enkele reis</option>
                                </select>
                                <span class="glyphicon form-control-feedback"  id="Verzendtype1"></span>  
                            </div>

                            
                            <h3 class="title">Wat is het afleveradres voor je bagage?</h3>
							<!--<div class="form-group form-fields has-feedback">
                                <label>Adres</label>  
                                <input type="search" class="form-control" name="Addres" id="Addres">
                                <span class="glyphicon form-control-feedback" id="Addres1"></span>  
                            </div>-->

                            <div class="form-group form-fields has-feedback">
                                <label>Land</label>  
                                <div id="countryDropdown_2">
                                </div>
                                <!--<select class="form-control" name="Klant">
                                    <option>Zwitserland</option>
                                    <option>Zwitserland</option>
                                    <option>Zwitserland</option>
                                    <option>Zwitserland</option>
                                    <option>Zwitserland</option>
                                </select>-->
                                <span class="glyphicon form-control-feedback" id="countryDropdown_21"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Hotel</label>  
                                <input type="text" class="form-control"  name="Hotel_2" id="Hotel_2">
                                <span class="glyphicon form-control-feedback" id="Hotel_21"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Postcode</label>  
                                <input type="text" class="form-control"  name="Postcode_2" id="Postcode_2">
                                <span class="glyphicon form-control-feedback" id="Postcode_21"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Huisnummer</label>  
                                <input type="text" class="form-control" name="Huisnummer_2" id="Huisnummer_2">
                                <span class="glyphicon form-control-feedback" id="Huisnummer_21"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Toevoeging huisnummer</label>  
                                <input type="text" class="form-control"  name="Toevoeging_2" id="Toevoeging_2">
                                <span class="glyphicon form-control-feedback" id="Toevoeging_21"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Straat</label>  
                                <input type="text" class="form-control"  name="Straat_2" id="Straat_2">
                                <span class="glyphicon form-control-feedback" id="Straat_21"></span>  
                            </div>

                            <div class="form-group form-fields has-feedback">
                                <label>Woonplaats</label>  
                                <input type="text" class="form-control" name="Woonplaats_2" id="Woonplaats_2">
                                <span class="glyphicon form-control-feedback" id="Woonplaats_21"></span>  
                            </div>
					

							<h3 class="title">Wanneer kunnen wij je bagage ophalen en retourneren?</h3>
                            <div class="form-group form-fields has-feedback">
                                <label>Ophaaldatum</label>  
								<select class="form-control custom" name="dpd3" id="dpd3">
								</select>
                                <!--<input type="text" class="form-control" value="" name="dpd3" id="dpd3" readonly="readonly">-->
                                <span class="glyphicon form-control-feedback" id="dpd31"></span>  
                            </div>
                            <div class="form-group form-fields has-feedback returnDateClass" >
                                <label>Retourdatum</label>  
								<select class="form-control custom" name="dpd4" id="dpd4">
								</select>
                                <!--<input type="text" class="form-control" value="" name="dpd4" id="dpd4" readonly="readonly">-->
                                <span class="glyphicon form-control-feedback" id="dpd41"></span>  
                            </div>

                            <div class="clearfix form-group has-feedback">
                                <div data-toggle="buttons">
                                    <label class="checkbox-inline btn checkbox active">
                                        <input type="checkbox" name="options" id="option_2" >
                                        <i class="fa fa-check"></i>                
                                    </label>
                                    <span>Als ik niet thuis ben mag mijn bagage bij mijn buren worden geretourneerd</span>
                                </div>
                                <span class="glyphicon form-control-feedback" id="option_21"></span>  
                            </div>


                            <div class="clearfix form-group">
                                <div class="button-arrow"><input type="submit" class="booking2Cls" value="BEVESTIGEN EN BETALEN" ></div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="BookingFields BookingFieldsMainDiv booking3" style="display:none;">    
                    <!--<div class="BookingSteps">
                        <a href="" class="step one">1 <span>Persoonlijke gegevens</span></a>
                        <a href="" class="step two">2 <span>Financiele gegevens</span></a>
                        <a href="" class="step three active">3 <span>Controle & bevestigen</span></a>
                    </div>-->
                    <div class="BookingFields">
                        <form action="#" class="" method="post" id="contact_form3">
                            <h3 class="title"><span>3</span> BAGAGE DETAILS</h3>
                            <p class="black">Welk type bagage wil je versturen?</p>

                            <div class="row">

                                <div class="col-sm-6">

                                    <div class="row pl15">
                                        <div class="col-xs-7 form-group form-fields">
                                            <input type="text" class="form-control" value="Koffer" disabled>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="form-group form-fields">
                                              <!-- <input type="number" class="form-control" min="1" value="1"> -->
                                                <input  type='number' class="form-control" name="SKU_Qnt[]" id='Koffer_3' min="0" max="10" data-name="koffer"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="koffer1 commonContainer"></div>                  
                                    <div class='koffer1a commonSpecifics' style="display:none">                    
                                        <div class="form-group form-fields">
                                            <select class="form-control custom" name="SKU_Drp[]" data-name="koffer">
                                                <option value="SKU_1">Koffer 30kg</option>
                                                <option value="SKU_2">Koffer 20kg</option>
                                                <option value="SKU_8">Fiets in doos of fietskoffer</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row pl15">
                                        <div class="col-xs-7 form-group form-fields">
                                            <input type="text" class="form-control" value="Ski's/snowboard" disabled>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="form-group form-fields">
                                              <!-- <input type="number" class="form-control" min="1" value="1"> -->
                                                <input  type='number' class="form-control" name="SKU_Qnt[]" id='snowboard_3' min="0" max="10" data-name="Snowboard" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Snowboard1 commonContainer"></div>                  
                                    <div class='Snowboard1a commonSpecifics' style="display:none">                    
                                        <div class="form-group form-fields">
                                            <select class="form-control custom" name="SKU_Drp[]" id="SnowboardDrp" data-name="Snowboard">
                                                <option class="" value="SKU_6"> Snow dubbele set excl. schoenen</option>
                                                <option class="" value="SKU_7"> Snow dubbele set incl. schoenen</option>
                                                <option class="" value="SKU_5"> Snow enkele set incl. schoenen</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row pl15">
                                        <div class="col-xs-7 form-group form-fields">
                                            <input type="text" class="form-control" value="Golf Stand bag" disabled>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="form-group form-fields">
                                              <!-- <input type="number" class="form-control" min="1" value="1"> -->
                                                <input  type='number' class="form-control" name="SKU_Qnt[]" id='GolfFiets_3' min="0" max="10" data-name="GolfFiets"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="GolfFiets1 commonContainer"></div>                  
                                    <div class='GolfFiets1a commonSpecifics' style="display:none">                    
                                        <div class="form-group form-fields">
                                            <select class="form-control custom" name="SKU_Drp[]" data-name="GolfFiets">
                                                <option value="SKU_4">Golf Stand Bag</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row pl15">
                                        <div class="col-xs-7 form-group form-fields">
                                            <input type="text" class="form-control" value="Kitesurf" disabled>
                                        </div>
                                        <div class="col-xs-5">
                                            <div class="form-group form-fields">
                                              <!-- <input type="number" class="form-control" min="1" value="1"> -->
                                                <input  type='number' class="form-control" name="SKU_Qnt[]" id='Kitesurf_3' min="0" max="10" data-name="Kitesurf"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Kitesurf1 commonContainer"></div>                  
                                    <div class='Kitesurf1a commonSpecifics' style="display:none">                    
                                        <div class="form-group form-fields">
                                            <select class="form-control custom" name="SKU_Drp[]" data-name="Kitesurf">
                                                <option value="SKU_3">Kitesurf pakket</option>
                                            </select>
                                        </div>
                                    </div>



                                </div>


                                <div class="col-sm-6">
                                    <!--<div class="form-group form-fields has-feedback">
                                        <label></label>  
                                        <span class="form-control">Landentoeslag is € <b class="Surcharge"></b>,-</span>
                                    </div>-->
                                    <div class="summary">
                                        <p class="black">Prijsdetails</p>
                                        <ul class="priceUL">
                                            <li class="priceBoxKoffer"><ul class="koffer"></ul></li>
                                           <!--  <li class="priceBoxSnowboard"><ul class="Snowboard"></ul></li>
                                            <li class="priceBoxGolfFiets"><ul class="GolfFiets"></ul></li> 
                                            <li class="priceBoxKitesurf"><ul class="Kitesurf"></ul></li> -->
                                            
                                            <li class=''><span class='left'>Landentoeslag is </span><span class='right'><b class="Surcharge"></b> €</span></li>
                                            <li>      
                                                <span class="left">Totaal</span>
                                                <span class="right"><span class="finalPrice">0</span> €</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix form-group">
                                <div class="button-arrow"><input type="submit" class="booking3Cls" value="NAAR STAP 3" ></div>

                            </div>

                        </form>
                    </div>
                </div>    

                <div class="BookingFields BookingFieldsMainDiv booking4" style="display:none;">
                    <form action="#" class="" method="post" id="contact_form4">
                        <h3 class="title"><span>!</span> CONTROLE & BEVESTIGEN</h3>

                        <div class="row">              

                            <div class="col-sm-12">
                                <div class="summary">
                                    <p class="black">Prijsdetails</p>
                                    <ul class="priceUL">
                                            <li class="priceBoxKoffer"><ul class="koffer"></ul></li>
                                            <li>      
                                                <span class="left">Totaal</span>
                                                <span class="right"><span class="finalPrice">0 </span> €</span>
                                            </li>
                                        </ul>
                                    <div class="summaryText"></div>

                                </div>

                                <div class="col-sm-12">
	                                   <div data-toggle="buttons" class="form-group form-fields has-feedback">
                                        <label class="checkbox-inline btn checkbox">
                                            <input type="checkbox" name="option_4" id="option_4" >
                                            <i class="fa fa-check"></i>                
                                        </label>
                                        <span>Ja, ik accepteer de <a href="">algemene voorwaarden</a> en heb bovenstaande gegevens gecontroleerd.</span>
										  <span class="glyphicon form-control-feedback" id="option_41"></span>
                                    
									</div>
                                    <div data-toggle="buttons" class="form-group form-fields has-feedback">
                                        <label class="checkbox-inline btn checkbox">
                                            <input type="checkbox" name="option_5" id="option_5" >
											 
                                            <i class="fa fa-check"></i>                
                                        </label>
                                        <span>Ja, ik wil mij aanmelden voor de nieuwsbrief</span>
										<span class="glyphicon form-control-feedback" id="option_51"></span> 
                                    
									</div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix form-group">
                            <div class="button-arrow"><input type="submit" class="booking4Cls" value="NU BETALEN" ></div>
                        </div>


                    </form>
                </div>
            </div>

			<div class="BookingFields BookingFieldsMainDiv booking5" style="display:none;">    
                <div class="BookingFields">
                    <form action="#" class="" method="post" id="contact_form5">
		                <div class="row">
		                    <div class="col-sm-6">                 
		                        <div>                    
		                            <div class="form-group form-fields has-feedback">
                                                <label></label>  
		                                <div id="bankDiv"><select class="form-control custom" name="bankNames" id="bankNames" >
                                                    </select></div>
										<span class="glyphicon form-control-feedback" id="bankNames1"></span> 
		                            </div>
                                            
		                        </div>
		                    </div>
		                </div>

		                <div class="clearfix form-group">
		                    <div class="button-arrow"><input type="submit" class="booking5Cls" value="betalen" ></div>
		                </div>
		            </form>
                </div>
            </div> 

        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
            <div class="col-xs-6 client text-center">
                <a href="https://www.thuiswinkel.org/leden/travel-light-b.v/certificaat" target="_blank"><img alt="travellight" src="img/client-1.png" ></a>
            </div>
            <div class="col-xs-6 client text-center">
                <img alt="travellight" src="img/client-2.png" >
            </div>
        </div>
    </div>
</section>



<div id="productListCloneBooking" style="display:none;">
	<div class="col-xs-9 block">
		<select name="productListCloneBookingDrp" class="form-control cloneDrp custom">
			<option value="">Wat neem je mee?</option>
			<option class="" value="SKU_8"> Fiets in Koffer</option>
			<option class="" value="SKU_4"> Golf Stand Bag</option>
			<option class="" value="SKU_3"> Kitesurfing</option>
			<option class="" value="SKU_1"> Koffer groot Hard Case</option>
			<option class="" value="SKU_2"> Koffer Klein Hard Case</option>
			<option class="" value="SKU_6"> Snow 2 sets excl.schoen</option>
			<option class="" value="SKU_7"> Snow Dubbel incl. Schoenen</option>
			<option class="" value="SKU_5"> Snowboard enkel incl. Schoenen</option>
		</select>
	</div>
</div>

<?php include 'footer.php'; ?>

<?php /*
<script>
var placesAutocomplete = places({
    container: document.querySelector('#Addres')
  }); 
placesAutocomplete.on('change', function resultSelected(e) {
    document.querySelector('#Straat_2').value = e.suggestion.name || '';
    document.querySelector('#Woonplaats_2').value = e.suggestion.city || '';
	setCountry = e.suggestion.countryCode || '';
	document.querySelector('#Postcode_2').value = e.suggestion.postcode || '';

	$.ajax({
        url: "http://fox.fireeagle.nl:8092/countries",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='countryList2' name='countryList2' class='form-control custom'>";
            html += "<option value=''>Kies bestemming</option>";
            $.each(allData, function (index, value) {
				if(setCountry.toUpperCase()==value.code){
					html += "<option value='" + value.code + "' selected='selected'> " + value.name + "</option>";
				}else{
                	html += "<option value='" + value.code + "'> " + value.name + "</option>";
				}
            });
            html += "</select>";
            $("#countryDropdown_2").html(html);
        }
    });
  });	
</script>
*/?>