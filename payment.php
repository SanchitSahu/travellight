<?php include 'header.php'; 
error_reporting(E_ALL);  
ini_set("display_errors", "ON");
?>
<section class="payment">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h2 class="MdTitle">Payment</h2>
            </div>
			<div class="paymentFields">
				<form action="" class="" method="post" id="contact_form5">
                    <div class="row">
                        <div class="col-sm-6">                 
                            <div>                    
                                <div class="form-group form-fields">
                                    <select class="form-control" name="bankNames" id="bankNames" >
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix form-group">
                        <div class="button-arrow"><input type="submit" class="booking5Cls" value="Submit" ></div>
                    </div>
                </form>
			</div>			
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
            <div class="col-xs-6 client text-center">
                <img src="img/client-1.png" >
            </div>
            <div class="col-xs-6 client text-center">
                <img src="img/client-2.png" >
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>
