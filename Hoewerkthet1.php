<?php include 'header.php'; ?>

    
    <div class="videoWrapper">
        <iframe width="560" height="349" src="https://www.youtube.com/embed/wzFMzMgJ-SU?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=1" frameborder="0" allowfullscreen></iframe>
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
					<center><h1>Travel Light <small>Hoe werkt het?</small></h1></center>
			</div>
          
          <div class="col-sm-10 col-sm-offset-1">
            <p class="text-justify txtDrk"><p>Op onze homepagina kun je een prijsopgaaf doen. Je geeft aan waar je naartoe wilt en wat je mee wilt nemen. Nadat je op 'berekenen' klikt, kom je op onze boekings pagina waarin je alle gegevens kunt achterlaten. Wanneer je alles hebt ingevuld ben je klaar voor betaling en kan het genieten beginnen.</p>

<p>Kortom: wanneer je alle stappen doorloopt, wijst de rest vanzelf. Bij iedere stap wordt duidelijk uitgelegd hoe het precies werkt. Na betaling ontvang je van ons een e-mail met daarin je boekingsbevestiging.</p>
<p>VOOR VERTREK</p>
<p>Ongeveer een week voor vertrek sturen we je per post een welkomstpakket met daarin de vervoersdocumenten en alle benodigde informatie om je klaar te maken voor vertrek. Ga je skiën, snowboarden of kitesurfen? Dan krijg je van ons naast een doos ook een inpak zak. Deze beschermt je spullen en onze doos. Want als jouw natte spulletjes een paar uur knuffelen met onze doos, wordt ze flink nat en blijft er maar weinig van dat recyclebare karton over. Koffers gaan overigens mee zonder doos.</p>
<p>BAGAGE WORDT AFGEHAALD</p><br>
<p>Als je alles ingepakt klaar hebt staan en goed hebt vast hebt getapet (liefst ook een paar keer rondom de doos) plak je de stickers en etiketten op de doos. Een TNT chauffeur komt hem op de afgesproken tijd halen.</p>
<p>ONDERWEG</p><br>
<p>Na het ophalen volgen wij te allen tijde jouw bagage. We kunnen je precies vertellen waar je bagage is. Kan je nou niet slapen van de spanning, dan kun je ook zelf de bagage volgen via Track & Trace. Het 10 cijferige nummer onder de barcode is je track & trace nummer en kan je via tnt.com zelf ook volgen. Wij doen er alles aan om jouw bagage uiterlijk één werkdag voor je aankomst al af te leveren in het hotel.</p>
<p>VOLLEDIGE GEGEVENS</p><br>
<p>Wij nemen contact op met het door jou opgegeven hotel, zodat zij de zending netjes aannemen, al vinden we het wel heel erg fijn als jij dat ook doet tijdens het boeken. Hoe vollediger jij de gegevens van je bestemming hebt ingevuld, hoe beter wij ons werk kunnen doen en hoe ontspannender jouw vakantie begint.  </p>
          

           
          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    
    
  <?php include 'footer.php'; ?>