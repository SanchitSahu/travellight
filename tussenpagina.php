<?php
include 'header.php';
error_reporting(E_ALL);
ini_set("display_errors", "ON");
?>

<div class="tussenpagina" id=""> <!--sticky-price-->
    <div class="content">
        <div class="container">
            <div class="calculation">
                <h2>ZELF SLEPEN EN WACHTEN IS NIET VOORDELIGER!</h2>
                <div class="bubble" >
                    <span>€ <b class="finalPrice"> <?php echo $_GET['price'] ?></b>,-</span>
                </div> 
            </div>  
        </div>
    </div>
</div>


<section>
    <div class="container mt40">
        <div class="row">
            <div class="page-header">
                <center><h1>Prijsopgave <small>Bereken vrijblijvend de kosten</small></h1></center>
            </div>
            <p class="text-center md--text">
                Bereken snel de kosten voor het versturen van al jouw bagage<br/>
                met onze handige rekenhulp. Overtuigd van het aanbod, boek<br/>
                dan direct of verstuur de prijsopgave per e-mail. 
            </p>
        </div>
        <form method="get" id="estimatePage" action="booking.php"><!--action="booking.php"-->
            <div class="row mt40">
                <div class="col-sm-6">
                    <div class="form-group form-fields has-feedback">
                        <label>Verzendtype</label>  
                        <select class="form-control custom" id="VerzendtypeQuick" name="VerzendtypeQuick">
                            <option value="1">Retour</option>
                            <option value="0">Enkele reis</option>
                        </select>
                        <!--<span class="glyphicon form-control-feedback"  id="VerzendtypeQuick1"></span>  -->
                    </div>

                    <div class="form-group form-fields calender has-feedback">
                        <label>Vertrek op</label>  
                        <input type="text" class="form-control" value="<?php echo $_GET['dpd1Header'] ?>" name="esimatecheckOut" id="esimatecheckOut" readonly="readonly">
                        <!--<span class="" id="esimatecheckOut1"></span>  -->
                    </div>

                    <div class="form-group form-fields calender has-feedback returnDateClass1">
                        <label>Terug op</label>  
                        <input type="text" class="form-control"  name="esimateBackon" id="esimateBackon" readonly="readonly">
                        <!--<span class="glyphicon form-control-feedback" id="esimateBackon1"></span>  -->
                    </div>

                    <div class="form-group form-fields has-feedback">
                        <label>Van</label>  
                        <div id="fromContryDiv"></div> 
                        <!--<span class="glyphicon form-control-feedback" id="fromContry1"></span>-->
                    </div>

                    <div class="form-group form-fields has-feedback">
                        <label>Naar</label>  
                        <div id="toContryDiv"></div> 
                        <!--<span class="glyphicon form-control-feedback" id="toContry1"></span>  -->
                    </div>
                    <input type="hidden" name="directBooking" value="1">



                    <!--<div class="form-group form-fields">
                        <label>Van</label>  
                        <input type="text" class="form-control" name="Voorletters" value="Nederland">
                    </div>

                    <div class="form-group form-fields">
                        <label>Naar</label>  
                        <input type="text" class="form-control" name="Voorletters" value="Zwitserland">
                    </div>-->

                </div>

                <div class="col-sm-6">            
                    <!--<div class="clearfix">
                      <div class="row pl15">
                        <div class="col-xs-7 form-group form-fields">
                          <input type="text" class="form-control" name="Voorletters" value="Koffer">
                        </div>
                        <div class="col-xs-5">
                          <div class="form-group form-fields">
                            <input type="number" class="form-control" min="1" value="1">
                          </div>
                        </div>
                      </div>
                    </div>-->

                    <div class="form-group form-fields has-feedback">
                        <label></label>  
                        <span class="form-control SurchargeClass">Landentoeslag is € <b class="Surcharge"> <?php echo ($_REQUEST['Surcharge'] == 0) ? "is gratis" : $_REQUEST['Surcharge'] ?></b>,-</span>
                    </div>



                    <div class="addProducts"></div>
                    <div class="clear-fix"></div>
                    <div class="">
                        <div id="ProductEst" class="form-group form-fields form-field-nolabel"> </div>
                    </div>  


                    <div class="col-sm-6" style="display:none">
                        <div class="summary">
                            <p class="black">Prijsdetails</p>
                            <ul class="priceUL">
                                <li class="priceBoxKoffer"><ul class="kofferUL"></ul></li>
                                <li class="priceBoxSnowboard"><ul class="SnowboardUL"></ul></li>
                                <li class="priceBoxGolfFiets"><ul class="GolfFietsUL"></ul></li> 
                                <li class="priceBoxKitesurf"><ul class="KitesurfUL"></ul></li>
                                <li>      
                                    <span class="left">Totaal</span>
                                    <span class="right"><span class="finalPrice">0 </span> €</span>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>

                <div class="col-sm-12 text-center mt40">
                    <p class="md--text">TIP : Selecteer extra opties zoals het meesturen van je koffer!</p>

                    <a href="#" class="btn border-btn size-default mt20 " data-toggle="modal" data-target="#myModal">E-MAIL PRIJSOPGAVE</a>
                    <input type="submit" class="btn btn-orange size-default mt20" value="DIRECT BOEKEN">
                </div>          
            </div>
        </form>
    </div>
</section>







<section class="pb80">
    <div class="container">
        <div class="row">
            <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
            <div class="col-xs-6 client text-center">
                <a href="https://www.thuiswinkel.org/leden/travel-light-b.v/certificaat" target="_blank"><img src="img/client-1.png" ></a>
            </div>
            <div class="col-xs-6 client text-center">
                <img src="img/client-2.png">
            </div>
        </div>
    </div>
</section>



<section class="testimonial">
    <div class="container">
        <div class="row">

            <div class="col-sm-10 col-sm-offset-1">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">

                        <div class="item active">
                            <div class="col-xs-3 image">
                                <a href="http://app.12feedback.com/86/travel-light" target="_blank"><img src="img/Review_2.png"></a>
                            </div>
                            <div class="col-xs-9 text">
                                <h3>ONZE TEVREDEN KLANTEN AAN HET WOORD</h3>
                                <a href="http://app.12feedback.com/86/travel-light" target="_blank"><p>Dankzij de tussenkomst van Travel Light kon onze fietsreis met ons dochtertje van zuid Italië naar huis gewoon doorgaan. Halverwege, bij het befaamde Gardameer, werden 1 juni jl. onze fietsen in no-time gestolen. Ondanks de sloten en de drukke toeristische wandelboulevard. Heel even leek dit het einde van ons avontuur van 4,5 maand, waarvan we pas op de helft waren. Gelukkig had een goede vriend 2 oude toerfietsen in de schuur staan en kende hij Travel Light. De fietsen werden snel afgestoft, ingepakt en vervolgens aan huis opgehaald door Travel Light. 3 dagen later werden ze bij ons aan de tent aan het Gardameer afgeleverd. ‘</p></a>
                                <span>Geert de Letter</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-3 image">
                                <a href="http://app.12feedback.com/86/travel-light" target="_blank"><img src="img/Review_3.png"></a>
                            </div>
                            <div class="col-xs-9 text">
                                <h3>ONZE TEVREDEN KLANTEN AAN HET WOORD</h3>
                                <a href="http://app.12feedback.com/86/travel-light" target="_blank"><p>‘Wat relaxed reizen met enkel handbagage! Alles keurig op tijd geleverd en zeer duidelijke en snelle communicatie met het team van Travel Light. We gaan hier zeker vaker gebruik van maken!!! Bedankt en succes.’</p></a>
                                <span>Wendy Goedel de Vos</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-3 image">
                                <a href="http://app.12feedback.com/86/travel-light" target="_blank"> <img src="img/Review_1.png"></a>
                            </div>
                            <div class="col-xs-9 text">
                                <h3>ONZE TEVREDEN KLANTEN AAN HET WOORD</h3>
                                <a href="http://app.12feedback.com/86/travel-light" target="_blank"><p>‘Gemak dient de mens’ In 2015 deed atleet Coen Stegeman mee aan de IRONMAN 70.3 Barcelona. Mind you, dat is achtereenvolgens 1,9 km zwemmen, 90 km fietsen en 21,1 km hardlopen. Hij deed er 6,5 uur over en planned er dit jaar 5,5 uur over te doen. Een topprestatie als je het ons vraagt. Hij maakte gebruik van Travel Light om zijn fiets naar Barcelona te versturen.
                                        Coen: ‘Travel Light is een mooi concept. Je hebt gewoon geen gedoe van het zelf meesjouwen van je fiets van en naar de luchthaven, waardoor je je volledig kunt focussen op waar je voor komt: het leveren van een topprestatie.’</p></a>
                                <span>Coen Stegeman</span>
                            </div>
                        </div>

                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>

            </div>

        </div>
    </div>
</section>



<section>
    <div class="container">
        <div class="row">
            <h2 class="MdTitle text-black">VERGELIJK JOUW VLIEGTUIGMAATSCHAPPIJ EN ZIE HOEVEEL JE BESPAART!</h2>

            <div class="col-sm-8 col-sm-offset-2">
                <div class="table-responsive oCompany">
                    <table class="table">
                        <tr>
                            <td>
                                <img src="img/oCompany1.png">
                            </td>
                            <td>
                                <h4>koffer 20kg</h4>
                                <h4>koffer 30kg</h4>
                            </td>
                            <td>
                                <h4>€ 64,- retour</h4>
                                <h4>€ 64,- retour</h4>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="img/oCompany2.png">
                            </td>
                            <td>
                                <h4>koffer 20kg</h4>
                                <h4>koffer 30kg</h4>
                            </td>
                            <td>
                                <h4>€ 100,- retour</h4>
                                <h4>€ 130,- retour</h4>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="position: sticky;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Afsluiten</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Stuur jezelf of een bekende de prijsopgave
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form role="form" id="emailForm" method="post">
                    <div class="form-group">
                        <!-- <label for="exampleInputEmail1">Vul hieronder je e-mailadres in</label> -->
                        <input type="email" class="form-control" id="emailId" name="emailId" placeholder="Vul hier het e-mailadres in" required/>
                        <span class="glyphicon form-control-feedback" id="emailId1"></span>  
                    </div>

                    <button type="submit" class="btn btn-orange size-default mt20">Versturen</button>
                </form>


            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                            Close
                </button>-->
            </div>
        </div>
    </div>
</div>
<div id="productListClone" style="display:none;">
    <div class="col-xs-9 block form-group form-fields form-field-nolabel">
        <select name="productListCloneDrp" class="form-control cloneDrp custom" readonly>
            <option value="" disabled="disabled">Selecteer andere bagage om te versturen</option>
            <option class="" disabled="disabled" value="SKU_8"> Fiets in doos of fietskoffer</option>
            <option class="" disabled="disabled" value="SKU_4"> Golf Stand Bag</option>
            <option class="" disabled="disabled" value="SKU_3"> Kitesurf pakket</option>
            <option class="" disabled="disabled" value="SKU_1"> Koffer 30kg</option>
            <option class="" disabled="disabled" value="SKU_2"> Koffer 20kg</option>
            <option class="" disabled="disabled" value="SKU_6"> Snow dubbele set excl. schoenen</option>
            <option class="" disabled="disabled" value="SKU_7"> Snow dubbele set incl. schoenen</option>
            <option class="" disabled="disabled" value="SKU_5"> Snow enkele set incl. schoenen</option>
        </select>
    </div>
    <div class="col-xs-3 padding-right-0">
        <div class="form-group form-fields form-field-nolabel"><input type="number" class="form-control inputFieldNumber" min="0" max="10" value="1"></div>
    </div>
</div>

<div id="productListClone1" style="display:none;">
    <div class="col-xs-9 block form-group form-fields form-field-nolabel">
        <input name='productListCloneDrp' class="form-control cloneDrp" type=text list="productsSKU">
        <datalist id="productsSKU">
            <option value="" disabled="disabled">Selecteer andere bagage om te versturen</option>
            <option class="" disabled="disabled" value="SKU_8"> Fiets in doos of fietskoffer</option>
            <option class="" disabled="disabled" value="SKU_4"> Golf Stand Bag</option>
            <option class="" disabled="disabled" value="SKU_3"> Kitesurf pakket</option>
            <option class="" disabled="disabled" value="SKU_1"> Koffer 30kg</option>
            <option class="" disabled="disabled" value="SKU_2"> Koffer 20kg</option>
            <option class="" disabled="disabled" value="SKU_6"> Snow dubbele set excl. schoenen</option>
            <option class="" disabled="disabled" value="SKU_7"> Snow dubbele set incl. schoenen</option>
            <option class="" disabled="disabled" value="SKU_5"> Snow enkele set incl. schoenen</option>
        </datalist>
    </div>    
    <div class="col-xs-3 padding-right-0">
        <div class="form-group form-fields form-field-nolabel"><input type="number" class="form-control inputFieldNumber" min="0" max="10" value="1"></div>
    </div>

</div>    
<!--<div id="productListClone" style="display:none;">
    <div class="col-xs-9 block padding-right-0">
        <div class="form-group form-fields form-field-nolabel">
            <input id="productListCloneDrp" class="form-control cloneDrp" data-sku-name='' type="text" readonly="readonly" name="productListCloneDrp">
        </div>    
    </div>
    
    <div class="col-xs-3 padding-right-0">
        <div class="form-group form-fields form-field-nolabel"><input type="number" class="form-control inputFieldNumber" min="0" max="10" value="1"></div>
    </div>
</div>-->

<?php include 'footer.php'; ?>
