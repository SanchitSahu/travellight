<?php include 'header.php'; ?>


    <section>
      <div class="container">
        <div class="row">
          
          

          <div id="main" class="clearfix">
            <div class="wf">
              <div class="col-sm-12">
                <div class="page-header">
                    <h1 id="privacyH1">Travel Light <small>Privacyverklaringen</small></h1>
			</div>
                <h4 id="algemene-privacyverklaring">Algemene Privacyverklaring</h4>
                <p class="text-justify">Travel Light streeft ernaar diensten van een hoge kwaliteit te leveren. Hierbij is betrouwbaarheid een belangrijke factor. Respect voor de privacy van onze klanten en een zorgvuldige omgang met persoonsgegevens zien wij als belangrijk onderdeel van de betrouwbaarheid van onze diensten. Hierna vind je algemene informatie over hoe wij met jouw gegevens omgaan. Waar nodig vind je per dienst of product een aanvullende privacyverklaring waarin meer specifiek wordt aangegeven welke gegevens worden vastgelegd en waarvoor deze worden gebruikt.</p>
               <br>
                <h4 id="gebruik-aanmeldingsgegevens">Gebruik aanmeldingsgegevens</h4>
          <p class="text-justify">Om gebruik te kunnen maken van onze producten en diensten moet je meestal een aanmeldingsprocedure doorlopen, waarbij gevraagd wordt naar een aantal gegevens, zoals naam, adres, woonplaats, postcode, geslacht, e-mailadres en bankrekening nummer. De aanmeldingsgegevens worden in de eerste plaats gebruikt om (in het kader van onze dienstverlening) informatie te kunnen toesturen. Ook willen wij je graag op de hoogte houden van nieuwe ontwikkelingen of interessante aanbiedingen (zie ook Actuele informatie).

          </p>
          <p class="text-justify">
                  <br>
                  <h4 id="e-mailadres">E-mailadres</h4>
          Jouw e-mailadres wordt in de eerste plaats gebruikt om de opdracht te kunnen bevestigen en eventueel voor klachtafhandeling. Wanneer je op de website aangeeft dat je graag informatie ontvangt, gebruiken wij je e-mailadres om met jou te kunnen communiceren. Tevens word je op de hoogte te houden van nieuwe ontwikkelingen, producten en diensten van Travel Light en (gerelateerde) bedrijven.&nbsp;
          </p>
                  <p class="text-justify">Indien je deze informatie niet meer wilt ontvangen kun je je hier 
                    <a href="/contact">afmelden</a>.
                  </p>
                 
                    <br>
                    
                    <h4 id="beveiliging">Beveiliging</h4>
         <p class="text-justify"> Travel Light besteedt vanwege de aard van haar dienstverlening en het vertrouwen dat men in haar dienstverlening moet kunnen stellen veel zorg aan technische en organisatorische vormen van beveiliging van jouw gegevens.
         </p>  <br>
                        <h4 id="analyse-van-gegevens-en-cookies">Analyse van gegevens en cookies</h4>
                        <p class="text-justify">
          De gegevens die ontstaan bij het aanklikken van informatie op onze websites of door uitwisseling van informatie tussen de website en jouw computer (door middel van bepaalde tekstbestandjes, zgn. cookies) geven ons informatie die van belang is voor de verbetering en ontwikkeling van onze dienstverlening, zoals ook het optimaliseren van onze website. Deze informatie ontlenen wij aan de analysering van deze gegevens die overzichten e.d. opleveren die niet in verband staan met individuele gebruikers. Jouw persoonlijk gebruik kan daaruit niet worden afgeleid.
                        </p>
                        <p class="text-justify">
                        
          Cookies worden ook gebruikt om gebruikersnamen, wachtwoorden en persoonlijke instellingen te bewaren. Bij een volgend bezoek hoeft u dan niet opnieuw uw gegevens in te voeren. De meeste internetbrowsers bieden u de mogelijkheid om het plaatsen van cookies te blokkeren of om u vooraf te laten melden wanneer er een cookie wordt geplaatst. Het uitzetten van cookies kan het gebruik van onze website en diensten beperken. Meer informatie over cookies kunt u vinden op www.cookiecentral.com/faq of op http://www.microsoft.com/info/nl/cookies.mspx.
                        </p><br>
                              <h4 id="ontvangers-van-gegevens">Ontvangers van gegevens</h4>
                              <p class="text-justify">
          Jouw gegevens die door Travel Light worden verzameld en gebruikt, zijn toegankelijk voor de medewerkers van Travel Light, die de gegevens nodig hebben bij de uitoefening van zijn of haar functie. Er zijn in beginsel geen externe partijen die de gegevens ontvangen. Het kan voorkomen dat op grond van een wettelijke verplichting of een geschil incidenteel de gegevens aan externe partijen ter beschikking gesteld moeten worden.
                              </p>
                              <p class="text-justify">
          In het kader van een fusie of overname of van faillissement of surseance van betaling kan het nodig zijn dat derden toegang krijgen tot de gegevens of de gegevens ter beschikking krijgen gesteld.
                              </p><br>
                                    <h4 id="actuele-informatie">Actuele informatie</h4>
                                    <p class="text-justify">
          Wij houden je graag op de hoogte van de laatste ontwikkelingen en diensten van Travel Light en (gerelateerde) bedrijven. Jouw gegevens zijn hier voor nodig. Indien je deze informatie niet meer wilt ontvangen of niet wilt dat de daarvoor benodigde communicatiegegevens aan andere (gerelateerde) bedrijven worden verstrekt, kun je je hier 
                                    <a href="/contact">afmelden</a>.
                                    </p><br>
                                        <h4 id="wijzigingen">Wijzigingen</h4>
                                        <p class="text-justify">
          Belangrijke wijzigingen in deze privacyverklaring worden op deze website bekend gemaakt.
                                        </p><br>
                                            <h4 id="vragen">Vragen</h4>
                                            <p class="text-justify">
          Voor vragen over deze verklaring of over de producten en diensten kun je per e-mail contact opnemen met de afdeling Travel Light Klantenservice via het 
                                            <a href="/contact">reactieformulier</a>.&nbsp;
                                            </p>
                                            <p class="text-justify">Je kunt de Travel Light Klantenservice ook bellen op 085-4874344 van maandag t/m vrijdag van 08.30 - 18.00 uur.
                                              <br>
                                              </p>
                                              <p>&nbsp;</p>
                                              <p>&nbsp;</p>
                                              <!--end-->
                                            </div>
                                            <div class="right">&nbsp;</div>
                                          </div>
                                        </div>
        


        </div>
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
              <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
              <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>



<?php include 'footer.php'; ?>