<section>
    <div class="container">
      <div class="row">
        
        <div class="hr">
            <img alt="travelLight" src="img/hrIcon.jpg">
        </div>

        <h2 class="md--text text-center"><i class="fa fa-check green"></i> Reis zonder bagage en bespaar op je totale reiskosten! </h2>
        <h2 class="md--text text-center"><i class="fa fa-check green"></i> Neem de trein en bespaar op lang parkeren</h2>
        <h2 class="md--text text-center"><i class="fa fa-check green"></i> Huur een kleinere huurauto en bespaar tot € 200,- per week!</h2>
        
      </div>
    </div>
    </section>

    

<div class="bgGrey bottom-nav text-center">
      <ul class="list-inline list-unstyled">
        <li><a href="index.php">Home</a></li>
        <li><a href="howItWorks.php">Hoe werkt het</a></li>
        <li><a href="booking.php">Boeken</a></li>
        <li><a href="AlgemeneVoorwaarden.php">Algemene voorwaarden</a></li>
        <li><a href="privacy-policy.php">Privacy policy</a></li>
        <!--<li><a href="Blog.php">Blog</a></li>-->
        <li><a href="contact.php">Contact</a></li>
      </ul>
    </div>
    



<footer>
  <div class="container">
      
      <div class="row footerLinks">
        <div class="col-sm-3 col-xs-12">
          <h5>Soorten bagage</h5>
          <ul>
              <li><a href="Koffer.php">Koffer</a></li>
              <li><a href="Fiets.php">Fiets</a></li>
              <li><a href="Golf.php">Golf</a></li>
              <li><a href="Kitesurf.php">Kitesurf</a></li>
              <li><a href="Snowboard.php">Ski's & snowboard</a></li>
              <li><a href="OverigeBagage.php">Overige bagage</a></li>
          </ul>
        </div>
        <div class="col-sm-3 col-xs-12">
          <h5>Bagage bestemmingen</h5>
          <ul>
            <li><a href="Frankrijk.php">Frankrijk</a></li>
            <li><a href="Italie.php">Italie</a></li>
            <li><a href="Spanje.php">Spanje</a></li>
            <li><a href="Oostenrijk.php">Oostenrijk</a></li>
            <li><a href="Portugal.php">Portugal</a></li>
            <li><a href="Belgie.php">België</a></li>
            <li><a href="Verenigd-Koninkrijk.php">Verenigd Koninkrijk</a></li>
            <li><a href="Duitsland.php">Duitsland</a></li>
            <li><a href="Luxemburg.php">Luxemburg</a></li>
          </ul>
        </div>
        <div class="col-sm-3 col-xs-12">
          <h5>Klantenservice</h5>
          <ul>
              <li><a href="contact.php">Contact</a></li>
              <li><a href="faq.php">Veelgestelde vragen</a></li>
              <li><a href="Over_Travel-light.php">Over Travel-light</a></li>
          </ul>
        </div>
        <div class="col-sm-3 col-xs-12">
          <h5>Travel light</h5>
          <ul>
            <li><a>info@travel-light.nl</a></li>
            <li>085 - 4874344</li>
            <li>Gevers Deynootweg 83</li>
            <li>2586BK Scheveningen</li>
          </ul>
        </div>
        
      </div>
	  	
	  	<div class="row">
          <h4 class="MdSocial">Volg ons op social media</h4>
        </div>


      <div class="col-md-12 text-center links-box">         
          
          <!--Social Links-->
          <div class="social-links">
              <a target="facebook" href="https://www.facebook.com/Ilove2travellight"><i class="fa fa-facebook"></i></a>
              <a target="twitter" href="https://twitter.com/Travellight_nl"><i class="fa fa-twitter"></i></a>
              <a target="google-plus" href="https://plus.google.com/+TravellightNlLoves2travel"><i class="fa fa-google-plus"></i></a>
              <a target="instagram" href="https://www.instagram.com/welove2travellight/"><i class="fa fa-instagram"></i></a>
          </div>
      </div>
  </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
     <script src="js/jquery.validate.min.js"></script>
     <script src="js/blockUI.js"></script>
	<script src="js/bootbox.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/intl-tel-input/js/intlTelInput.js"></script>
    <script src="js/intl-tel-input/js/utils.js"></script>
	<script src="https://cdn.jsdelivr.net/places.js/1/places.min.js"></script>




    <script src='js/bootstrapvalidator.min.js'></script>
    <script src="js/custom.js"></script>


    <script type="text/javascript">
      $(window).scroll(function() {
          var nav = $('.navbar');
          var top = 200;
          if ($(window).scrollTop() >= top) {

              nav.addClass('fixed');

          } else {
              nav.removeClass('fixed');
          }
      });
    </script>

    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script>
      if ( $(window).width() > 768) {
        $(window).load(function(){
            $("#sticky").sticky({ topSpacing: 64 });
            $("#sticky-price").sticky({ topSpacing: -150 });
        });
      }
    </script>

    <script src="js/bootstrap-select.js"></script>


    <script type="text/javascript">
    $(function(){
        $(".col-sm-6 #numberOfLocations").change(function(){
            var value = $(this).val();
            $(".col-sm-6 .blockContainer").empty();
            for(var i = 0; i<value; i++){
                var block = $("<div>",{'class':"block"});
                $(block).append($(".col-sm-6 .businessSpecifics").html());
                $(".col-sm-6 .blockContainer").append(block);
            }
        }); 
      
    });
     
     
    /*$(".booking1Cls").on('click', function() {
        $(".booking2").fadeIn();
        $(".booking1").fadeOut();
     });


    $(".booking2Cls").on('click', function() {
        $(".booking3").fadeIn();
        $(".booking2").fadeOut();
     });


    $(".booking3Cls").on('click', function() {
        $(".booking4").fadeIn();
        $(".booking3").fadeOut();
     }); */


    /*$("form").submit(function(){
        //alert("Submitted");
        alert( $('#Klant_1').val() );
    });*/
    </script>


  </body>
</html>
