<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/frankrijk2.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
           <div class="page-header">
               <h1 class="pageH1">Frankrijk <small>Een veelzijdig land voor reizigers.</small></h1>
			</div>
          
          <div class="col-sm-10 col-sm-offset-1">
        <blockquote>   	        	
        Gemiddelde levertijd vaste land: 					4 werkdagen		<br>
		Gemiddelde levertijd Corsica: 						5 werkdagen 	<br>
        </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           Frankrijk is een land van veelzijdigheid voor de reiziger. De verschillen in klimaat & het feit dat meer dan 25% van het land bestaat uit bosgebied, zorgen ervoor dat er voor elke outdoor activiteit wel ergens een goed plekje te vinden is. Zo zijn er een heleboel prachtige nationale parken om te wandelen, fietsen, kamperen en te kajakken en kunnen surfers en kiters zich uitleven in het zuiden van het land. Daarnaast zijn er natuurlijk ook nog genoeg plekken voor wintersport liefhebbers; de Franse Alpen en de Pyreneeën bijvoorbeeld. Frankrijk biedt ook meer dan genoeg voor echte cultuurliefhebbers; historische steden, kastelen, burchten, kunst & bijzondere architectuur. <br>

Naast het vaste land van Frankrijk leveren wij ook op het Franse eiland Corsica.<br>

Er zijn een aantal speciale AirBnB of privé residenties waar wij ook kunnen leveren. Deze bestemmingen zijn als volgt: <br></p>

<ul>
	<li>Tignes - Afleveradres: Office de Tourisme, Tignes, 73320, Place Centrale 1, Frankrijk. Tel nr: +33 4 79 40 04 40</li>
	<li>Val d’isere - Afleveradres: Concergerie service bij TO, Val d Isere, 73150,  Place Jacques Mouflier 1, Frankrijk. Tel nr: +33 4 79 06 06 60</li>
	<li>Avoriaz, Morzine - Afleveradres: Skimium ALPES ATTITUDE, Morzine, 74110, Place du Baraty 19 Frankrijk. Tel nr +33 4 50 92 95 13</li>
	<li>Val Thorens - Afleveradres: SOGEVAB Lethitia, VAL THORENS, 73440, Maison de Val Thorens 1, Frankrijk. Tel nr +33 4 79 00 00 18</li>
	<li>Val Thorens - Afleveradres: Alta Pura, VAL THORENS, 73440, Rue du Soleil 1, Frankrijk. Tel nr +33 4 50 91 49 73</li>
	<li>Les Menuires - Afleveradres: Belleville Prestige Conciergerie, LES MENUIRES, 73440, Résidence le Brelin B.P 15, Frankrijk. Tel nr +33 6 59 71 46 45</li>
	<li>Les Menuires - Afleveradres: SOGEVAB Lethitia, LES MENUIRES, 73440, Quartier Croisette 1, Frankrijk. Tel nr +33 4 79 01 08 83</li>
</ul>

 <i>Let op: Bovenstaande locaties zijn bekend met Travel Light en hebben aangegeven dat zij jouw bagage willen ontvangen en in bewaring nemen. Je dient zelf contact op te nemen met de betreffende locatie om je aankomst aan te kondigen. In bijna alle gevallen zult je lokaal een vergoeding moeten betalen voor de stalling van je bagage. De locaties vallen niet onder de verantwoordelijkheid en service van Travel Light.</i><br><br>
<p>
<b>Waar je op moet letten:</b><br>

-Indien je je zending naar een afgelegen gebied stuurt, stel de ontvangende partij dan goed op de hoogte. Sommige gebieden (in de bergen bijvoorbeeld) zijn namelijk wat lastiger te bereiken. Het zou natuurlijk heel vervelend zijn als de aflevering in zo’n gebied door een miscommunicatie verkeerd loopt, dat kan immers ongewenste vertraging opleveren.

 </p>
 <br><br>
          

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                    <div class="image">
                          <a href="/tussenpagina.php?price=165&countryList=FR&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=79&countryList=FR&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/snow_small.png"></a>
                  </div>
                 <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=69&countryList=FR&productList=SKU_1&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
            </div>

          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>



  <?php include 'footer.php'; ?>