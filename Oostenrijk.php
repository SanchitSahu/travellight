<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/oostenrijk3.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Oostenrijk <small>Wintersport en nog veel meer!</small></h1>
			</div>
          
        
        <blockquote>Gemiddelde levertijd: 							5 werkdagen<br>

    </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           Het land dat vooral bekend staat om de wintersport, maar wat veel meer dan alleen dat te bieden heeft. Een kwart van het land behoort tot laag- en heuvelland, de rest behoort tot het middel- en hooggebergte en bijna de helft van het land is bedekt met bossen. Dat biedt mogelijkheden tot een hele hoop leuke activiteiten; fietsen, kamperen, wandelen, klimmen, snowboarden, noem het maar op. Je kunt er de hoogste watervallen van Europa bewonderen (de Krimml watervallen) & de op één na hoogste piek van Europa; de Grossglockner. Hou je niet zo van outdoor activiteiten? Wenen is een echt cultureel centrum dat bekend staat om zijn klassieke muziek, opera, musea, theater en indrukwekkende architectuur.  <br></p>

<p>
<strong>Waar je op moet letten:</strong><br></p>

<p>Let er op dat je, indien je je zending naar een afgelegen gebied stuurt, de ontvangende partij goed op de hoogte stelt van je zending. Sommige gebieden (in de bergen bijvoorbeeld) zijn namelijk wat lastiger te bereiken. Het zou natuurlijk heel vervelend zijn als de aflevering in zo’n gebied door een miscommunicatie verkeerd loopt, dat kan immers ongewenste vertraging opleveren. 
</p><p>
 <br><br>

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                    <div class="image">
                          <a href="/tussenpagina.php?price=165&countryList=AT&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=79&countryList=AT&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/snow_small.png"></a>
                  </div>
                 <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=69&countryList=AT&productList=SKU_1&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
            </div>

          </div>
        </div>

    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>



<?php include 'footer.php'; ?>