<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/uk.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Verenigd Koninkrijk: <small>Schotland, Ierland, Engeland & Wales.</small></h1>
			</div>
          
          <blockquote>
        Gemiddelde levertijd Engeland: 						4 werkdagen	<br>	
		Gemiddelde levertijd Noord Ierland:					5 werkdagen	<br>	
		Gemiddelde levertijd Schotland:  					4 werkdagen	<br>	
		Gemiddelde levertijd Wales: 						4 werkdagen	<br>

          </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           Één van de populairste activiteiten in het Verenigd Koninkrijk is wandeltochten maken. Er wordt veel energie gestopt in het onderhouden van de wandelroutes en de bewegwijzering daarvan, wat het wandelen erg uitnodigend maakt buiten de indrukwekkende, varierende landschappen die er te vinden zijn om. Waar er zo gewandeld kan worden, kan natuurlijk ook gefietst worden! Er zijn rustige fietsroutes waar geen ander verkeer mag komen & intense mountainbike trails voor de wat avontuurlijkere fietser. Je kunt er prachtige routes langs oude kastelen, burchten en tuinen of de ‘Jurassic Coast’ van Engeland volgen. <br></p>
	           
	           <p> Als eiland biedt het Verenigd Koninkrijk natuurlijk heel veel mogelijkheden tot water activiteiten en wordt wel eens genoemd als beste kite land in de hele wereld. Over het hele gebied zijn er heel veel plekken te vinden waar fanatiek wordt gekite; er is immers lekker veel wind, zee en strand! </p>

<p>
<strong>Waar je op moet letten:</strong><br></p>

<p>Indien je je zending naar een afgelegen gebied stuurt, stel de ontvangende partij dan goed op de hoogte. Sommige gebieden (in de bergen bijvoorbeeld) zijn namelijk wat lastiger te bereiken. Het zou natuurlijk heel vervelend zijn als de aflevering in zo’n gebied door een miscommunicatie verkeerd loopt, dat kan immers ongewenste vertraging opleveren.
</p><p>
Wij kunnen zendingen sturen van en naar Engeland, Noord Ierland, Schotland en Wales. Wij versturen niet naar Britse eilanden als Isle of Man, Jersey, Isle of Arran etc. 
</p><p>
 <br><br>

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                  <div class="image">
                          <a href="/tussenpagina.php?price=165&countryList=GB&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=69&countryList=GB&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=99&countryList=GB&productList=SKU_4&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/golf_small.png"></a>
                </div>
				<!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
            </div>

          </div>
        </div>

      
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>