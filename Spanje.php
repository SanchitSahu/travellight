<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/spain3.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Spanje <small>Verstuur je bagage naar de Spaanse zon.</small></h1>
			</div>
         <blockquote>
        Gemiddelde levertijd vaste land: 				 4 werkdagen	<br>	
		Gemiddelde levertijd Ibiza: 					 8 werkdagen	<br>	 
		Gemiddelde levertijd Mallorca: 					 5 werkdagen	<br>		
		Gemiddelde levertijd Menorca: 					 5 werkdagen	<br>
         </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           Spanje is een land van diversiteit waar je heerlijk op vakantie kunt gaan. Als je aan Spanje denkt, denk je waarschijnlijk aan zon, strand en zee. In Tarifa bijvoorbeeld, wordt er veel gekite & regelmatig maken mensen een heerlijke fietstocht door het land heen. Echter kun je in de winter ook prima skiien en snowboarden in de Pyreneeën en is Spanje een zeer populaire bestemming voor golfvakanties door de vele prachtige golfbanen en resorts. Aan de Costa del Sol liggen zoveel golfbanen dat het ook wel eens Costa del Golf wordt genoemd ;)<br></p>
<p>
	Spanje is een populaire bestemming voor Nederlanders om te overwinteren. Wij begrijpen dat veel mensen op leeftijd ook willen genieten van de Spaanse zon. Bij Travel Light helpen wij heel graag een handje mee door zware bagage rechtstreeks naar de receptie van het hotel te sturen. Wij hebben gemerkt dat het versturen van bagage voor veel senioren een nieuwe ervaring is en dat er af en toe wat nerveuze gevoelens opspelen. Daar hebben wij natuurlijk alle begrip voor. Wij houden u altijd op de hoogte van de status van de zending en het gaat bijna altijd helemaal goed. Onze klanten zijn achteraf tevreden en erg blij dat zij geen zware koffers hebben moeten meeslepen. 

</p>

<p>
	Wij leveren óók op de Spaanse eilanden Ibiza, Mallorca & Menorca.
</p>

<p>
<strong>Waar je op moet letten:</strong><br></p>

<p>Indien je je zending naar een afgelegen gebied stuurt, stel de ontvangende partij dan goed op de hoogte. Sommige gebieden (in de bergen bijvoorbeeld) zijn namelijk wat lastiger te bereiken. Het zou natuurlijk heel vervelend zijn als de aflevering in zo’n gebied door een miscommunicatie verkeerd loopt, dat kan immers ongewenste vertraging opleveren.</p><p>
-Houd er rekening mee dat wij niet naar de belastingvrije staten binnen Italië kunnen versturen, zoals bijvoorbeeld San Marino.
</p><p>
- Spanje heeft een hoop nationale feestdagen. De onderstaande feestdagen zijn vrije dagen voor heel Spanje in 2017 en op deze dagen zullen er dus geen zendingen worden geleverd:
</p>
<p>

<ul>
	<li>1 januari:	Nieuwjaarsdag </li>
	<li>6 januari: 	Driekoningen </li>
	<li>maart/april:	Pasen </li>
	<li>1 mei: 		Dag van de Arbeid </li>
	<li>15 augustus: 	Asuncion de la Virgen</li>
	<li>12 oktober: 	Spaanse Nationale Feestdag </li>
	<li>1 november: 	Allerheiligen </li>
	<li>6 december: 	Dag van de Constitutie </li>
	<li>8 december: 	Inmaculada Concepción </li>
	<li>25 december: 	Eerste kerstdag</li>
</ul>
<p>
<i>Naast deze nationale feestdagen zijn er soms ook regionale feestdagen waar men vrij heeft. Wij houden dit zelf zo goed mogelijk bij, maar het kan nooit kwaad om het zelf ook voor de zekerheid na te gaan.</i></p>
 <br><br>

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                
                  <div class="image">
                          <a href="/tussenpagina.php?price=165&countryList=ES&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=69&countryList=ES&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=99&countryList=ES&productList=SKU_4&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/golf_small.png"></a>
                </div>
				<!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
                
              </div>
            </div>

          </div>
        </div>

    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>



<?php include 'footer.php'; ?>