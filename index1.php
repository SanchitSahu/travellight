<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include 'header.php';?>

<section>
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">HOE WERKT TRAVEL LIGHT?</h2>
        </div>
        <div class="row services">
          <div class="col-sm-4">
              <div class="image">
                <img src="img/service-1.png">
              </div>
              <h4>Jouw reis en bagage</h4>
              <p>Bereken je kosten en boek online</p>
          </div>
          <div class="col-sm-4">
              <div class="image">
                <img src="img/service-2.png" height="51px">
              </div>
              <h4>Bagage klaarzetten</h4>
              <p>Onze chauffeur haalt je bagage op</p>
          </div>
          <div class="col-sm-4">
              <div class="image">
                <img src="img/service-3.png" height="51px">
              </div>
              <h4>Aflevering bagage</h4>
              <p>Je bagage staat klaar op je bestemming </p>
          </div>
        </div>
      </div>
    </section>


    
    <section class="reviews">
      <div class="container">
          <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                  <div class="item active">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star five"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>DIT BEDRIJF KRIJGT VAN ONS EEN GOUDEN PLUIM!</p>
                        <span>Anja & Rob</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star four"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>VOLGENDE KEER GAAT ONZE BAGAGE ZEKER WEER OP REIS MET TRAVEL LIGHT!</p>
                        <span>Selina</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star five"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>WIJ ZIJN UITERST TEVREDEN OVER JULLIE SERVICE!</p>
                        <span>Charlotte</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star five"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>DUIDELIJKE UITLEG, VERPAKKING EN DOCUMENTEN ALLEMAAL NETJES OP TIJD. HELEMAAL TOP!</p>
                        <span>Sam</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star four"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>PRIMA SERVICE EN VOOR HERHALING VATBAAR!</p>
                        <span>Pieter</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star five"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>SNELLE SERVICE, PERFECT GEREGELD EN PRIMA COMMUNICATIE!</p>
                        <span>Jeanine</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star four"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>ALS IK VOLGEND JAAR WEER MET DE MOTOR GA, DAN WEER MET TRAVEL LIGHT!</p>
                        <span>Huub</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star five"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>BEN ERG TEVREDEN. VOLGENDE KEER WEER, ZOU HET IEDEREEN AANRADEN!</p>
                        <span>Hetty</span>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="text-center">
                      <div class="ratings">
                          <span class="star five"></span>
                      </div>
                      <div class="ReviewContent">
                        <p>NIET MEER SJOUWEN MET JE FIETS. GEEN GEDOE OP HET VLIEGVELD. SIMPELWEG: IDEAAL! </p>
                        <span>Jan</span>
                      </div>
                    </div>
                  </div>
              </div>
              <!--<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>-->
            </div>
          </div>
          <div class="row text-center">
              <a target="_blank" href="http://app.12feedback.com/86/travel-light" class="btn-border">Meer reviews</a>
          </div>
        </div>
    </section>


    <section>
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">Travel light: Verstuur je bagage eenvoudig en snel van huis naar hotel</h2>
        </div>
        <div class="row">
          <div class="col-sm-6 features one">
            <div class="image">
              <img src="img/features-1.png">
            </div>
            <p>Travel light is goedgekeurd door Thuiswinkel waarborg</p>
          </div>
          <div class="col-sm-6 features two">
            <div class="image">
              <img src="img/features-2.png">
            </div>
            <p>Een dag voor je aankomst wordt je bagage afgeleverd op je vakantieadres</p>
          </div>
          <div class="col-sm-6 features three">
            <div class="image">
              <img src="img/features-3.png">
            </div>
            <p>Spaar punten bij elke reis en krijg korting op je volgende reis</p>
          </div>
          <div class="col-sm-6 features four">
            <div class="image">
              <img src="img/features-4.png">
            </div>
            <p>Wij verrassen je graag met exclusieve acties en kortingen. Mis deze niet!.</p>
          </div>
        </div>
      </div>
    </section>


    <section class="bgGrey strip">
      <div class="container">
        <div class="row">
          <div class="col-sm-8">
            <p>Beste prijs garantie</p>
            <h4>Bespaar geld met onze deals</h4>
          </div>
          <div class="col-sm-4 text-right">
              <a href="deals.php" class="btn btn-md btnOrange">SCHRIJF JE IN EN MIS GEEN DEALS!</a>
          </div>
        </div>
      </div>
    </section>


    <section>
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <a href="https://www.thuiswinkel.org/leden/travel-light-b.v/certificaat" target="_blank"><img src="img/client-1.png" ></a>
          </div>
          <div class="col-xs-6 client text-center">
            <img src="img/client-2.png" >
          </div>
        </div>
      </div>
    </section>

<?php include 'footer.php';?>
