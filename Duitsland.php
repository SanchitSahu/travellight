<?php include 'header.php';?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/duitsland3.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
         <div class="page-header">
             <h1 class="pageH1">Duitsland <small>Veelzijdig vakantieland voor iedereen.</small></h1>
			</div>
          
          <blockquote>
        Gemiddelde levertijd: 					4 werkdagen	<br>	


          </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           Ons buurland is een erg groot land dat veel te bieden heeft. Er zijn veel grote steden en kleine traditionele dorpjes om te bezoeken in de bergen. Je kunt er skien en snowboarden, wandelen in de uitgestrekte bossen, fietsen langs rivieren, kastelen bezoeken, golfen, kamperen, uit je dak gaan op Oktoberfest of de gezellige, bekende kerstmarkten bezoeken in november en december. Kortom; er is een hele hoop te doen in Duitsland.  <br></p>

<p>
<strong>Waar je op moet letten:</strong><br></p>

<p>Duitsland is erg groot. De levertijden hangen dan ook sterk af van je bestemming in Duitsland. Als je bestemming zich in West Duitsland bevindt, kan het zomaar zo zijn dat je zending maar 2 werkdagen onderweg is. Nu is dit in veel gevallen helemaal geen probleem, maar je moet er wel rekening mee houden als je bijvoorbeeld heel veel bagage hebt dat ergens moet worden opgeslagen. 
</p><p>
Indien je je zending naar een afgelegen gebied stuurt, stel de ontvangende partij dan goed op de hoogte. Sommige gebieden (in de bergen bijvoorbeeld) zijn namelijk wat lastiger te bereiken. Het zou natuurlijk heel vervelend zijn als de aflevering in zo’n gebied door een miscommunicatie verkeerd loopt, dat kan immers ongewenste vertraging opleveren.</p><p> <br><br>

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                    <div class="image">
                          <a href="/tussenpagina.php?price=155&countryList=DE&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=69&countryList=DE&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/snow_small.png"></a>
                  </div>
                 <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=59&countryList=DE&productList=SKU_1&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
                                </div>
            </div>

          </div>
        </div>

    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    
    
  <section>
    <div class="container">
      <div class="row">
        
        <div class="hr">
          <img alt="travellight" src="img/hrIcon.jpg">
        </div>

        <h2 class="md--text text-center">Reis zonder bagage en bespaar op je totale reiskosten! </h2>
        <h2 class="md--text text-center"><i class="fa fa-check green"></i> Neem de trein en bespaar op lang parkeren</h2>
        <h2 class="md--text text-center"><i class="fa fa-check green"></i> Huur een kleinere huurauto en bespaar tot € 200,- per week!</h2>
        
      </div>
    </div>
    </section>

    
<?php include 'footer.php';?>
