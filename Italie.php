<?php include 'header.php'; ?>
    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/italy2.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
	        <div class="page-header">
                    <h1 class="pageH1">Italië <small>Het land van heerlijk eten...</small></h1>
			</div>
          <blockquote>
        Gemiddelde levertijd vaste land: 					5 werkdagen	<br>	
		Gemiddelde levertijd Elba: 							5 werkdagen	<br>	
		Gemiddelde levertijd Sardinië: 						5 werkdagen	<br>	
		Gemiddelde levertijd Sicilië: 						7 werkdagen	<br>

          </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           Italië, het land van heerlijk eten, historische steden én schitterende natuur. Van skivakanties in de Dolomiten tot kiten in het zuiden van het land, het kan allemaal. Naast het vaste land van Italië versturen wij ook bagage naar de volgende eilanden: Elba, Sardinië en Sicilië. <br></p>

<p>
<strong>Waar je op moet letten:</strong><br></p>

<p>Let er op dat je, indien je je zending naar een afgelegen gebied stuurt, de ontvangende partij goed op de hoogte stelt van je zending. Sommige gebieden (in de bergen bijvoorbeeld) zijn namelijk wat lastiger te bereiken. Het zou natuurlijk heel vervelend zijn als de aflevering in zo’n gebied door een miscommunicatie verkeerd loopt, dat kan immers ongewenste vertraging opleveren.
</p><p>
-Houd er rekening mee dat wij niet naar de belastingvrije staten binnen Italië kunnen versturen, zoals bijvoorbeeld San Marino.
</p><p>
- Italië heeft, zoals veel katholieke medditeraanse landen, veel nationale feestdagen. Op deze dagen is men vrij en zal er niets worden opgehaald of afgeleverd. De onderstaande data gelden als vrije dagen voor heel Italië in 2017:
</p>
<ul>
	<li>1 januari:	Capodanno (Nieuwjaarsdag)</li>
	<li>6 januari: 	Befana / Epifani (Driekoningen)</li>
	<li>maart/april:	Sant'Angelo / Pasquetta (Pasen)</li>
	<li>25 april:	Giorno della Liberazione (Bevrijdingsdag)</li>
	<li>1 mei: 		Festa del Lavoro (Dag van de Arbeid)</li>
	<li>2 juni: 		Festa della Repubblica (Nationale feestdag)</li>
	<li>15 augustus: 	Ferragosto (Maria Hemelvaart)</li>
	<li>1 november: 	Ognissanti (Allerheiligen)</li>
	<li>8 december: 	Immacolata Concezione (Onbevlekte ontvangenis)</li>
	<li>25 december: 	Natale (Eerste kerstdag)</li>
	<li>26 december: 	Santo Stefano (Tweede kerstdag)</li>
</ul>
<p>
<i>Net zoals Spanje heeft Italië naast deze nationale feestdagen ook regionale feestdagen waar men vrij heeft. Wij houden dit zelf zo goed mogelijk bij, maar het kan nooit kwaad om het zelf ook voor de zekerheid na te gaan ;)</i></p>
 <br><br>

          

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                  <div class="image">
                          <a href="/tussenpagina.php?price=165&countryList=IT&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=79&countryList=IT&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/snow_small.png"></a>
                  </div>
                 <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=69&countryList=IT&productList=SKU_1&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
                
              </div>
            </div>

          </div>
        </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>



<?php include 'footer.php'; ?>