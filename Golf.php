<?php include 'header.php'; ?>

    
    <div class="innerBanner">
      <img alt="travellight" src="img/golf.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Golf <small>We versturen je standbag.</small></h1>
			</div>
          
          <div class="col-sm-10 col-sm-offset-1">
            <p class="text-justify txtDrk">Wij versturen voorlopig alleen een Golf Standbag in verband met de afmeting. Voor de duidelijkheid dit is een draagtas, ook wel carrybag of standbag genoemd. Voor het vervoeren van jouw golfclubs hebben wij speciaal een doos ontwikkeld ter bescherming van jouw materiaal. Deze doos heeft de volgende inhoudsmaat; 120cm hoog, 32cm breed en 27cm diep. In de meeste gevallen past zelfs de trolleybag in deze doos. Door je tas op te meten kom je hier snel achter. Bij een Trolleybag kan de trolley zelf niet mee worden verstuurd in de doos. Deze zijn vaak kosteloos beschikbaar bij het betreffende golf resort op de plaats van bestemming.
In de doos is ruimte voor extra bagage zoals bijvoorbeeld kleding of een toilettas. Al jouw overige bagage kan in je handbagage. </p>
          
            
                
            <p class="text-justify txtDrk"><b>Let op!</b> Met Travel Light is het niet mogelijk om tour- en caddybags te verzenden. Wij hebben ervoor gekozen dit formaat tas niet te accepteren i.v.m. de grootte. </p>
            <p class="text-justify txtDrk">Voor meer informatie en uitleg over onze dienst bekijk onze inpak video </p>
            
            <div class="text-center">
                <div class="spacer20 clearfix"></div>
                <iframe class="iframeClass" width="560" height="315" src="https://www.youtube.com/embed/dEr_06CxeZQ?list=PLuAtrvFtSay3vkfCAEpMIuZCR8KhOfYsr" allowfullscreen></iframe>  
                <div class="spacer20 clearfix"></div>
                <p>Most golfers prepare for disaster. A good golfer prepares for succes.</p>
                <p>- Bob Toski –</p>
            </div>
                
            

          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>