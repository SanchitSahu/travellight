<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/Overige_bagage.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Overige bagage <small>Andere bagage versturen wij natuurlijk ook.</small></h1>
			</div>
          <div class="col-sm-10 col-sm-offset-1">
            <p class="text-justify txtDrk">Heb jij een bagagestuk dat jij graag wilt versturen maar heb je niet kunnen vinden of wij dat wel aanbieden? Je kunt ons altijd bellen op werkdagen van 09:00 tot 17:00 met het nummer 085-4874344 of mail je vraag naar info@travel-light.nl. Geef ons hierbij de afmetingen van je pakket, het gewicht en de bestemming door zodat wij snel & efficient je vraag kunnen beantwoorden.</p>
            <p class="text-justify txtDrk">
Voor een aantal soorten bagage (kites, snowboards, ski’s & golfsets) versturen wij 10 dagen voor de ophaaldatum een doos naar onze klanten. Je ontvangt een Type A doos als je een kite of dubbele set snowboards/ski inclusief schoenen inboekt. Een type B doos ontvang je bij 1 set ski/snowboard inclusief schoenen of 2 sets ski’s/snowboards exclusief schoenen en een Type C doos ontvang je bij het verzenden van een golfset. Je hoeft deze dozen echter niet per sé te vullen met dit type bagage; zolang je ervoor zorgt dat de doos niet bol gaat staan en je het maximale gewicht niet overschrijdt, mag je de dozen ook vullen met andere spullen, denk aan camping spullen, klimmateriaal, vishengels, kleding etc. 
 </p>
     	     
 		 <div class="white overBagageClass">
      <div class="container">
        <div class="image">
	        <img alt="travellight" src="img/Overigebagage.png" class="img-responsive">
        </div>
      </div>
 		 </div>
            
            
          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>