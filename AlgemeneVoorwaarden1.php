<?php include 'header.php'; ?>

    
    


    <section>
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">Algemene Leverings- en Betalingsvoorwaarden</h2>
          
          <div class="col-sm-10 col-sm-offset-1">
            <p class="text-justify txtDrk"><div id="tekst">
<p>Travel Light is goedgekeurd door stichting thuiswinkel waarborg. Om die reden hanteren wij hun Algemene Voorwaarden met daarop aanvullend onze voorwaarden. Hiermee bent u ervan verzekerd dat u veilig zaken kunt doen met onze organisatie.&nbsp;</p>
<p style="text-align: center;"><a href="https://www.thuiswinkel.org/leden/travel-light-b.v/certificaat" target="_blank"><img src="img/client-1.png" ></a><br></p>
<h3 id="algemene-voorwaarden-thuiswinkel">Algemene Voorwaarden Thuiswinkel</h3>
<p>Deze Algemene Voorwaarden van de Nederlandse Thuiswinkel Organisatie (hierna: Thuiswinkel.org) zijn tot stand gekomen in overleg met de Consumentenbond in het kader van de Coördinatiegroep Zelfreguleringsoverleg (CZ) van de Sociaal-Economische Raad en treden in werking per 1 juni 2014.<br>
<br></p>
<h3>Inhoudsopgave:</h3>
<p>Artikel 1 – Definities<br>
Artikel 2 - Identiteit van de ondernemer<br>
Artikel 3 - Toepasselijkheid<br>
Artikel 4 - Het aanbod<br>
Artikel 5 - De overeenkomst<br>
Artikel 6 - Herroepingsrecht<br>
Artikel 7 - Verplichtingen van de consument tijdens de bedenktijd<br>
Artikel 8 - Uitoefening van het herroepingsrecht door de consument en kosten daarvan<br>
Artikel 9 - Verplichtingen van de ondernemer bij herroeping<br>
Artikel 10 - Uitsluiting herroepingsrecht<br>
Artikel 11 - De prijs<br>
Artikel 12 - Nakoming en extra garantie<br>
Artikel 13 - Levering en uitvoering<br>
Artikel 14 - Duurtransacties: duur, opzegging en verlenging<br>
Artikel 15 - Betaling<br>
Artikel 16 - Klachtenregeling<br>
Artikel 17 - Geschillen<br>
Artikel 18 - Branchegarantie<br>
Artikel 19 - Aanvullende of afwijkende bepalingen<br>
Artikel 20 - Wijziging van de Algemene Voorwaarden Thuiswinkel</p>
<h3 id="artikel-1-definities">Artikel 1 - Definities</h3>
In deze voorwaarden wordt verstaan onder:<br>
1. Aanvullende overeenkomst: een overeenkomst waarbij de consument producten, digitale inhoud en/of diensten verwerft in verband met een overeenkomst op afstand en deze zaken, digitale inhoud en/of diensten door de ondernemer worden geleverd of door een derde partij op basis van een afspraak tussen die derde en de ondernemer;<br>
2. Bedenktijd: de termijn waarbinnen de consument gebruik kan maken van zijn herroepingsrecht;<br>
3. Consument: de natuurlijke persoon die niet handelt voor doeleinden die verband houden met zijn handels-, bedrijfs-, ambachts- of beroepsactiviteit;<br>
4. Dag: kalenderdag;<br>
5. Digitale inhoud: gegevens die in digitale vorm geproduceerd en geleverd worden;<br>
6. Duurovereenkomst: een overeenkomst die strekt tot de regelmatige levering van zaken, diensten en/of digitale inhoud gedurende een bepaalde periode;<br>
7. Duurzame gegevensdrager: elk hulpmiddel - waaronder ook begrepen e-mail - dat de consument of ondernemer in staat stelt om informatie die aan hem persoonlijk is gericht, op te slaan op een manier die toekomstige raadpleging of gebruik gedurende een periode die is afgestemd op het doel waarvoor de informatie is bestemd, en die ongewijzigde reproductie van de opgeslagen informatie mogelijk maakt;<br>
8. Herroepingsrecht: de mogelijkheid van de consument om binnen de bedenktijd af te zien van de overeenkomst op afstand;<br>
9. Ondernemer: de natuurlijke of rechtspersoon die lid is van Thuiswinkel.org en producten, (toegang tot) digitale inhoud en/of diensten op afstand aan consumenten aanbiedt;<br>
10. Overeenkomst op afstand: een overeenkomst die tussen de ondernemer en de consument wordt gesloten in het kader van een georganiseerd systeem voor verkoop op afstand van producten, digitale inhoud en/of diensten, waarbij tot en met het sluiten van de overeenkomst uitsluitend of mede gebruik gemaakt wordt van één of meer technieken voor communicatie op afstand;<br>
11. Modelformulier voor herroeping: het in Bijlage I van deze voorwaarden opgenomen Europese modelformulier voor herroeping; Bijlage I hoeft niet ter beschikking te worden gesteld als de consument ter zake van zijn bestelling geen herroepingsrecht heeft;<br>
12. Techniek voor communicatie op afstand: middel dat kan worden gebruikt voor het sluiten van een overeenkomst, zonder dat consument en ondernemer gelijktijdig in dezelfde ruimte hoeven te zijn samengekomen.<br>
<h3 id="artikel-2-identiteit-van-de-ondernemer">Artikel 2 - Identiteit van de ondernemer</h3>
Naam ondernemer: Travel Light B.V.<br>
Handelend onder de naam/namen: - Travel Light. B.V.<br>
Vestigingsadres: Gevers Deynootweg 93 2586 BK Scheveningen<br>
Telefoonnummer: 0854874344<br>
Bereikbaarheid: Van maandag t/m vrijdag vanaf 08.30 uur tot 18.00 uur<br>
E-mailadres: info@travel-light.nl<br>
KvK-nummer: 53276264<br>
Btw-nummer: NL 850820510 B01<br>
<h3 id="artikel-3-toepasselijkheid">Artikel 3 - Toepasselijkheid</h3>
1. Deze algemene voorwaarden zijn van toepassing op elk aanbod van de ondernemer en op elke tot stand gekomen overeenkomst op afstand tussen ondernemer en consument.<br>
2. Voordat de overeenkomst op afstand wordt gesloten, wordt de tekst van deze algemene voorwaarden aan de consument beschikbaar gesteld. Indien dit redelijkerwijs niet mogelijk is, zal de ondernemer voordat de overeenkomst op afstand wordt gesloten, aangeven op welke wijze de algemene voorwaarden bij de ondernemer zijn in te zien en dat zij op verzoek van de consument zo spoedig mogelijk kosteloos worden toegezonden.<br>
3. Indien de overeenkomst op afstand elektronisch wordt gesloten, kan in afwijking van het vorige lid en voordat de overeenkomst op afstand wordt gesloten, de tekst van deze algemene voorwaarden langs elektronische weg aan de consument ter beschikking worden gesteld op zodanige wijze dat deze door de consument op een eenvoudige manier kan worden opgeslagen op een duurzame gegevensdrager. Indien dit redelijkerwijs niet mogelijk is, zal voordat de overeenkomst op afstand wordt gesloten, worden aangegeven waar van de algemene voorwaarden langs elektronische weg kan worden kennisgenomen en dat zij op verzoek van de consument langs elektronische weg of op andere wijze kosteloos zullen worden toegezonden.<br>
4. Voor het geval dat naast deze algemene voorwaarden tevens specifieke product- of dienstenvoorwaarden van toepassing zijn, is het tweede en derde lid van overeenkomstige toepassing en kan de consument zich in geval van tegenstrijdige voorwaarden steeds beroepen op de toepasselijke bepaling die voor hem het meest gunstig is.<br>
<h3 id="artikel-4-het-aanbod">Artikel 4 - Het aanbod</h3>
1. Indien een aanbod een beperkte geldigheidsduur heeft of onder voorwaarden geschiedt, wordt dit nadrukkelijk in het aanbod vermeld.<br>
2. Het aanbod bevat een volledige en nauwkeurige omschrijving van de aangeboden producten, digitale inhoud en/of diensten. De beschrijving is voldoende gedetailleerd om een goede beoordeling van het aanbod door de consument mogelijk te maken. Als de ondernemer gebruik maakt van afbeeldingen, zijn deze een waarheidsgetrouwe weergave van de aangeboden producten, diensten en/of digitale inhoud. Kennelijke vergissingen of kennelijke fouten in het aanbod binden de ondernemer niet.<br>
3. Elk aanbod bevat zodanige informatie, dat voor de consument duidelijk is wat de rechten en verplichtingen zijn, die aan de aanvaarding van het aanbod zijn verbonden.<br>
<h3 id="artikel-5-de-overeenkomst">Artikel 5 - De overeenkomst</h3>
1. De overeenkomst komt, onder voorbehoud van het bepaalde in lid 4, tot stand op het moment van aanvaarding door de consument van het aanbod en het voldoen aan de daarbij gestelde voorwaarden.<br>
2. Indien de consument het aanbod langs elektronische weg heeft aanvaard, bevestigt de ondernemer onverwijld langs elektronische weg de ontvangst van de aanvaarding van het aanbod. Zolang de ontvangst van deze aanvaarding niet door de ondernemer is bevestigd, kan de consument de overeenkomst ontbinden.<br>
3. Indien de overeenkomst elektronisch tot stand komt, treft de ondernemer passende technische en organisatorische maatregelen ter beveiliging van de elektronische overdracht van data en zorgt hij voor een veilige webomgeving. Indien de consument elektronisch kan betalen, zal de ondernemer daartoe passende veiligheidsmaatregelen in acht nemen.<br>
4. De ondernemer kan zich binnen wettelijke kaders - op de hoogte stellen of de consument aan zijn betalingsverplichtingen kan voldoen, alsmede van al die feiten en factoren die van belang zijn voor een verantwoord aangaan van de overeenkomst op afstand. Indien de ondernemer op grond van dit onderzoek goede gronden heeft om de overeenkomst niet aan te gaan, is hij gerechtigd gemotiveerd een bestelling of aanvraag te weigeren of aan de uitvoering bijzondere voorwaarden te verbinden.<br>
5. De ondernemer zal uiterlijk bij levering van het product, de dienst of digitale inhoud aan de consument de volgende informatie, schriftelijk of op zodanige wijze dat deze door de consument op een toegankelijke manier kan worden opgeslagen op een duurzame gegevensdrager, meesturen:<br>
a. het bezoekadres van de vestiging van de ondernemer waar de consument met klachten terecht kan;<br>
b. de voorwaarden waaronder en de wijze waarop de consument van het herroepingsrecht gebruik kan maken, dan wel een duidelijke melding inzake het uitgesloten zijn van het herroepingsrecht;<br>
c. de informatie over garanties en bestaande service na aankoop;<br>
d. de prijs met inbegrip van alle belastingen van het product, dienst of digitale inhoud; voor zover van toepassing de kosten van aflevering; en de wijze van betaling, aflevering of uitvoering van de overeenkomst op afstand;<br>
e. de vereisten voor opzegging van de overeenkomst indien de overeenkomst een duur heeft van meer dan één jaar of van onbepaalde duur is;<br>
f. indien de consument een herroepingsrecht heeft, het modelformulier voor herroeping.<br>
6. In geval van een duurtransactie is de bepaling in het vorige lid slechts van toepassing op de eerste levering.<br>
<h3 id="artikel-6-8211-herroepingsrecht">Artikel 6 – Herroepingsrecht</h3>
Bij producten:<br>
1. De consument kan een overeenkomst met betrekking tot de aankoop van een product gedurende een bedenktijd van 14 dagen zonder opgave van redenen ontbinden. De ondernemer mag de consument vragen naar de reden van herroeping, maar deze niet tot opgave van zijn reden(en) verplichten.<br>
2. De in lid 1 genoemde bedenktijd gaat in op de dag nadat de consument, of een vooraf door de consument aangewezen derde, die niet de vervoerder is, het product heeft ontvangen, of:<br>
a. als de consument in eenzelfde bestelling meerdere producten heeft besteld: de dag waarop de consument, of een door hem aangewezen derde, het laatste product heeft ontvangen. De ondernemer mag, mits hij de consument hier voorafgaand aan het bestelproces op duidelijke wijze over heeft geïnformeerd, een bestelling van meerdere producten met een verschillende levertijd weigeren.<br>
b. als de levering van een product bestaat uit verschillende zendingen of onderdelen: de dag waarop de consument, of een door hem aangewezen derde, de laatste zending of het laatste onderdeel heeft ontvangen;<br>
c. bij overeenkomsten voor regelmatige levering van producten gedurende een bepaalde periode: de dag waarop de consument, of een door hem aangewezen derde, het eerste product heeft ontvangen<br>
Bij diensten en digitale inhoud die niet op een materiële drager is geleverd:<br>
3. De consument kan een dienstenovereenkomst en een overeenkomst voor levering van digitale inhoud die niet op een materiële drager is geleverd gedurende 14 dagen zonder opgave van redenen ontbinden. De ondernemer mag de consument vragen naar de reden van herroeping, maar deze niet tot opgave van zijn reden(en) verplichten.<br>
4. De in lid 3 genoemde bedenktijd gaat in op de dag die volgt op het sluiten van de overeenkomst.<br>
Verlengde bedenktijd voor producten, diensten en digitale inhoud die niet op een materiële drager is geleverd bij niet informeren over herroepingsrecht:<br>
5. Indien de ondernemer de consument de wettelijk verplichte informatie over het herroepingsrecht of het modelformulier voor herroeping niet heeft verstrekt, loopt de bedenktijd af twaalf maanden na het einde van de oorspronkelijke, overeenkomstig de vorige leden van dit artikel vastgestelde bedenktijd.<br>
6. Indien de ondernemer de in het voorgaande lid bedoelde informatie aan de consument heeft verstrekt binnen twaalf maanden na de ingangsdatum van de oorspronkelijke bedenktijd, verstrijkt de bedenktijd 14 dagen na de dag waarop de consument die informatie heeft ontvangen.<br>
<h3 id="artikel-7-verplichtingen-van-de-consument-tijdens-de-bedenktijd">Artikel 7 - Verplichtingen van de consument tijdens de bedenktijd</h3>
1. Tijdens de bedenktijd zal de consument zorgvuldig omgaan met het product en de verpakking. Hij zal het product slechts uitpakken of gebruiken in de mate die nodig is om de aard, de kenmerken en de werking van het product vast te stellen. Het uitgangspunt hierbij is dat de consument het product slechts mag hanteren en inspecteren zoals hij dat in een winkel zou mogen doen.<br>
2. De consument is alleen aansprakelijk voor waardevermindering van het product die het gevolg is van een manier van omgaan met het product die verder gaat dan toegestaan in lid 1.<br>
3. De consument is niet aansprakelijk voor waardevermindering van het product als de ondernemer hem niet voor of bij het sluiten van de overeenkomst alle wettelijk verplichte informatie over het herroepingsrecht heeft verstrekt.<br>
<h3 id="artikel-8-uitoefening-van-het-herroepingsrecht-door-de-consument-en-kosten-daarvan">Artikel 8 - Uitoefening van het herroepingsrecht door de consument en kosten daarvan</h3>
1. Als de consument gebruik maakt van zijn herroepingsrecht, meldt hij dit binnen de bedenktermijn door middel van het modelformulier voor herroeping of op andere ondubbelzinnige wijze aan de ondernemer.<br>
2. Zo snel mogelijk, maar binnen 14 dagen vanaf de dag volgend op de in lid 1 bedoelde melding, zendt de consument het product terug, of overhandigt hij dit aan (een gemachtigde van) de ondernemer. Dit hoeft niet als de ondernemer heeft aangeboden het product zelf af te halen. De consument heeft de terugzendtermijn in elk geval in acht genomen als hij het product terugzendt voordat de bedenktijd is verstreken.<br>
3. De consument zendt het product terug met alle geleverde toebehoren, indien redelijkerwijs mogelijk in originele staat en verpakking, en conform de door de ondernemer verstrekte redelijke en duidelijke instructies.<br>
4. Het risico en de bewijslast voor de juiste en tijdige uitoefening van het herroepingsrecht ligt bij de consument.<br>
5. De consument draagt de rechtstreekse kosten van het terugzenden van het product. Als de ondernemer niet heeft gemeld dat de consument deze kosten moet dragen of als de ondernemer aangeeft de kosten zelf te dragen, hoeft de consument de kosten voor terugzending niet te dragen.<br>
6. Indien de consument herroept na eerst uitdrukkelijk te hebben verzocht dat de verrichting van de dienst of de levering van gas, water of elektriciteit die niet gereed voor verkoop zijn gemaakt in een beperkt volume of bepaalde hoeveelheid aanvangt tijdens de bedenktijd, is de consument de ondernemer een bedrag verschuldigd dat evenredig is aan dat gedeelte van de verbintenis dat door de ondernemer is nagekomen op het moment van herroeping, vergeleken met de volledige nakoming van de verbintenis.<br>
7. De consument draagt geen kosten voor de uitvoering van diensten of de levering van water, gas of elektriciteit, die niet gereed voor verkoop zijn gemaakt in een beperkt volume of hoeveelheid, of tot levering van stadsverwarming, indien:<br>
a. de ondernemer de consument de wettelijk verplichte informatie over het herroepingsrecht, de kostenvergoeding bij herroeping of het modelformulier voor herroeping niet heeft verstrekt, of;<br>
b. de consument niet uitdrukkelijk om de aanvang van de uitvoering van de dienst of levering van gas, water, elektriciteit of stadsverwarming tijdens de bedenktijd heeft verzocht.<br>
8. De consument draagt geen kosten voor de volledige of gedeeltelijke levering van niet op een materiële drager geleverde digitale inhoud, indien:<br>
a. hij voorafgaand aan de levering ervan niet uitdrukkelijk heeft ingestemd met het beginnen van de nakoming van de overeenkomst voor het einde van de bedenktijd;<br>
b. hij niet heeft erkend zijn herroepingsrecht te verliezen bij het verlenen van zijn toestemming; of<br>
c. de ondernemer heeft nagelaten deze verklaring van de consument te bevestigen.<br>
9. Als de consument gebruik maakt van zijn herroepingsrecht, worden alle aanvullende overeenkomsten van rechtswege ontbonden.<br>
<h3 id="artikel-9-verplichtingen-van-de-ondernemer-bij-herroeping">Artikel 9 - Verplichtingen van de ondernemer bij herroeping</h3>
1. Als de ondernemer de melding van herroeping door de consument op elektronische wijze mogelijk maakt, stuurt hij na ontvangst van deze melding onverwijld een ontvangstbevestiging.<br>
2. De ondernemer vergoedt alle betalingen van de consument, inclusief eventuele leveringskosten door de ondernemer in rekening gebracht voor het geretourneerde product, onverwijld doch binnen 14 dagen volgend op de dag waarop de consument hem de herroeping meldt. Tenzij de ondernemer aanbiedt het product zelf af te halen, mag hij wachten met terugbetalen tot hij het product heeft ontvangen of tot de consument aantoont dat hij het product heeft teruggezonden, naar gelang welk tijdstip eerder valt.<br>
3. De ondernemer gebruikt voor terugbetaling hetzelfde betaalmiddel dat de consument heeft gebruikt, tenzij de consument instemt met een andere methode. De terugbetaling is kosteloos voor de consument.<br>
4. Als de consument heeft gekozen voor een duurdere methode van levering dan de goedkoopste standaardlevering, hoeft de ondernemer de bijkomende kosten voor de duurdere methode niet terug te betalen.<br>
<h3 id="artikel-10-uitsluiting-herroepingsrecht">Artikel 10 - Uitsluiting herroepingsrecht</h3>
De ondernemer kan de navolgende producten en diensten uitsluiten van het herroepingsrecht, maar alleen als de ondernemer dit duidelijk bij het aanbod, althans tijdig voor het sluiten van de overeenkomst, heeft vermeld:<br>
1. Producten of diensten waarvan de prijs gebonden is aan schommelingen op de financiële markt waarop de ondernemer geen invloed heeft en die zich binnen de herroepingstermijn kunnen voordoen<br>
2. Overeenkomsten die gesloten zijn tijdens een openbare veiling. Onder een openbare veiling wordt verstaan een verkoopmethode waarbij producten, digitale inhoud en/of diensten door de ondernemer worden aangeboden aan de consument die persoonlijk aanwezig is of de mogelijkheid krijgt persoonlijk aanwezig te zijn op de veiling, onder leiding van een veilingmeester, en waarbij de succesvolle bieder verplicht is de producten, digitale inhoud en/of diensten af te nemen;<br>
3. Dienstenovereenkomsten, na volledige uitvoering van de dienst, maar alleen als:<br>
a. de uitvoering is begonnen met uitdrukkelijke voorafgaande instemming van de consument; en<br>
b. de consument heeft verklaard dat hij zijn herroepingsrecht verliest zodra de ondernemer de overeenkomst volledig heeft uitgevoerd;<br>
4. Pakketreizen als bedoeld in artikel 7:500 BW en overeenkomsten van personenvervoer;<br>
5. Dienstenovereenkomsten voor terbeschikkingstelling van accommodatie, als in de overeenkomst een bepaalde datum of periode van uitvoering is voorzien en anders dan voor woondoeleinden, goederenvervoer, autoverhuurdiensten en catering;<br>
6. Overeenkomsten met betrekking tot vrijetijdsbesteding, als in de overeenkomst een bepaalde datum of periode van uitvoering daarvan is voorzien;<br>
7. Volgens specificaties van de consument vervaardigde producten, die niet geprefabriceerd zijn en die worden vervaardigd op basis van een individuele keuze of beslissing van de consument, of die duidelijk voor een specifieke persoon bestemd zijn;<br>
8. Producten die snel bederven of een beperkte houdbaarheid hebben;<br>
9. Verzegelde producten die om redenen van gezondheidsbescherming of hygiëne niet geschikt zijn om te worden teruggezonden en waarvan de verzegeling na levering is verbroken;<br>
10. Producten die na levering door hun aard onherroepelijk vermengd zijn met andere producten;<br>
11. Alcoholische dranken waarvan de prijs is overeengekomen bij het sluiten van de overeenkomst, maar waarvan de levering slechts kan plaatsvinden na 30 dagen, en waarvan de werkelijke waarde afhankelijk is van schommelingen van de markt waarop de ondernemer geen invloed heeft;<br>
12. Verzegelde audio-, video-opnamen en computerprogrammatuur, waarvan de verzegeling na levering is verbroken;<br>
13. Kranten, tijdschriften of magazines, met uitzondering van abonnementen hierop;<br>
14. De levering van digitale inhoud anders dan op een materiële drager, maar alleen als:<br>
a. de uitvoering is begonnen met uitdrukkelijke voorafgaande instemming van de consument; en<br>
b. de consument heeft verklaard dat hij hiermee zijn herroepingsrecht verliest.<br>
<h3 id="artikel-11-de-prijs">Artikel 11 - De prijs</h3>
1. Gedurende de in het aanbod vermelde geldigheidsduur worden de prijzen van de aangeboden producten en/of diensten niet verhoogd, behoudens prijswijzigingen als gevolg van veranderingen in btw-tarieven.<br>
2. In afwijking van het vorige lid kan de ondernemer producten of diensten waarvan de prijzen gebonden zijn aan schommelingen op de financiële markt en waar de ondernemer geen invloed op heeft, met variabele prijzen aanbieden. Deze gebondenheid aan schommelingen en het feit dat eventueel vermelde prijzen richtprijzen zijn, worden bij het aanbod vermeld.<br>
3. Prijsverhogingen binnen 3 maanden na de totstandkoming van de overeenkomst zijn alleen toegestaan indien zij het gevolg zijn van wettelijke regelingen of bepalingen.<br>
4. Prijsverhogingen vanaf 3 maanden na de totstandkoming van de overeenkomst zijn alleen toegestaan indien de ondernemer dit bedongen heeft en:<br>
a. deze het gevolg zijn van wettelijke regelingen of bepalingen; of<br>
b. de consument de bevoegdheid heeft de overeenkomst op te zeggen met ingang van de dag waarop de prijsverhoging ingaat.<br>
5. De in het aanbod van producten of diensten genoemde prijzen zijn inclusief btw.<br>
<h3 id="artikel-12-nakoming-overeenkomst-en-extra-garantie">Artikel 12 - Nakoming overeenkomst en extra garantie</h3>
1. De ondernemer staat er voor in dat de producten en/of diensten voldoen aan de overeenkomst, de in het aanbod vermelde specificaties, aan de redelijke eisen van deugdelijkheid en/of bruikbaarheid en de op de datum van de totstandkoming van de overeenkomst bestaande wettelijke bepalingen en/of overheidsvoorschriften. Indien overeengekomen staat de ondernemer er tevens voor in dat het product geschikt is voor ander dan normaal gebruik.<br>
2. Een door de ondernemer, diens toeleverancier, fabrikant of importeur verstrekte extra garantie beperkt nimmer de wettelijke rechten en vorderingen die de consument op grond van de overeenkomst tegenover de ondernemer kan doen gelden indien de ondernemer is tekortgeschoten in de nakoming van zijn deel van de overeenkomst.<br>
3. Onder extra garantie wordt verstaan iedere verbintenis van de ondernemer, diens toeleverancier, importeur of producent waarin deze aan de consument bepaalde rechten of vorderingen toekent die verder gaan dan waartoe deze wettelijk verplicht is in geval hij is tekortgeschoten in de nakoming van zijn deel van de overeenkomst.<br>
<h3 id="artikel-13-levering-en-uitvoering">Artikel 13 - Levering en uitvoering</h3>
1. De ondernemer zal de grootst mogelijke zorgvuldigheid in acht nemen bij het in ontvangst nemen en bij de uitvoering van bestellingen van producten en bij de beoordeling van aanvragen tot verlening van diensten.<br>
2. Als plaats van levering geldt het adres dat de consument aan de ondernemer kenbaar heeft gemaakt.<br>
3. Met inachtneming van hetgeen hierover in artikel 4 van deze algemene voorwaarden is vermeld, zal de ondernemer geaccepteerde bestellingen met bekwame spoed doch uiterlijk binnen 30 dagen uitvoeren, tenzij een andere leveringstermijn is overeengekomen. Indien de bezorging vertraging ondervindt, of indien een bestelling niet dan wel slechts gedeeltelijk kan worden uitgevoerd, ontvangt de consument hiervan uiterlijk 30 dagen nadat hij de bestelling geplaatst heeft bericht. De consument heeft in dat geval het recht om de overeenkomst zonder kosten te ontbinden en recht op eventuele schadevergoeding.<br>
4. Na ontbinding conform het vorige lid zal de ondernemer het bedrag dat de consument betaald heeft onverwijld terugbetalen.<br>
5. Het risico van beschadiging en/of vermissing van producten berust bij de ondernemer tot het moment van bezorging aan de consument of een vooraf aangewezen en aan de ondernemer bekend gemaakte vertegenwoordiger, tenzij uitdrukkelijk anders is overeengekomen.<br>
<h3 id="artikel-14-duurtransacties-duur-opzegging-en-verlenging">Artikel 14 - Duurtransacties: duur, opzegging en verlenging</h3>
Opzegging:<br>
1. De consument kan een overeenkomst die voor onbepaalde tijd is aangegaan en die strekt tot het geregeld afleveren van producten (elektriciteit daaronder begrepen) of diensten, te allen tijde opzeggen met inachtneming van daartoe overeengekomen opzeggingsregels en een opzegtermijn van ten hoogste één maand.<br>
2. De consument kan een overeenkomst die voor bepaalde tijd is aangegaan en die strekt tot het geregeld afleveren van producten (elektriciteit daaronder begrepen) of diensten, te allen tijde tegen het einde van de bepaalde duur opzeggen met inachtneming van daartoe overeengekomen opzeggingsregels en een opzegtermijn van ten hoogste één maand.<br>
3. De consument kan de in de vorige leden genoemde overeenkomsten:<br>
a. te allen tijde opzeggen en niet beperkt worden tot opzegging op een bepaald tijdstip of in een bepaalde periode;<br>
b. tenminste opzeggen op dezelfde wijze als zij door hem zijn aangegaan;<br>
c. altijd opzeggen met dezelfde opzegtermijn als de ondernemer voor zichzelf heeft bedongen.<br>
Verlenging:<br>
4. Een overeenkomst die voor bepaalde tijd is aangegaan en die strekt tot het geregeld afleveren van producten (elektriciteit daaronder begrepen) of diensten, mag niet stilzwijgend worden verlengd of vernieuwd voor een bepaalde duur.<br>
5. In afwijking van het vorige lid mag een overeenkomst die voor bepaalde tijd is aangegaan en die strekt tot het geregeld afleveren van dag- nieuws- en weekbladen en tijdschriften stilzwijgend worden verlengd voor een bepaalde duur van maximaal drie maanden, als de consument deze verlengde overeenkomst tegen het einde van de verlenging kan opzeggen met een opzegtermijn van ten hoogste één maand.<br>
6. Een overeenkomst die voor bepaalde tijd is aangegaan en die strekt tot het geregeld afleveren van producten of diensten, mag alleen stilzwijgend voor onbepaalde duur worden verlengd als de consument te allen tijde mag opzeggen met een opzegtermijn van ten hoogste één maand. De opzegtermijn is ten hoogste drie maanden in geval de overeenkomst strekt tot het geregeld, maar minder dan eenmaal per maand, afleveren van dag-, nieuws- en weekbladen en tijdschriften.<br>
7. Een overeenkomst met beperkte duur tot het geregeld ter kennismaking afleveren van dag-, nieuws- en weekbladen en tijdschriften (proef- of kennismakingsabonnement) wordt niet stilzwijgend voortgezet en eindigt automatisch na afloop van de proef- of kennismakingsperiode.<br>
Duur:<br>
8. Als een overeenkomst een duur van meer dan een jaar heeft, mag de consument na een jaar de overeenkomst te allen tijde met een opzegtermijn van ten hoogste één maand opzeggen, tenzij de redelijkheid en billijkheid zich tegen opzegging vóór het einde van de overeengekomen duur verzetten.<br>
<h3 id="artikel-15-betaling">Artikel 15 - Betaling</h3>
1. Voor zover niet anders is bepaald in de overeenkomst of aanvullende voorwaarden, dienen de door de consument verschuldigde bedragen te worden voldaan binnen 14 dagen na het ingaan van de bedenktermijn, of bij het ontbreken van een bedenktermijn binnen 14 dagen na het sluiten van de overeenkomst. In geval van een overeenkomst tot het verlenen van een dienst, vangt deze termijn aan op de dag nadat de consument de bevestiging van de overeenkomst heeft ontvangen.<br>
2. Bij de verkoop van producten aan consumenten mag de consument in algemene voorwaarden nimmer verplicht worden tot vooruitbetaling van meer dan 50%. Wanneer vooruitbetaling is bedongen, kan de consument geen enkel recht doen gelden aangaande de uitvoering van de desbetreffende bestelling of dienst(en), alvorens de bedongen vooruitbetaling heeft plaatsgevonden.<br>
3. De consument heeft de plicht om onjuistheden in verstrekte of vermelde betaalgegevens onverwijld aan de ondernemer te melden.<br>
4. Indien de consument niet tijdig aan zijn betalingsverplichting(en) voldoet, is deze, nadat hij door de ondernemer is gewezen op de te late betaling en de ondernemer de consument een termijn van 14 dagen heeft gegund om alsnog aan zijn betalingsverplichtingen te voldoen, na het uitblijven van betaling binnen deze 14-dagen-termijn, over het nog verschuldigde bedrag de wettelijke rente verschuldigd en is de ondernemer gerechtigd de door hem gemaakte buitengerechtelijke incassokosten in rekening te brengen. Deze incassokosten bedragen maximaal: 15% over openstaande bedragen tot € 2.500,=; 10% over de daaropvolgende € 2.500,= en 5% over de volgende € 5.000,= met een minimum van € 40,=. De ondernemer kan ten voordele van de consument afwijken van genoemde bedragen en percentages.<br>
<h3 id="artikel-16-klachtenregeling">Artikel 16 - Klachtenregeling</h3>
1. De ondernemer beschikt over een voldoende bekend gemaakte klachtenprocedure en behandelt de klacht overeenkomstig deze klachtenprocedure.<br>
2. Klachten over de uitvoering van de overeenkomst moeten binnen bekwame tijd nadat de consument de gebreken heeft geconstateerd, volledig en duidelijk omschreven worden ingediend bij de ondernemer.<br>
3. Bij de ondernemer ingediende klachten worden binnen een termijn van 14 dagen gerekend vanaf de datum van ontvangst beantwoord. Als een klacht een voorzienbaar langere verwerkingstijd vraagt, wordt door de ondernemer binnen de termijn van 14 dagen geantwoord met een bericht van ontvangst en een indicatie wanneer de consument een meer uitvoerig antwoord kan verwachten.<br>
4. Een klacht over een product, dienst of de service van de ondernemer kan eveneens worden ingediend via een klachtenformulier op de consumentenpagina van de website van Thuiswinkel.org www.thuiswinkel.org. De klacht wordt dan zowel naar de betreffende ondernemer als naar Thuiswinkel.org gestuurd.<br>
5. De consument dient de ondernemer in ieder geval 4 weken de tijd te geven om de klacht in onderling overleg op te lossen. Na deze termijn ontstaat een geschil dat vatbaar is voor de geschillenregeling.<br>
<h3 id="artikel-17-geschillen">Artikel 17 - Geschillen</h3>
1. Op overeenkomsten tussen de ondernemer en de consument waarop deze algemene voorwaarden betrekking hebben, is uitsluitend Nederlands recht van toepassing.<br>
2. Geschillen tussen de consument en de ondernemer over de totstandkoming of uitvoering van overeenkomsten met betrekking tot door deze ondernemer te leveren of geleverde producten en diensten, kunnen, met inachtneming van het hierna bepaalde, zowel door de consument als de ondernemer worden voorgelegd aan de Geschillencommissie Thuiswinkel, Postbus 90600, 2509 LP te Den Haag (www.sgc.nl).<br>
3. Een geschil wordt door de Geschillencommissie slechts in behandeling genomen, indien de consument zijn klacht eerst binnen bekwame tijd aan de ondernemer heeft voorgelegd.<br>
4. Leidt de klacht niet tot een oplossing dan moet het geschil uiterlijk 12 maanden na de datum waarop de consument de klacht bij de ondernemer indiende, schriftelijk of in een andere door de Commissie te bepalen vorm bij de Geschillencommissie aanhangig worden gemaakt.<br>
5. Wanneer de consument een geschil wil voorleggen aan de Geschillencommissie, is de ondernemer aan deze keuze gebonden. Bij voorkeur meldt de consument dit eerst aan de ondernemer.<br>
6. Wanneer de ondernemer een geschil wil voorleggen aan de Geschillencommissie, zal de consument binnen vijf weken na een daartoe door de ondernemer schriftelijk gedaan verzoek, schriftelijk dienen uit te spreken of hij zulks ook wenst dan wel het geschil wil laten behandelen door de daartoe bevoegde rechter. Verneemt de ondernemer de keuze van de consument niet binnen de termijn van vijf weken, dan is de ondernemer gerechtigd het geschil voor te leggen aan de bevoegde rechter.<br>
7. De Geschillencommissie doet uitspraak onder de voorwaarden zoals deze zijn vastgesteld in het reglement van de Geschillencommissie (www.degeschillencommissie.nl/over-ons/de-commissies/2404/thuiswinkel). De beslissingen van de Geschillencommissie geschieden bij wege van bindend advies.<br>
8. De Geschillencommissie zal een geschil niet behandelen of de behandeling staken, indien aan de ondernemer surseance van betaling is verleend, deze in staat van faillissement is geraakt of zijn bedrijfsactiviteiten feitelijk heeft beëindigd, voordat een geschil door de commissie op de zitting is behandeld en een einduitspraak is gewezen.<br>
9. Indien naast de Geschillencommissie Thuiswinkel een andere erkende of bij de Stichting Geschillencommissies voor Consumentenzaken (SGC) of het Klachteninstituut Financiële Dienstverlening (Kifid) aangesloten geschillencommissie bevoegd is, is voor geschillen betreffende hoofdzakelijk de methode van verkoop of dienstverlening op afstand de Geschillencommissie Thuiswinkel bij voorkeur bevoegd. Voor alle overige geschillen de andere erkende bij SGC of Kifid aangesloten geschillencommissie.<br>
<h3 id="artikel-18-branchegarantie">Artikel 18 - Branchegarantie</h3>
1. Thuiswinkel.org staat garant voor de nakoming van de bindende adviezen van de Geschillencommissie Thuiswinkel door haar leden, tenzij het lid besluit het bindend advies binnen twee maanden na de verzending ervan ter toetsing aan de rechter voor te leggen. Deze garantstelling herleeft, indien het bindend advies na toetsing door de rechter in stand is gebleven en het vonnis waaruit dit blijkt, in kracht van gewijsde is gegaan. Tot maximaal een bedrag van €10.000,- per bindend advies, wordt dit bedrag door Thuiswinkel.org aan de consument uitgekeerd. Bij bedragen groter dan €10.000,- per bindend advies, wordt €10.000,- uitgekeerd. Voor het meerdere heeft Thuiswinkel.org een inspanningsverplichting om ervoor te zorgen dat het lid het bindend advies nakomt.<br>
2. Voor toepassing van deze garantie is vereist dat de consument een schriftelijk beroep hierop doet bij Thuiswinkel.org en dat hij zijn vordering op de ondernemer overdraagt aan Thuiswinkel.org. Indien de vordering op de ondernemer meer bedraagt dan €10.000,-, wordt de consument aangeboden zijn vordering voor zover die boven het bedrag van €10.000,- uitkomt over te dragen aan Thuiswinkel.org, waarna deze organisatie op eigen naam en kosten de betaling daarvan in rechte zal vragen ter voldoening aan de consument.<br>
<h3 id="artikel-19-aanvullende-of-afwijkende-bepalingen">Artikel 19 - Aanvullende of afwijkende bepalingen</h3>
Aanvullende dan wel van deze algemene voorwaarden afwijkende bepalingen mogen niet ten nadele van de consument zijn en dienen schriftelijk te worden vastgelegd dan wel op zodanige wijze dat deze door de consument op een toegankelijke manier kunnen worden opgeslagen op een duurzame gegevensdrager.<br>
<h3 id="artikel-20-wijziging-van-de-algemene-voorwaarden-thuiswinkel">Artikel 20 - Wijziging van de Algemene Voorwaarden Thuiswinkel</h3>
1. Thuiswinkel.org zal deze algemene voorwaarden niet wijzigen dan in overleg met de Consumentenbond.<br>
2. Wijzigingen in deze voorwaarden zijn slechts van kracht nadat deze op daartoe geëigende wijze zijn gepubliceerd, met dien verstande, dat bij toepasselijke wijzigingen gedurende de looptijd van een aanbod de voor de consument meest gunstige bepaling zal prevaleren.<br>
Thuiswinkel.org<br>
www.thuiswinkel.org<br>
Horaplantsoen 20, 6717 LT Ede<br>
Postbus 7001, 6710 CB Ede<br>
<h1 id="aanvullende-bepalingen">Aanvullende bepalingen</h1>
<p>- voor de verzending van bagage en sportuitrustingen van reizigers van deur tot deur binnen de Europese Unie. (hierna: "pakket(ten)")</p>
<h3 id="artikel-1-definities">Artikel 1 - Definities</h3>
In deze algemene voorwaarden wordt verstaan onder:
<p>Travel Light: Travel Light B.V., Gevestigd te ’s-Gravenhage;</p>
<p>Afzender: Opdrachtgever van Travel Light B.V.;</p>
<p>Geadresseerde: Degene aan wie Travel Light de Zending krachtens de Vervoersovereenkomst dient uit te leveren;</p>
<p>Zending: Een aan Travel Light ten vervoer aangeboden vervoerseenheid (pakket of ander soortige bagage) welke is bestemd voor een Geadresseerde, en daartoe is voorzien van een eigen vervoersdocument;</p>
<p>Internationale Zending: Pakket, en losse Zending met een bestemming binnen de EU, Zending met een bestemming in een gebied buiten Nederland (uitgaand) of een zending afkomstig uit zo’n gebied (inkomend);</p>
<p>Partij: Een hoeveelheid zendingen die gelijktijdig ten vervoer worden aangeboden voor rekeningen van één en dezelfde Afzender overeengekomen voorwaarden met betrekking tot onder andere de verschuldigde vergoeding, het minimum aan te bieden aantal, tijdstip van aanbieding en Aanbiedingspunt;</p>
<p>Vervoersovereenkomst: Een tussen Travel Light en de afzender onder toepasselijkheid van deze voorwaarden gesloten overeenkomst tot vervoer;</p>
<p>Vervoersdocumenten: De op de Zending aanwezige gegevensdrager (of combinatie van gegevensdragers), waaruit de specificaties met betrekking tot het vervoer kunnen worden herleid, zoals afzendadres, adres van de Geadresseerde, barcode en zendingsnummer;</p>
<p>Consignment Number: Op de logistieke documenten, verstrekt door Travel Light, wordt een Consignment Number vermeld. Onder dit nummer is de Zending bekend bij zowel Travel Light als haar onderaannemers.</p>
<h3 id="artikel-2-de-partij-waarmee-u-een-overeenkomst-afsluit">Artikel 2 - De Partij waarmee u een overeenkomst afsluit</h3>
De overeenkomst voor vervoer sluit u af met de onderneming Travel Light.<br>
• Travel Light accepteert geen orders van commerciële wederverkopers, behalve die van geautoriseerde wederverkopers.
<h3 id="artikel-3-aanvaarding-algemene-voorwaarden">Artikel 3 - Aanvaarding Algemene Voorwaarden</h3>
Door uw Zending aan ons toe te vertrouwen, aanvaardt u onze algemene voorwaarden namens uzelf en/of namens elke andere partij die een belang heeft in de zending of de uitvoering van overige diensten. Onze algemene voorwaarden zijn ook van toepassing op, en kunnen ook ingeroepen worden door iedere partij die we inschakelen voor het ophalen, vervoeren of afleveren van uw zending dan wel voor het verrichten van overige diensten, of aan wie we deze diensten uitbesteden, alsook op en door onze medewerkers, bestuurders en agenten.
<h3 id="artikel-4-reikwijdte-van-de-overeenkomst">Artikel 4 - Reikwijdte van de overeenkomst</h3>
Door met ons een overeenkomst van om het even welke aard af te sluiten dat het vervoer van goederen omvat, stemt u ermee in dat:<br>
• de overeenkomst geldt als overeenkomst voor vervoer van goederen over de weg indien de zending effectief over de weg wordt vervoerd;<br>
• de overeenkomst geldt als overeenkomst voor vervoer van goederen door de lucht indien de zending effectief door de lucht wordt vervoerd;<br>
• de overeenkomst geldt als overeenkomst voor vervoer van goederen over zee indien de zending effectief over zee wordt vervoerd;<br>
• de overeenkomst geldt als overeenkomst voor het verrichten van overige diensten indien zij betrekking heeft op diensten anders dan vervoer
<h3 id="artikel-5-aanbiedingsvoorwaarden">Artikel 5 - Aanbiedingsvoorwaarden</h3>
5.1 Als Vervoersdocumenten hanteert Travel Light de documenten van haar onderaannemer TNT Express, welke worden overhandigd door Travel Light. Zowel de heen als retour Zendingen zijn voorzien van een volledig ingevuld vervoersdocument. Het vermelden van de Nederlandse afzendergegevens is een verplicht onderdeel van het vervoersdocument.<br>
5.2 Zendingen dienen op het door de Afzender bepaalde adres te worden aangeboden aan TNT Express.
<h3 id="artikel-6-gevaarlijke-goederen-veiligheid">Artikel 6 - Gevaarlijke goederen &amp; veiligheid</h3>
6.1 Behalve in de omstandigheden zoals aangegeven in paragraaf 6.2 hieronder, vervoeren we geen gevaarlijke goederen of goederen die naar ons inzicht gevaarlijk zijn, noch verrichten we overige diensten met betrekking tot dergelijke goederen. Gevaarlijke goederen omvatten, maar zijn niet beperkt tot de goederen die worden genoemd in de technische instructies van de Internationale Burgerluchtvaart organisatie (ICAO), de voorschriften omtrent gevaarlijke goederen van de International Air Transport Association (IATA), de International Maritime Dangerous Goods (IMDG) code, de voorschriften van het Europees Verdrag betreffende het internationaal vervoer van gevaarlijke goederen over de weg (ADR) of enig andere nationale of internationale regelgeving die van toepassing is op het vervoer van, of het verrichten van overige diensten met betrekking tot gevaarlijke goederen.<br>
6.2 Zendingen die door ons worden vervoerd of verwerkt, kunnen aan een veiligheidscontrole worden onderworpen, onder meer door het gebruik van röntgenstraling, detectietechnieken voor explosieven of andere screeningmethoden. Uw Zending kan tijdens het vervoer worden geopend zodat dat de inhoud kan worden onderzocht.<br>
6.4 We aanvaarden geen zendingen die verboden goederen bevatten.<br>
6.5 Om veiligheids- en/of douaneredenen, kunnen we worden verzocht gegevens te delen, waaronder uw persoonlijke gegevens voor uw zending, met de autoriteiten in het land van bestemming van uw zending of in een land waardoor uw zending wordt vervoerd.
<h3 id="artikel-7-recht-op-inspectie">Artikel 7 - Recht op inspectie</h3>
De opdrachtgever stemt ermee in dat wijzelf of enige overheidsinstantie, waaronder douane en veiligheidsdiensten, uw zending op elk moment kunnen openen en inspecteren. Wij zorgen dat dit geen invloed heeft op de verwachte afleverdatum. Wanneer u akkoord gaat met onze algemene voorwaarden accepteert u eventuele vertraging ten gevolge van deze inspectie en bent u zich bewust dat hier geen financiële compensatie in welke vorm dan ook voor gearrangeerd wordt.
<h3 id="artikel-8-betaling">Artikel 8 - Betaling</h3>
Betaling van de verschuldigde vergoeding geschiedt tijdens het boekingsproces binnen de online omgeving www.travel-light.nl.
<h3 id="artikel-9-8211-onjuist-adres-en-postbusnummers">Artikel 9 – Onjuist Adres en Postbusnummers</h3>
9.1 Als we een Zending niet kunnen afleveren omwille van een onjuist adres, zullen we alles doen wat redelijkerwijs in ons vermogen ligt om het juiste adres te achterhalen. We stellen u op de hoogte van de aanpassing en leveren de zending af op het juiste adres, of ondernemen althans een poging daartoe.<br>
9.2 Leveringen aan postbusadressen worden niet aanvaard.
<h3 id="artikel-10-verzendingen">Artikel 10 - Verzendingen</h3>
10.1 Vervoerd worden koffers en pakketten (waarbij in het geval van onderstaande pakketten de transportdoos wordt aangeleverd door Travel Light) met de volgende maten en gewichten per categorie en per pakket:<br>
a. Categorie a: Enkele set Ski’s of Snowboard, max. 30 Kg, max .lengte 180 cm; (Conform de door Travel Light aangeleverde transportdoos)<br>
b. Categorie b: Dubbel set Ski’s en/of Snowboards, max 30 Kg, max. lengte 180 cm; (Conform de door Travel Light aangeleverde transportdoos)<br>
c. Categorie c: Enkele Golf stand bag, max 30 kg, max. lengte 120 cm; (Conform de door Travel Light aangeleverde transportdoos)<br>
d. Categorie d: Kitesurf uitrusting, max 30 Kg, max. lengte 145 cm; (Conform de door Travel Light aangeleverde transportdoos)<br>
e. Categorie e: Koffers, max 20 Kg, max. Lengte 70 cm, breedte 48 cm, hoogte 31 cm;<br>
f. Categorie f: Koffers, max 30 Kg;<br>
g. Categorie g: Fiets in koffer, max 30 Kg, max. Lengte 180 cm, breedte 95 cm, hoogte 22 cm; (Conform de instructie van Travel Light, geen uitstekende (losse) onderdelen en volledig dicht geplakte doos)<br>
10.2 Individuele pakketten worden geaccepteerd door onze vervoerder en altijd nagemeten / gewogen. U dient de pakketten dus voorafgaand aan de verzending te wegen en binnen de maten te houden. Indien de maten en/of het gewicht wordt overschreden zal het verschil in rekening worden gebracht.<br>
10.3 Het is de taak van de Afzender om te zorgen voor de juiste adressering. Het is de taak van Travel Light om te zorgen voor de juiste labels/vervoersdocumenten.<br>
10.4 Transport van koffers via Travel Light vereist een zodanige verpakking, dat deze de goederen voldoende beschermt tegen de belasting veroorzaakt door automatische sorteermachines en mechanische overslag.
<h3 id="artikel-11-opdrachtbevestiging-en-annulering">Artikel 11 - Opdrachtbevestiging en annulering</h3>
Nadat de order online is geregistreerd wordt aan de afzender het ordernummer verstrekt per e-mail en is uw order geaccepteerd voor verdere verwerking. Uiterlijk 9 dagen voor de ophaaldatum in Nederland en 3 dagen voor uw vertrek op de plaats van bestemming kan uw opdracht nog aangepast worden in de online omgeving www.travel-light.nl of per telefoon. U dient rekening te houden met het volgende;<br>
• Uiterlijk 9 dagen voor de ophaaldatum in Nederland kunt u uw opdracht annuleren. Dit dient te alle tijden telefonisch te gebeuren. Voor het annuleren van een opdracht brengen wij € 35,00 incl. BTW in rekening en zal in mindering gebracht worden op de crediteringsnota.
<h3 id="artikel-12-weigering-opschorting-of-staking-van-vervoer">Artikel 12 - Weigering, opschorting of staking van vervoer</h3>
12.1 Travel Light en onderaannemer TNT Express kunnen het vervoer van een Zending, desgevraagd onder opgave van redenen, weigeren, opschorten of staken indien:<br>
a. de Afzender niet voldoet aan de voorwaarden die Travel Light heeft gesteld voor acceptatie tot vervoer van de Zending (ten aanzien van bijvoorbeeld: betaling, plaats van aanbieding, verstrekking of vermelding van gegevens, gebruik van een Vervoersdocument, gebruik van een barcode, verpakking, inhoud, gewicht en afmetingen);<br>
b. het vervoer van de Zending gevaar kan opleveren voor personen of zaken; dit geldt in ieder geval voor vervoer van goederen, waarop de nationale- of internationale wet- en regelgeving inzake vervoer van gevaarlijke stoffen van toepassing is; (zie ook Artikel 6)<br>
c. het vervoer bij wet of overheidsvoorschrift is verboden, of Travel Light aanwijzingen heeft dat het vervoer strijdig kan zijn met de wet of een overheidsvoorschrift; (zie ook Artikel 6)<br>
d. Travel Light een andere gegronde reden tot weigering, opschorting of staking heeft, waaronder maar niet beperkt tot natuurrampen, oorlogen of gewapende conflicten, (werk)stakingen.<br>
12.2 In geval van weigering of staking van het vervoer van een Zending stelt Travel Light de Afzender, voor zover mogelijk, in staat weer in het bezit te komen van de Zending alsmede van de eventueel daarbij overgelegde bescheiden.
<h3 id="artikel-13-wijze-van-aflevering">Artikel 13 - Wijze van aflevering</h3>
13.1 Algemeen, Travel Light zorgt voor de verzending van uw Zending middels de inzet van vrachtvervoerders, het aannemen, de overslag en het doen bezorgen van pakketten;<br>
a. Tenzij dit redelijkerwijs niet van Travel Light gevraagd kan worden, wordt er op alle dagen van de week afgeleverd, met uitzondering van de zaterdagen, zondagen en de algemeen erkende feestdagen nationaal en internationaal.<br>
b. Aflevering vindt plaats op het op de Zending vermelde adres.<br>
c. Na acceptatie ten vervoer kunnen, tenzij uitdrukkelijk anders met de Afzender of de Geadresseerde overeengekomen, het afleveradres niet meer worden gewijzigd. Zolang de zending nog niet is afgeleverd kan de Afzender Travel Light verzoeken deze retour te zenden. Travel Light zal zich dan inspannen gevolg te geven aan zo’n verzoek.<br>
d. Ten aanzien van vervoer van (uitgaande) Internationale Zendingen is de nagestreefde overkomstduur afhankelijk van het land of het gebied van bestemming.<br>
13.2 Wijze van aflevering, de aflevering kan plaatsvinden door uitreiking aan de Geadresseerde, een volwassen huisgenoot van de Geadresseerde of de gemachtigde van de Geadresseerde, dan wel aan een medewerker van de organisatie die op de Zending als Geadresseerde is aangeduid. Dit geldt voor de meeste Aanvullende diensten.<br>
13.3 Eerste en tweede aflevering; Bewaring<br>
a. Indien aflevering door middel van deponering in een daartoe geschikte voorziening of door uitreiking aan de Geadresseerde of aan een andere daartoe geschikte persoon niet mogelijk is, zal Travel Light de Zending voor een periode van maximaal drie weken in bewaring nemen. De voorwaarden van bewaring van uitgaande Internationale Zendingen kunnen van land tot land verschillen.<br>
b. Wanneer aflevering niet mogelijk is gebleken en Travel Light de zending daarop in bewaring neemt, zal de Geadresseerde daarover steeds schriftelijk worden geïnformeerd, met vermelding van: de eventuele mogelijkheden tot een tweede afleverpoging, de termijn en locatie van bewaring, de mogelijke tijdvakken waarbinnen de zending op de locatie van bewaring kan worden afgehaald, en de gang van zaken indien binnen de bewaartermijn- om welke reden dan ook – geen aflevering plaats vindt.<br>
c. Travel Light neemt geen Zendingen in bewaring die kennelijk of naar Travel Light vermoedt een bederfelijke inhoud hebben.<br>
13.4 Als we de aflevering van een zending niet kunnen voltooien, om eender welke reden, zullen we proberen op het adres van de ontvanger een bericht achter te laten met de mededeling dat een poging tot aflevering werd ondernomen en met een indicatie van de plaats waar de zending zich bevindt. Als ook bij de tweede poging de zending niet kan worden afgeleverd, of als de ontvanger weigert de zending in ontvangst te nemen, zullen we proberen u te contacteren om een passende vervolgstap overeen te komen. Indien we niet binnen een redelijke termijn na onze tweede afleverpoging noch van u noch van de ontvanger nadere instructies ontvangen, dan stemt u ermee in dat we de inhoud van de zending kunnen vernietigen of verkopen, zonder enige verdere aansprakelijkheid tegenover u.
<h3 id="artikel-14-uw-verplichtingen">Artikel 14 - Uw verplichtingen</h3>
14.1 De Zending (waaronder maar niet beperkt tot het gewicht en het aantal colli), naar behoren is gelabeld en dat het label of de labels door u stevig op een prominente en voor ons duidelijk zichtbare plek aan de buitenzijde van de Zending zijn aangebracht;<br>
14.2 De contactgegevens van de ontvanger volledig vermeld zijn tijden het verstrekken van de opdracht en het aangaan van de overeenkomst;<br>
14.3 De inhoud van de Zending veilig en zorgvuldig door u is voorbereid en verpakt ter bescherming tegen risico’s van vervoer of vervulling door ons;
<h3 id="artikel-15-omvang-van-onze-aansprakelijkheid">Artikel 15 - Omvang van onze aansprakelijkheid</h3>
Onder voorbehoud van artikel 16 hieronder is onze aansprakelijkheid voor verlies, beschadiging of vertraging van uw zending of een deel ervan als gevolg van het vervoer beperkt als volgt:<br>
a. Als uw zending geheel of gedeeltelijk per luchtvracht wordt vervoerd en haar uiteindelijke bestemming of een tussenstop in een ander land ligt dan het land van vertrek, dan valt uw zending onder het Verdrag van Warschau (1929) of het Verdrag van Warschau zoals gewijzigd door het Protocol van Den Haag (1955) en/of het Protocol van Montreal nr. 4 (1975), of het Verdrag van Montreal (1999), al naargelang welk van deze dwingend van toepassing is. Deze internationale verdragen bepalen en beperken onze aansprakelijkheid voor verlies, beschadiging of vertraging van uw zending tot 19 bijzondere trekkingsrechten per kilo (dat is ongeveer 20 euro per kilo, onder voorbehoud van wijzigingen van de wisselkoers).<br>
b. Als we uw zending over de weg vervoeren in, naar of vanuit een land dat is aangesloten bij het Verdrag betreffende de overeenkomst tot internationaal vervoer van goederen over de weg uit 1956 (CMR), dan wordt onze aansprakelijkheid voor verlies of beschadiging van uw zending bepaald door het CMR en is zij derhalve beperkt tot 8,33 bijzondere trekkingsrechten per kilo (dat is ongeveer 10 euro per kilo, onder voorbehoud van wijzigingen van de wisselkoers). In het geval van opzet en/of bewuste roekeloosheid kan de vervoerder zich niet op deze limiet beroepen. Zie artikel 8:1108 van het Burgerlijk Wetboek.
<h3 id="artikel-16-uitsluiting-van-aansprakelijkheid">Artikel 16 - Uitsluiting van aansprakelijkheid</h3>
16.1 Travel Light levert in samenwerking met haar vervoerder TNT Express een on-time performance van 98,5% op alle Zendingen naar de door ons geselecteerde bestemmingen.&nbsp;<br>
16.2 Travel Light is niet aansprakelijk wanneer wij enige verplichting tegenover u niet vervullen als gevolg van:<br>
a. Omstandigheden buiten onze controle, welke onder overmacht vallen. Zie artikelen 8:23 BW (binnenlands vervoer van goederen), 8:1098, 8:1108, 8:1110, 8:1114, 8:1115, 8:1117 en 8:1118 BW (vervoer over de weg) artikel 8:1353, 8:1354 en 8:1359 BW (goederenvervoer door de lucht. Zie de artikelen ook voor de wettelijke verplichtingen van onze klanten, het niet nakomen van deze verplichtingen daarvan heeft gevolgen voor de aansprakelijkheid van de vervoerder.<br>
b. Handelingen of omissies van uw kant of derden:<br>
• Verzuim van uw kant (al dan niet door toedoen van enige andere partij met een vermeend belang in de Zending) in het vervullen van uw verplichtingen krachtens deze Algemene Voorwaarden, met name de hiervoor in artikel 14 vermelde vaarborgen;<br>
• De schade door handelingen of nalatigheid van de Afzender, de ontvanger of hun assistenten is opgetreden;<br>
• De schade te wijten is aan het gebruikte verpakkingsmateriaal in het geval van koffers en fietsen;<br>
• Handelingen of omissies van functionarissen van douaneautoriteiten, luchtvaartmaatschappijen, luchthavens of overheidsinstanties.<br>
c. De aanwezigheid van verboden goederen in de Zending, zelfs al hebben we de Zending abusievelijk aanvaard.<br>
d. Onze weigering om illegale betalingen in uw naam uit te voeren.<br>
16.3 Travel Light is op geen enkele wijze gebonden aan de inhoud zoals door de klant gesteld en/of aangegeven in de vrachtbrief.
<p>16.4 Travel Light is op geen enkele wijze verantwoordelijk dat de zending ook daadwerkelijk wordt aangenomen door de ontvangende partij. De klant is zelf verantwoordelijk voor het informeren van de ontvangende locatie betreffende de aflevering. Dit dient voor het maken van de boeking overeengekomen te zijn met de ontvangende partij. Na getekend ontvangst ligt de verantwoordelijkheid bij de ontvangende partij of de klant.&nbsp;<br>
&nbsp;</p>
<h3 id="artikel-17-waardevolle-goederen">Artikel 17 - Waardevolle goederen</h3>
Waardevolle goederen zoals edelstenen, edelmetalen, juwelen, geld, handelspapieren, onbeschermd meubilair, glas of porselein, kunstvoorwerpen, antiek en belangrijke documenten zoals paspoorten, tenders en aandeel- of optiebewijzen behoren niet via ons distributienetwerk te worden verstuurd, aangezien in dat netwerk apparatuur wordt gebruikt voor mechanische verwerking en geautomatiseerde sortering en zendingen meerdere malen op voertuigen worden geladen en gelost, wat tot verlies en/of beschadiging zou kunnen leiden.
<h3 id="artikel-18-verzekering">Artikel 18 - Verzekering</h3>
De Afzender is verantwoordelijk voor het op een juiste wijze verzekeren van de verzending
<h3 id="artikel-19-procedure-voor-claims">Artikel 19 - Procedure voor claims</h3>
Als u een claim wilt indienen in verband met een verloren, beschadigde Zending of enige andere schade, dient dat te gebeuren in overeenstemming met eventuele toepasselijke verdragen alsmede met onderstaande procedure. Bij gebreke daarvan, behoudt Travel Light zich het recht voor uw claim af te wijzen:<br>
19.1 U dient ons schriftelijk in te lichten over het verlies of de beschadiging binnen 21 dagen (i) na de aflevering van de zending, (ii) na de datum waarop de zending had moeten worden afgeleverd of, 21 dagen na de datum waarop u redelijkerwijs het verlies of de schade had kunnen vaststellen;<br>
19.2 Binnen de 21 dagen nadat u uw claim aan ons hebt overgemaakt, dient u deze claim te onderbouwen door ons alle relevante informatie toe te sturen over de zending en/of het verlies, of de schade;<br>
19.3 We gaan ervan uit dat de zending in goede staat werd afgeleverd, behalve in het geval van verlies c.q. niet afleveren van de Zending, tenzij de ontvanger bij het aannemen van de zending op ons leveringsbewijs melding van schade heeft gemaakt. Opdat we een schadeclaim in behandeling zouden kunnen nemen, dienen de inhoud van uw zending en de originele verpakking aan ons beschikbaar te worden gesteld voor inspectie;<br>
19.4 Behalve voor zover anders bepaald door enige toepasselijke verdragen en/of wetgeving, komt uw recht tot het claimen van schade te vervallen als u de kwestie niet binnen 1 jaar na de afleverdatum van de zending, na de datum waarop de zending afgeleverd had moeten worden of na de datum waarop het vervoer werd beëindigd dan wel, indien de claim betrekking heeft op overige diensten, niet binnen 1 jaar na de datum waarop u redelijkerwijs het verlies, de schade of de vertraging had kunnen vaststellen, aan de rechter hebt voorgelegd;<br>
19.5 Bij acceptatie door ons van uw claim of een deel ervan garandeert u ons dat enige derde partij die een belang heeft in de zending afstand heeft gedaan van enig recht, verhaal of redres waartoe zij gerechtigd zouden kunnen zijn door subrogatie of anderszins;
<h3 id="artikel-20-wijziging-voorwaarden">Artikel 20 - Wijziging voorwaarden</h3>
Travel Light heeft het recht deze voorwaarden te wijzigen en/of aan te vullen. Tenzij anders bepaald of overeengekomen zijn wijzigingen en aanvullingen tot nader order van toepassing op alle Vervoerovereenkomsten die op en na de door Travel Light bekend gemaakte datum van invoering van de wijzigingen en/of aanvullingen tot stand komen.
<p>Travel Light B.V.<br>
14 januari 2015 ’s-Gravenhage<br>
KvK: 53276264</p>
<h3 id="artikel-21-toepasselijkheid-regelgeving">Artikel 21 - Toepasselijkheid regelgeving</h3>
Op alle werkzaamheden en overeenkomsten van Travel Light zijn van toepassing;<br>
a. Nationaal vervoer over de weg: De Algemene Vervoerscondities 2002 (AVC), steeds de meest actuele versie daarvan zoals door de Stichting Vervoeradres gepubliceerd en gedeponeerd ter Griffie van de rechtbanken te Amsterdam en Rotterdam;<br>
b. Internationaal vervoer over de weg: Verdrag betreffende de Overeenkomst tot internationaal vervoer van goederen over de weg, in de door Nederland bekrachtigde versie (CMR);<br>
c. (Inter-) Nationaal vervoer over zee: De overeenkomst geldt als overeenkomst voor vervoer van goederen over zee indien de Zending effectief over zee wordt vervoerd;<br>
d. Luchtvervoer: Verdrag tot het brengen van eenheid in enige bepalingen inzake het internationaalluchtvervoer, in de door Nederland bekrachtigde versie (Verdrag van Warschau).<br>
<br>
<p>	
<a href="Downloads/algemene_voorwaarden_thuiswinkel_incl_aanvullendevoorwaaden_travel_light-2016.pdf" class="xcms_download" onclick="window.open(this.href, '', ''); return false;">Download hier onze algemene voorwaarden incl aanvullende voorwaarden</a></p>
<p><i>Laatste update: 6 mei 2016</i></p>
<!--end--></div> </p>
          

           

          </div>
        </div>

        
      </div>
    </section>


    
    




    <?php include 'footer.php'; ?>