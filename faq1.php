<?php include 'header.php'; ?>

	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />
    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<!--	<title>Travel Light - Veelgestelde vragen</title>
	
<body>-->
<div class="container">
    <br />
    <br />
    <br />
    <br />

    <div class="panel-group" id="accordion">
        <div class="page-header">
					<center><h1>FAQ <small>Veelgestelde vragen.</small></h1></center>
			</div>
		<blockquote>
        Hieronder vind je de meest gestelde vragen en antwoorden met betrekking tot <strong>Travel Light</strong>. Klik op de vraag om het antwoord te krijgen. Mocht je je antwoord niet kunnen vinden dan kun je altijd contact met ons zoeken via de e-mail of telefoon!  
    </blockquote>		
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Waarom heft Travel-light landentoeslag?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    Om onze service mogelijk te maken door de hele Europese Unie werken we samen met lokale chauffeurs. Daarom moeten we een eenmalige landentoeslag rekenen. Deze zit niet in de prijs verwerkt omdat deze per land varieert. In sommige regio’s is het voor ons voordeliger dan in anderen. 

Het is een eenmalige toeslag die we per boeking in rekening brengen, ongeacht hoeveel je meeneemt. 


Tot eind 2016 rekenen wij geen landentoeslag! Dit doen we om meer mensen kennis te laten maken met Travel Light. Wij hebben ons prijspeil zo aangepast dat jij in 90% van alle gevallen voordeliger uitkomt door het op te sturen dan zelf meenemen met bijv. Transavia of Ryanair. 

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Wat als jullie te laat zijn?</a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="panel-body">
                    Momenteel bieden wij een 99,9% on-time delivery. Wij streven ernaar je bagage uiterlijk een dag voordat jij aankomt te leveren aan je hotel. Uiteraard gaat het bij ons ook wel eens mis en zijn we afhankelijk van externe invloeden zoals het weer, verkeersopstoppingen en uiteraard de bereidwilligheid van jouw hotel. Indien wij te laat zijn, zullen wij alles op alles zetten om zo snel als mogelijk te leveren. Uiteraard houden we je dan uitvoerig op de hoogte van de voortgang. Het positieve is wel dat je dit vaak al weet voordat jezelf vertrekt waardoor je de noodzakelijke zaken in je handbagage kunt stoppen. 
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">Zijn er nog bijkomende kosten voor het thuis en/of in het hotel ophalen?</a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="panel-body">
                    Er zijn geen bijkomende kosten. De prijs die wij communiceren is de 'all-in' prijs voor de gehele trip. Wij leveren een zak of doos waarin jouw bagage wordt verstuurd.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Wat als ik niet thuis ben ten tijden van het ophalen?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    Wij communiceren duidelijk op verschillende manieren wanneer wij de verzending komen ophalen. Als u te laat of niet aanwezig bent wordt de bagage ook niet meegenomen. Zorg er dus voor dat er iemand aanwezig is en dat de verzending klaar staat.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Wat kan ik allemaal opsturen?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    Wij blijven onze dienstverlening optimaliseren en het assortiment uitbreiden. Momenteel bieden wij de mogelijkheid om de volgende bagage te versturen:
                    <ul>
                        <li>Koffers</li>
                        <li>Kitesurf materiaal</li>
                        <li>Ski's</li>
                        <li>Snowboards</li>
                        <li>Fietsen</li>
                        <li>Golfsets</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Waarom verstuurt Travel Light alleen koffers, kitesurf materiaal, ski's / snowboards incl. schoenen, fietsen, golfsets?</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    Wij versturen zoveel mogelijk in dozen die hier speciaal voor zijn ontworpen. Hierdoor zijn je spullen goed beschermt.
                    <br />
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">Mag ik meer meenemen in de doos dan alleen mijn fiets, kite-, ski- of golfmateriaal?</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                  Ja hoor, zolang het gewicht niet boven de 30kg komt en de doos niet bol staat. De chauffeur neemt de doos mee, dus leg het klaar zodat de chauffeur niet lang hoeft te wachten.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">Wat zijn de maximale maten die verstuurd kunnen worden?</a>
                </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse">
                <div class="panel-body">
             lengte 180cm | breedte 95cm | hoogte 22cm
Valt de jouwe er buiten, neem dan contact met ons op.
                </div>
            </div>
        </div>
<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">Ik wil mijn fiets versturen in een doos, waar moet ik op letten?</a>
                </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse">
                <div class="panel-body">
                 Het versturen van een fiets in een doos kan niet boven de 180x90x22cm (voor meer info over fietsen <a href="/fiets">klik hier</a>). Het liefst halen wij je fiets op bij een fietsen handel. Zij hebben de kennis om je fiets goed in te pakken waardoor het minder snel beschadigd, daarnaast is het voor jou makkelijker. Let op dat de doos niet bol staat of uitstekende delen heeft. Een mooi voorbeeld hoe het niet moet zie je op onderstaande foto.<br>
<img src="/img/Fietsdoos_verkeerd_ingepakt.png" width="250" height="500" alt="" title="" /><br>
Hierdoor is de doos minder sterk en vergroot je de kans op schade. Twijfel je of een doos sterk genoeg is? Stuur ons dan een foto via de mail. De doos zorgt voor bescherming van je dierbare spullen. Wij hebben helaas geen elfjes in dienst dus de fiets moet wel tegen een paar stootjes kunnen. Wil je er zeker van zijn dat je fiets heel aankomt? Dan adviseren wij ten alle tijden een fietskoffer te huren/gebruiken.
                </div>
            </div>
        </div>
<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">Als mijn spullen kwijtraken of worden gestolen, ben ik dan verzekerd?</a>
                </h4>
            </div>
            <div id="collapseNine" class="panel-collapse collapse">
                <div class="panel-body">
                 Wij zijn een boekingskantoor en zijn daarom op geen enkele wijze verantwoordelijk voor het verlies van of schade aan een pakket of de inhoud ervan. Wij zullen kosteloos voor onze klanten een claimprocedure opstarten bij onze transporteurs. In het geval van verlies en/of diefstal binnen ons proces, dekken onze transporteurs tot &#8364;125,- per bagage stuk. Deze dekking zit standaard bij de boeking inbegrepen. Eventuele overige schade of verlies dien je te claimen bij je reis- of inboedelverzekering. Wij adviseren je dan ook voor vertrek na te gaan of jouw verzekering dit dekt. Travel Light biedt nu geen additionele verzekering aan.
Verder is de kans op diefstal en/of schade zeer klein. In samenwerking met TNT express leveren wij een betere performance dan bijvoorbeeld de luchtvaartmaatschappijen. Mocht uw verzending onverhoopt toch kwijt raken, dan stellen wij uiteraard alles in werking om de verzending terug te vinden en alsnog de levering te laten plaatsvinden.
Veelal ligt de oorzaak bij de vervoersdocumenten, dat deze niet juist zijn opgeplakt. Kijk op onderstaande instructie video hoe de labels aangebracht dienen te worden. Daarnaast zijn onze dozen bedrukt met Travel Light logo en staat er een telefoonnummer op. Mocht er geen label op zitten en ze vinden de bagage, dan sturen ze hem naar ons Europese hoofdkantoor in Scheveningen.
<iframe src="//www.youtube.com/embed/FxeD0-otLw8?wmode=transparent&amp;rel=0&amp;autoplay=0&amp;loop=0&amp;showsearch=0" allowfullscreen="" style="width: 620px; height: 387.5px;"></iframe>
                </div>
            </div>
        </div>
        
<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen2">Mijn spullen passen niet in de inpakzak, wat moet ik nu doen?</a>
                </h4>
            </div>
            <div id="collapseTen2" class="panel-collapse collapse">
                <div class="panel-body">
                    Boek dan een koffer bij ;-)
In principe zijn de zakken zo ontworpen dat al je materiaal er in past. Zorg er bijvoorbeeld voor dat bij ski&#8217;s je bindingen zijn ingeklapt of in het geval van snowboard(s) zijn losgemaakt, dit scheelt vaak veel ruimte. Wanneer je te veel extra spullen erbij hebt gestopt dan kan het zijn dat de doos of zak te breed, hoog of zwaar is. Bekijk de onderstaande how to video, dan wordt het helemaal duidelijk.
<iframe width="640" height="400" src="//www.youtube.com/embed/FxeD0-otLw8?wmode=transparent&amp;rel=0&amp;autoplay=0&amp;loop=0&amp;showsearch=0" allowfullscreen=""></iframe>                
</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven2">Hoe kan ik mijn bagage volgen via Track & Trace?</a>
                </h4>
            </div>
            <div id="collapseEleven2" class="panel-collapse collapse">
                <div class="panel-body">
                Bij iedere verzending verkrijg je een Track and Trace code van TNT Post waarmee je eenvoudig je verzending kunt volgen.
                </div>
            </div>
        </div>

        
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwelve">Ik heb geen koffer/tas voor mijn fiets. Kan ik hem dan wel opsturen?</a>
                </h4>
            </div>
            <div id="collapseTwelve" class="panel-collapse collapse">
                <div class="panel-body">
            Om te voorkomen dat je fiets beschadigd, moet je deze in een doos of fietskoffer versturen.
Op www.mhverhuur.nl kun je koffers kopen en huren die wij accepteren.
                </div>
            </div>
        </div>
            </div>
</div>
 
    
  <?php include 'footer.php'; ?>