<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/snow.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Ski's/snowboard <small>De sneeuw in zonder bagage.</small></h1>
			</div>
          
            <div class="col-sm-10 col-sm-offset-1">
                
                <p class="text-justify txtDrk">Wij versturen zowel ski's als snowboards zolang de de maximale lengte van ski’s en/of snowboards niet boven de 180 cm uitkomt. Wij kennen twee type dozen. Wanneer je twee sets met schoenen boekt krijg je maar één doos. Deze is uiteraard groter dan bij een enkele set. </p>
                <p class="text-justify txtDrk"><b>Let op:</b> zorg ervoor dat de remmen bij ski's omhoog staan. Wij leveren hiervoor extra dikke elastieken in het welkomstpakket. Wij doen uiteraard niet moeilijk wanneer je meer wilt meenemen dan puur je materiaal, de ruimte die je overhoud in de doos kun je zelf opvullen met bijvoorbeeld kleding, zolang de doos niet bol staat en niet zwaarder zal wegen dan toegestaan.  </p>
                
                <div class="text-center">
                    <p class="text-justify txtDrk"><b>Voor meer informatie en uitleg over onze dienst bekijk onze inpak video's</b></p>   
                </div>

                <div class="row">
                    <div class="col-6 col-6 col-sm-6 vdo"><iframe class="iframeClass" width="560" height="315" src="https://www.youtube.com/embed/bYnlM7eJzNI?list=PLuAtrvFtSay3vkfCAEpMIuZCR8KhOfYsr" allowfullscreen></iframe>  </div>
                    <div class="col-6 col-6 col-sm-6 vdo"><iframe class="iframeClass" width="560" height="315" src="https://www.youtube.com/embed/A-9zKNVwKe4?list=PLuAtrvFtSay3vkfCAEpMIuZCR8KhOfYsr" allowfullscreen></iframe>  </div>
                    
                </div>

                <h3>Verzekering?</h3>
                <p class="text-justify txtDrk">Wil je nu extra zekerheid en goed verzekerd op vakantie gaan, dan adviseren wij de Nederlandse Ski Vereniging. Zij leveren de meest complete doorlopende reisverzekering voor wintersporters. </p>

                <div class="text-center">
                 
                 <div class="image"> <a href="https://www.wintersport.nl/reisverzekering/?ref=travel-light2" target="_blank"> <img src="img/Snowskiverzekering.png" alt="verzekering" class="img-responsive"> </a></div>
                 
                </div>
                <div class="spacer20 clearfix"></div>
                <p class="text-justify txtDrk">De Nederlandse Ski Vereniging weet wat er allemaal kan misgaan tijdens je wintersport. Zij zijn al sinds 1978 actief op het verzekeringsvlak om die risico’s zo optimaal mogelijk voor je af te dekken. We durven te stellen dat zij de beste doorlopende reisverzekering voor wintersporters hebben ontwikkeld.</p>
                <h3>Appartementen</h3>
                <p class="text-justify txtDrk">Ga je naar een appartement? In de volgende gebieden kunnen we ook op bagage drop-off punten leveren. Wanneer je wenst dat Travel Light je wintersportbagage aflevert bij een conciërge drop-off punt, dien je rekening te houden met een lokale dagvergoeding. Deze varieert tussen de 3 en 8 euro per dag en verschilt per regio.</p>
                <h3>Wij bieden in de volgende gebieden een drop-off faciliteit aan: </h3>
                <p class="text-justify txtDrk"><b>Frankrijk / Oostenrijk / Italie</b></p>
                <p class="text-justify txtDrk">Wij leveren af bij alle 850 appartement recepties van de grote skigebieden! </p>
                
                <p class="text-justify txtDrk"><b>Boekt u bij een privé huis of AirBnB kunt u ook afleveren bij:</b></p>
                <p class="text-justify txtDrk"><a href="https://www.google.nl/maps/place/Office+de+Tourisme+de+Tignes/@45.469319,6.9049683,17z/data=!3m1!4b1!4m5!3m4!1s0x4789732cabfa891d:0x485f049430274976!8m2!3d45.469319!4d6.907157" target="_blank">Tignes</a> - Afleveradres: Office de Tourisme, Tignes, 73320, Place Centrale 1, Frankrijk. Tel nr: +33 4 79 40 04 40</p>
            
                
                <p class="text-justify txtDrk"><a href="https://www.google.nl/maps/place/Office+du+Tourisme+de+Val+d'Is%C3%A8re/@45.4492112,6.9780357,17z/data=!3m1!4b1!4m5!3m4!1s0x47890be8d2918a3f:0xdbf52a5cc1090823!8m2!3d45.4492112!4d6.9802244" target="_blank">Val d’isere</a> - Afleveradres: Concergerie service bij TO, Val d Isere, 73150,  Place Jacques Mouflier 1, Frankrijk. Tel nr: +33 4 79 06 06 60</p>
                
                <p class="text-justify txtDrk"><a href="https://www.google.nl/maps/place/Alpes+Attitude/@46.1809047,6.7019104,17z/data=!3m1!4b1!4m5!3m4!1s0x478c1d4159c430e3:0x5a719342aa28ac26!8m2!3d46.1809047!4d6.7040991" target="_blank">Avoriaz, Morzine </a>- Afleveradres: Skimium ALPES ATTITUDE, Morzine, 74110, Place du Baraty 19 Frankrijk. Tel nr +33 4 50 92 95 13</p>
                
                
                <p class="text-justify txtDrk"><a href="https://www.google.nl/maps/place/Soci%C3%A9t%C3%A9+Gestion+Equipement+Vall%C3%A9e+de+Belleville+(SOGEVAB)/@45.3116951,6.5432321,14z/data=!4m8!1m2!2m1!1ssogevab+Val+Thorens+73440+VAL+THORENS!3m4!1s0x0:0xfae056088768b34a!8m2!3d45.2979242!4d6.5835679" target="_blank">Val Thorens</a> - Afleveradres: SOGEVAB Lethitia, VAL THORENS, 73440, Maison de Val Thorens 1, Frankrijk. Tel nr +33 4 79 00 00 18</p>
                
                
                <p class="text-justify txtDrk"><a href="https://www.google.nl/maps/place/Altapura/@45.2971415,6.5752148,17z/data=!4m8!1m2!2m1!1sAlta+Pura,+VAL+THORENS,+73440,+Rue+du+Soleil+1,+Frankrijk.!3m4!1s0x47898658fc97def1:0x8c206105124a589b!8m2!3d45.29728!4d6.574612" target="_blank">Val Thorens</a> - Afleveradres: Alta Pura, VAL THORENS, 73440, Rue du Soleil 1, Frankrijk. Tel nr +33 4 50 91 49 73</p>
                
                
                <p class="text-justify txtDrk">Les Menuires - Afleveradres: Belleville Prestige Conciergerie, LES MENUIRES, 73440, Résidence le Brelin B.P 15, Frankrijk. Tel nr +33 6 59 71 46 45</p>
                
               
                <p class="text-justify txtDrk">Les Menuires - Afleveradres: SOGEVAB Lethitia, LES MENUIRES, 73440, Quartier Croisette 1, Frankrijk. Tel nr +33 4 79 01 08 83 </p>
                
                <p class="text-justify txtDrk"><b>Let op:</b> Bovenstaande locaties zijn bekend met Travel Light en hebben aangegeven dat zij uw bagage willen ontvangen en in bewaring nemen. U dient zelf contact op te nemen met de betreffende locatie om uw aankomst aan te kondigen. In bijna alle gevallen zult u lokaal een vergoeding moeten betalen voor de stalling van uw bagage. De locaties vallen niet onder de verantwoordelijkheid en service van Travel Light.</p>
            </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>