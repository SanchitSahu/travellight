<?php include 'header.php'; ?>

    
    


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Over Travel Light <small>Wij laten je bagagevrij reizen.</small></h1>
			</div>
          
          <div class="col-sm-10 col-sm-offset-1">
            <p class="text-justify txtDrk">Travel Light is powered by TNT Express en gevestigd op Scheveningen. Het versturen van bagage lijkt nieuw maar dat is het niet. Wij maken de reiservaring aangenamer door bagage bij je thuis op te halen en klaar te zetten op jouw plek van bestemming. Hierdoor wordt reizen eenvoudiger en bovenal meer ontspannen. Of je nu met vrienden gaat golfen in Portugal, in Oostenrijk een weekendje gaat skiën, of gewoon met je gezin op vakantie gaat. Travel Light zorgt ervoor dat je binnen de Europese Unie van vertrek tot terugkomst ontspannen kunt reizen. </p>
            
            <p> Travel Light verstuurt al jouw bagage van groot tot klein door de hele Europese Unie. Wij zijn er voor jou als reiziger en vergroten het comfort en gevoel van luxe door het versturen van bagage voor een transparante en betaalbare prijs. Travel Light is premium partner van TNT, een van de grootste logistieke partijen in de wereld. Met het netwerk van TNT en een speciaal team van Travel Light chauffeurs bieden wij een perfecte service, zelfs beter dan die van de luchtvaartmaatschappijen!</p>
            
            <p>Wij zijn continu bezig onze service te verbeteren en deze zo voordelig mogelijk aan te bieden. Graag laten we jou de vrijheid en het gemak van bagage-vrij reizen ervaren.</p>
            
            <p>Onze dienstverlening is voorlopig alleen nog beschikbaar voor de Nederlandse reiziger die binnen de Europese Unie reist naar geselecteerde bestemmingen. Uiteraard blijven we Travel Light ontwikkelen en gaan we ervoor uiteindelijk ook buiten de Europese Unie jouw bagage te kunnen verzenden.</p>
            
            <p>Wij willen de huidige manier van reizen veranderen. Reizigers kunnen vanaf nu met alle gemak op vakantie. Wij zorgen ervoor dat de bagage op het juiste moment op de plaats van bestemming is zodat jij als reiziger kan genieten van wat echt belangrijk is.<p>
	            
	        <p>Voor ons is elke klant belangrijk, of het nu gaat om het vervoeren van simpelweg ski’s of de meest luxe bagage. De manier waarop wij de klant van dienst kunnen zijn staat bij ons centraal. Gedurende ons proces nemen we de klant mee en houden we die op de hoogte. Vooraf vertellen wij welke service een klant van ons mag verwachten en tegen welk tarief, zonder verrassingen achteraf.</p>
	        
	        <p>Door te reizen met Travel Light kunnen vervoerders efficiëntere vliegtuigen, auto's en treinen maken. Reizen gaat sneller wat op zijn beurt beter is voor het milieu en onze veiligheid.</p>
	        
	        <p>We verbinden de gemeenschap door middel van het doorbreken van traditionele markten en modellen. Vanuit onze filosofie en recht van bestaan, staan we voor gemak, luxe en persoonlijk gewin, in de ruimste zin des woords!</p>
	        
	        <p>Wij verbeteren de reiservaring van onze klanten door hun gemak en gevoel van luxe voorop te stellen. Onze klanten zijn reizigers, reisbureaus, hotels en luchtvaartmaatschappijen. Onze toegevoegde waarde is de realisatie van bagage vrij reizen.</p>
	        
	        <p>Uiteindelijk gaat het niet om het verzenden van bagage maar om het vergroten van de totale reisbeleving! Als Travel Light willen we een bijdrage leveren aan een fantastische trip of vakantie! Wij geloven dat het verzenden van bagage van huis naar hotel de reiservaring écht aangenamer maakt.</p>
	        
	        <p>Alvast een fijne vakantie! Het Travel Light Team.</p>
          

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                      <img alt="travellight" src="img/country1.jpg">
                  </div>
                  <h4>BELGIE VANAF <span>€ 30,-</span></h4>
                </a>
              </div>
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                    <img alt="travellight" src="img/country2.jpg">
                  </div>
                  <h4>FRANKRIJK VANAF <span>€ 35,-</span></h4>
                </a>
              </div>
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                    <img alt="travellight" src="img/country3.jpg">
                  </div>
                  <h4>DUITSLAND VANAF <span>€ 40,-</span></h4>
                </a>
              </div>
            </div>

          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>