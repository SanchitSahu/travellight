<?php include 'header.php'; ?>
    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/portugal.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Portugal <small>Vlieg zonder je bagage naar de zon.</small></h1>
			</div>
          <blockquote>
        Gemiddelde levertijd: 					5 werkdagen	<br>	
          </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           Golfen is misschien niet het eerste waar je aan denkt als je ‘Portugal’ hoort. Toch is Portugal een écht golfland; meer dan de helft van alle golfbanen hebben een hoge standaard als het gaat om hun banen en service. Met tientallen golfbanen om uit te kiezen, luxe resorts, het mooie weer en de mooie natuur is het een heel erg fijn land om op golfvakantie te gaan. Wij kunnen je golfset rechtstreeks naar de golfbaan brengen zolang de receptie daar mee in stemt. <br></p>

<p>Naast golfen is Portugal natuurlijk ook gewoon heel erg geschikt voor strandvakantie’s, fietsvakantie’s, kitevakanties of kampeervakanties. </p>

<p>
<strong>Waar je op moet letten:</strong><br></p>

<p>Indien je je zending naar een afgelegen gebied stuurt, stel de ontvangende partij dan goed op de hoogte. Sommige gebieden (in de bergen bijvoorbeeld) zijn namelijk wat lastiger te bereiken. Het zou natuurlijk heel vervelend zijn als de aflevering in zo’n gebied door een miscommunicatie verkeerd loopt, dat kan immers ongewenste vertraging opleveren.
</p><p>
Portugal heeft als katholiek land veel nationale feestdagen. De onderstaande feestdagen zijn vrije dagen voor heel Portugal in 2017 en op deze dagen zullen er dus geen zendingen worden geleverd::
</p><p>

<ul>
	<li>1 januari:	Nieuwjaarsdag</li>
	<li>19 maart: 	Vaderdag</li>
	<li>14 april:	Goede vrijdag</li>
	<li>16 april:	Pasen</li>
	<li>25 april: 		Anjerrevolutie</li>
	<li>1 mei:		Dag van de arbeid</li>
	<li>10 juni:	Nationale feestdag Portugal</li>
	<li>15 juni:	Sacramentsdag</li>
	<li>15 augustus: Onze Lieve Vrouw Hemelvaart</li>
	<li>5 oktober: Dag van de Republiek van Portugal</li>
	<li>1 november:	Allerheiligen</li>
	<li>1 december: Portugese Restauratieoorlog</li>
	<li>8 december: Onbevlekte Ontvangenis van Maria</li>
	<li>25 december: Kerstmis</li>
</ul>
<p>
<i>Naast deze nationale feestdagen zijn er soms ook regionale feestdagen waar men vrij heeft. Wij houden dit zelf zo goed mogelijk bij, maar het kan nooit kwaad om het zelf ook voor de zekerheid na te gaan.</i></p>
 <br><br>

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                  <div class="image">
                          <a href="/tussenpagina.php?price=170&countryList=PT&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=74&countryList=PT&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=104&countryList=PT&productList=SKU_4&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/golf_small.png"></a>
                </div>
				<!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
            </div>

          </div>
        </div>

    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>


<?php include 'footer.php'; ?>