<?php include 'header.php'; ?>
    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/belgium2.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">België <small>Lekker dichtbij huis en nog makkelijker zonder bagage.</small></h1>
			</div>
          <blockquote>
        Gemiddelde levertijd: 					3 werkdagen	<br>	
          </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           België, lekker dichtbij en toch een populaire bestemming voor wandelaars, fietsers en kampeerders. De Belgische Ardennen trekken jaarlijks duizenden Nederlanders naar de bossen. Je kunt er een hoop dorpjes bezoeken, grotten ontdekken, kastelen bekijken en genieten van de rust. Naast de Ardennen zijn er ook een hoop historische steden in het land; in het sprookjesachtige Brugge wordt elk jaar een fantastische kerstmarkt georganiseert en ook in steden als Brussel, Antwerpen of Gent kan men een hoop zien en ervaren.  <br></p>

<p>Wij krijgen een hoop vragen van onze zuider buren over onze service. Momenteel kunnen wij nog geen zendingen sturen van een Belgisch adres naar het buitenland of van het buitenland naar een Belgisch adres. Wij willen dit wel erg graag en zijn ook hard aan het werk om dit mogelijk te maken. Je kunt je inschrijven voor onze nieuwsbrief of ons volgen op facebook om op de hoogte te blijven van deze veranderingen. 
</p> <br><br>

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                  <div class="image">
                          <a href="/tussenpagina.php?price=155&countryList=BE&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=59&countryList=BE&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
                  <!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=89&countryList=BE&productList=SKU_4&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/golf_small.png"></a>
                </div>
				<!--<h4>BELGIE VANAF <span>€ 30,-</span></h4>-->
              </div>
            </div>

          </div>
        </div>

    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>