var sku_values = {};
var onlyCountries = ['at', 'be', 'fr', 'de', 'it', 'lu', 'nl', 'pt', 'es', 'gb'];

$(document.body).on("change", "#countryList_3", function () {
    var returnVal = 0;
    var destination_country = $("#countryList2").val();
    var pickup_country = $("#countryList_3").val();
    var flyDate = $("#dpd1").val();
    if (pickup_country == "NL") {
        $('#Postcode').addClass('postcodeClass');
    } else {
        $('#Postcode').removeClass('postcodeClass');
    }

    if (destination_country == "NL") {
        $('#Postcode_2').addClass('postcodeClass2');
    } else {
        $('#Postcode').removeClass('postcodeClass2');
    }


    if ($(this).val() == 'NL' && $('#countryList2').val() == 'NL') {

    } else {
        if ($(this).val() == 'NL') {
            $('#countryList2').val('');
        } else {
            $('#countryList2').val('NL');
        }
    }

    if (pickup_country == "") {
        /*$("#countryDropdown_31").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#countryDropdown_31").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#countryDropdown_3 select:first").focus();*/
        //bootbox.alert("You must select Pickup or Destination country");
    } else if (destination_country == "") {
        /*$("#countryDropdown_21").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#countryDropdown_21").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#countryDropdown_2 select:first").focus();*/
        //bootbox.alert("You must select Pickup or Destination country");
    } else if (flyDate == "") {
        /*$("#dpd1").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#dpd1").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#dpd1").focus();*/
        //bootbox.alert("please select check in date");
    } else {
        //alert("1");
        
        var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                if (i == 0) {
                    queryString = index.toLowerCase() + '=' + item;
                    i++;
                } else {
                    queryString += '&' + index.toLowerCase() + '=' + item;
                }
                //console.log(index);
                //console.log(item);
            })
  
            var url = '';
            if(queryString == ''){
                url = "http://fox.fireeagle.nl:8092/prices?destination_country=" + destination_country + "&retour=" + returnVal;
            }else{
                url = "http://fox.fireeagle.nl:8092/prices?destination_country=" + destination_country + "&retour=" + returnVal + "&" + queryString;
            }
        $.ajax({
            url: url,
            async: false,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                //alert(response);
                var allData = response.data;
                $(".koffer").empty();
                //alert(allData.price_in);


                $(".koffer").html('');
                $.each(allData.products, function (index, item) {
                    //var name = $('option[value="'+item.sku+'"]').parent().eq(1).attr('data-name');

                    $(".koffer").append($("<li class='priceBoxKoffer'><span class='left'>" + item.amount + " x " + item.description + "</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                });

                var Surcharge = Math.round(allData.price_delivery_in);
                $(".Surcharge").html(Surcharge);
                /*$.each(allData.products, function (index, item) {
                 //alert("index"+index);alert("item"+item);alert(index.toSource());alert(item.toSource());

                 $("."+data_name).html($("<li class='priceBoxKoffer'><span class='left'>" + item.amount + " x "+data_name+"</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                 })*/
                //$("."+data_name).html("<li style='display:none'><span class='right subPrice'>" + Math.round(allData.price_in) + " €</span></li>");
                var Surcharge = Math.round(allData.price_delivery_in);
                if (Surcharge == 0)
                    Surcharge = "is gratis";
                else
                    Surcharge = Surcharge;
                $(".Surcharge").html(Surcharge);
                $(".finalPrice").html(Math.round(allData.price_in));
                //window.location = 'tussenpagina.php?price=' + price;
            }
        });
        
        var dpd1D = flyDate.split('-').reverse().join('-');
        $.ajax({
            url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country + "&destination_country=" + destination_country + "&date=" + dpd1D + "&is_return=" + returnVal,
            async: true,
            success: function (response) {
                var allData = response.data;
                //alert(allData);				
                $('#dpd3').empty();
                $.each(allData, function (index, value) {
                    var newDtemp = value.split('-').reverse().join('-');
                    $("#dpd3").append("<option value=" + newDtemp + ">" + newDtemp + "</option>");

                });

                //var unavailableDates = ["2016-11-11"];

                //alert(allData);
                /*$('#dpd3').datepicker({
                 beforeShowDay:function(date){
                 dmy = date.getFullYear() + "-" +  (date.getMonth() + 1)  + "-" + date.getDate();
                 if ($.inArray(dmy, allData) == -1) {
                 return false;
                 } else {
                 return true;
                 }
                 },
                 format: 'yyyy-mm-dd'
                 }).show();	*/
            }
        });
    }



    //for check out
    var returnVal1 = 1;
    var destination_country1 = $("#countryList_3").val();
    var pickup_country1 = $("#countryList2").val();
    var flyDate1 = $("#dpd2").val();

    if (pickup_country1 == "" || destination_country1 == "" || flyDate1 == "") {
    } else {
        //alert("2");
        var dpd2D = flyDate1.split('-').reverse().join('-');
        $.ajax({
            url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country1 + "&destination_country=" + destination_country1 + "&date=" + dpd2D + "&is_return=" + returnVal1,
            async: true,
            success: function (response) {
                var allData2 = response.data;
                //var unavailableDates = ["2016-11-11"];

                //alert(allData);
                $('#dpd4').empty();
                $.each(allData2, function (index, value) {
                    var newDtemp = value.split('-').reverse().join('-');
                    $("#dpd4").append("<option value=" + newDtemp + ">" + newDtemp + "</option>");

                });
            }
        });
    }

});

$(document.body).on("change", "#countryList2", function () {
    var returnVal = 0;
    var destination_country = $("#countryList2").val();
    var pickup_country = $("#countryList_3").val();
    var flyDate = $("#dpd1").val();

    if (pickup_country == "NL") {
        $('#Postcode').addClass('postcodeClass');
    } else {
        $('#Postcode').removeClass('postcodeClass');
    }

    if (destination_country == "NL") {
        $('#Postcode_2').addClass('postcodeClass2');
    } else {
        $('#Postcode').removeClass('postcodeClass2');
    }


    if ($(this).val() == 'NL' && $('#countryList_3').val() == 'NL') {

    } else {
        if ($(this).val() == 'NL') {
            $('#countryList_3').val('');
        } else {
            $('#countryList_3').val('NL');
        }
    }

    if (pickup_country == "") {
        /*$("#countryDropdown_31").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#countryDropdown_31").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#countryDropdown_3 select:first").focus();*/
        //bootbox.alert("You must select Pickup or Destination country");
    } else if (destination_country == "") {
        /*$("#countryDropdown_21").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#countryDropdown_21").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#countryDropdown_2 select:first").focus();*/
        //bootbox.alert("You must select Pickup or Destination country");
    } else if (flyDate == "") {
        /*$("#dpd1").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#dpd1").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#dpd1").focus();*/
        //bootbox.alert("please select check in date");
    } else {
        //alert("3");
        var sku_values = {};
        $.each($(".col-sm-6 .commonContainer .block"), function (index, item) {
                    var item_value = $(item).find('select option:selected').val();
                    if (typeof sku_values[item_value] == "undefined") {
                        sku_values[item_value] = 1;
                    } else {
                        sku_values[item_value] = sku_values[item_value] + 1;
                    }
                });
        console.log(sku_values);
  
        var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                if (i == 0) {
                    queryString = index.toLowerCase() + '=' + item;
                    i++;
                } else {
                    queryString += '&' + index.toLowerCase() + '=' + item;
                }
                //console.log(index);
                //console.log(item);
            })
            
            var url = '';
            if(queryString == ''){
                url = "http://fox.fireeagle.nl:8092/prices?destination_country=" + destination_country + "&retour=" + returnVal;
            }else{
                url = "http://fox.fireeagle.nl:8092/prices?destination_country=" + destination_country + "&retour=" + returnVal + "&" + queryString;
            }

        $.ajax({
            url: url,
            async: false,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                //alert(response);
                var allData = response.data;
                $(".koffer").empty();
                //alert(allData.price_in);


                $(".koffer").html('');
                $.each(allData.products, function (index, item) {
                    //var name = $('option[value="'+item.sku+'"]').parent().eq(1).attr('data-name');

                    $(".koffer").append($("<li class='priceBoxKoffer'><span class='left'>" + item.amount + " x " + item.description + "</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                });

                var Surcharge = Math.round(allData.price_delivery_in);
                $(".Surcharge").html(Surcharge);
                /*$.each(allData.products, function (index, item) {
                 //alert("index"+index);alert("item"+item);alert(index.toSource());alert(item.toSource());

                 $("."+data_name).html($("<li class='priceBoxKoffer'><span class='left'>" + item.amount + " x "+data_name+"</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                 })*/
                //$("."+data_name).html("<li style='display:none'><span class='right subPrice'>" + Math.round(allData.price_in) + " €</span></li>");
                var Surcharge = Math.round(allData.price_delivery_in);
                if (Surcharge == 0)
                    Surcharge = "is gratis";
                else
                    Surcharge = Surcharge;
                $(".Surcharge").html(Surcharge);
                $(".finalPrice").html(Math.round(allData.price_in));
                //window.location = 'tussenpagina.php?price=' + price;
            }
        });
            
            
            
            
        var dpd1D = flyDate.split('-').reverse().join('-');
        $.ajax({
            url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country + "&destination_country=" + destination_country + "&date=" + dpd1D + "&is_return=" + returnVal,
            async: true,
            success: function (response) {
                var allData = response.data;
                //alert(allData);				
                $('#dpd3').empty();
                $.each(allData, function (index, value) {
                    var newDtemp = value.split('-').reverse().join('-');
                    $("#dpd3").append("<option value=" + newDtemp + ">" + newDtemp + "</option>");

                });

                //var unavailableDates = ["2016-11-11"];

                //alert(allData);
                /*$('#dpd3').datepicker({
                 beforeShowDay:function(date){
                 dmy = date.getFullYear() + "-" +  (date.getMonth() + 1)  + "-" + date.getDate();
                 if ($.inArray(dmy, allData) == -1) {
                 return false;
                 } else {
                 return true;
                 }
                 },
                 format: 'yyyy-mm-dd'
                 }).show();	*/
            }
        });
    }


    //for check out
    var returnVal1 = 1;
    var destination_country1 = $("#countryList_3").val();
    var pickup_country1 = $("#countryList2").val();
    var flyDate1 = $("#dpd2").val();

    if (pickup_country1 == "" || destination_country1 == "" || flyDate1 == "") {
    } else {
        //alert("4");
        var dpd2D = flyDate1.split('-').reverse().join('-');
        $.ajax({
            url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country1 + "&destination_country=" + destination_country1 + "&date=" + dpd2D + "&is_return=" + returnVal1,
            async: true,
            success: function (response) {
                var allData2 = response.data;
                //var unavailableDates = ["2016-11-11"];

                //alert(allData);
                $('#dpd4').empty();
                $.each(allData2, function (index, value) {
                    var newDtemp = value.split('-').reverse().join('-');
                    $("#dpd4").append("<option value=" + newDtemp + ">" + newDtemp + "</option>");

                });
            }
        });
    }
});


$("#dpd1").on('change', function () {
    
    if(esimatecheckOut != ""){
        $("#esimatecheckOut").val(esimatecheckOut);
    }
    var returnVal = 0;
    var destination_country = valTo;//$("#countryList2").val();
    var pickup_country = valFrom;//$("#countryList_3").val();
    var flyDate = $("#dpd1").val();

    if (pickup_country == "NL") {
        $('#Postcode').addClass('postcodeClass');
    } else {
        $('#Postcode').removeClass('postcodeClass');
    }

    if (destination_country == "NL") {
        $('#Postcode_2').addClass('postcodeClass2');
    } else {
        $('#Postcode').removeClass('postcodeClass2');
    }

    if (pickup_country == "") {
        pickup_country = valFrom;
        /*$("#countryDropdown_31").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#countryDropdown_31").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#countryDropdown_3 select:first").focus();*/
        //bootbox.alert("You must select Pickup or Destination country");
    } else if (destination_country == "") {
        destination_country = valTo;
        /*$("#countryDropdown_21").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#countryDropdown_21").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#countryDropdown_2 select:first").focus();*/
        //bootbox.alert("You must select Pickup or Destination country");
    } else if (flyDate == "") {
        /*$("#dpd1").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#dpd1").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#dpd1").focus();*/
        //bootbox.alert("please select check in date");
    } else {
        //alert("5");
        var dpd1D = flyDate.split('-').reverse().join('-');
        $.ajax({
            url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country + "&destination_country=" + destination_country + "&date=" + dpd1D + "&is_return=" + returnVal,
            async: true,
            success: function (response) {
                var allData = response.data;
                //alert(allData);				
                $('#dpd3').empty();
                $.each(allData, function (index, value) {
                    var newDtemp = value.split('-').reverse().join('-');
                    $("#dpd3").append("<option value=" + newDtemp + ">" + newDtemp + "</option>");

                });

                //var unavailableDates = ["2016-11-11"];

                //alert(allData);
                /*$('#dpd3').datepicker({
                 beforeShowDay:function(date){
                 dmy = date.getFullYear() + "-" +  (date.getMonth() + 1)  + "-" + date.getDate();
                 if ($.inArray(dmy, allData) == -1) {
                 return false;
                 } else {
                 return true;
                 }
                 },
                 format: 'yyyy-mm-dd'
                 }).show();	*/
            }
        });
    }

});


$("#dpd2").on('change', function () {
    var returnVal = 1;
    var destination_country = valTo;//$("#countryList_3").val();
    var pickup_country = valFrom;//$("#countryList2").val();
    var flyDate = $("#dpd2").val();

    if (pickup_country == "NL") {
        $('#Postcode').addClass('postcodeClass');
    } else {
        $('#Postcode').removeClass('postcodeClass');
    }

    if (destination_country == "NL") {
        $('#Postcode_2').addClass('postcodeClass2');
    } else {
        $('#Postcode').removeClass('postcodeClass2');
    }

    if (pickup_country == "") {
        /*$("#countryDropdown_31").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#countryDropdown_31").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#countryDropdown_3 select:first").focus();*/
        //bootbox.alert("please select Pickup country");
    } else if (destination_country == "") {
        /*$("#countryDropdown_21").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#countryDropdown_21").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#countryDropdown_2 select:first").focus();*/
        //bootbox.alert("please select Destination country");
    } else if (flyDate == "") {
        /*$("#dpd1").closest('.form-group').removeClass('has-success').addClass('has-error');
         $("#dpd1").removeClass('glyphicon-ok').addClass('glyphicon-remove');
         $("#dpd1").focus();*/
        //bootbox.alert("please select check in date");
    } else {
        //alert("6");
        var dpd2D = flyDate.split('-').reverse().join('-');
        $.ajax({
            url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country + "&destination_country=" + destination_country + "&date=" + dpd2D + "&is_return=" + returnVal,
            async: true,
            success: function (response) {
                var allData2 = response.data;
                //var unavailableDates = ["2016-11-11"];

                //alert(allData);
                $('#dpd4').empty();
                $.each(allData2, function (index, value) {
                    var newDtemp = value.split('-').reverse().join('-');
                    $("#dpd4").append("<option value=" + newDtemp + ">" + newDtemp + "</option>");

                });
            }
        });
    }

});

$(document.body).on('keyup', '.postcodeClass', function () {
    var postCode = $(this).val();
    $.ajax({
        url: "https://api.pro6pp.nl/v1/autocomplete?auth_key=nWvd4vIkHedksaiK&nl_sixpp=" + postCode + "",
        success: function (response) {
            var street = response.results[0].street;
            var city = response.results[0].city;
            //alert(street);
            $("#Straat").val(street);
            $("#Woonplaats").val(city);
        }
    });
});

$(document.body).on('keyup', '.postcodeClass2', function () {
    var postCode = $(this).val();
    $.ajax({
        url: "https://api.pro6pp.nl/v1/autocomplete?auth_key=nWvd4vIkHedksaiK&nl_sixpp=" + postCode + "",
        success: function (response) {
            var street = response.results[0].street;
            var city = response.results[0].city;
            //alert(street);
            $("#Straat_2").val(street);
            $("#Woonplaats_2").val(city);
        }
    });
});

$(document).ready(function () {
    if(dpd1Headerval != ""){
        $("#dpd1Header").val(dpd1Headerval);
    }else{
        $("#dpd1Header").val("Wanneer vertrek je?");
    }
    $("#dpd1Header").click(function(){
        $("#dpd1Header").datepicker('show');
    })
    
    
    if (returnQuick == 0) {
        $(".returnDateClass").css("display", "none");
        $("#Verzendtype option[value='0']").attr('selected', true);
    } else {
        $(".returnDateClass").css("display", "block");
        $("#Verzendtype option[value='1']").attr('selected', true);
    }

    var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
    $(".menu li a").each(function () {
        if ($(this).attr("href") == pgurl || $(this).attr("href") == '')
            $(this).parent().addClass("active");
    })


    $("#dpd3").click(function () {
        var returnVal = 0;
        var destination_country = $("#countryList2").val();
        var pickup_country = $("#countryList_3").val();
        var flyDate = $("#dpd1").val();

        if (pickup_country == "") {
            /*$("#countryDropdown_31").closest('.form-group').removeClass('has-success').addClass('has-error');
             $("#countryDropdown_31").removeClass('glyphicon-ok').addClass('glyphicon-remove');
             $("#countryDropdown_3 select:first").focus();*/
            bootbox.alert("selecteer Oppakken land");
        } else if (destination_country == "") {
            /*$("#countryDropdown_21").closest('.form-group').removeClass('has-success').addClass('has-error');
             $("#countryDropdown_21").removeClass('glyphicon-ok').addClass('glyphicon-remove');
             $("#countryDropdown_2 select:first").focus();*/
            bootbox.alert("selecteert u Land van bestemming");
        } else if (flyDate == "") {
            /*$("#dpd1").closest('.form-group').removeClass('has-success').addClass('has-error');
             $("#dpd1").removeClass('glyphicon-ok').addClass('glyphicon-remove');
             $("#dpd1").focus();*/
            bootbox.alert("please select check in date");
        }

    });

    $("#dpd4").click(function () {
        var returnVal = 1;
        var destination_country = $("#countryList_3").val();
        var pickup_country = $("#countryList2").val();
        var flyDate = $("#dpd2").val();

        if (pickup_country == "") {
            /*$("#countryDropdown_31").closest('.form-group').removeClass('has-success').addClass('has-error');
             $("#countryDropdown_31").removeClass('glyphicon-ok').addClass('glyphicon-remove');
             $("#countryDropdown_3 select:first").focus();*/
            bootbox.alert("selecteer Oppakken land");
        } else if (destination_country == "") {
            /*$("#countryDropdown_21").closest('.form-group').removeClass('has-success').addClass('has-error');
             $("#countryDropdown_21").removeClass('glyphicon-ok').addClass('glyphicon-remove');
             $("#countryDropdown_2 select:first").focus();*/
            bootbox.alert("selecteert u Land van bestemming");
        } else if (flyDate == "") {
            /*$("#dpd1").closest('.form-group').removeClass('has-success').addClass('has-error');
             $("#dpd1").removeClass('glyphicon-ok').addClass('glyphicon-remove');
             $("#dpd1").focus();*/
            bootbox.alert("please select check in date");
        }

    });
    $("#countries").intlTelInput({
        initialCountry: "auto",
        onlyCountries: onlyCountries,
        geoIpLookup: function (callback) {
            $.get('http://ipinfo.io', function () {}, "jsonp").always(function (resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                var tolowercode = countryCode.toLowerCase();
                ifhasCountryinList = onlyCountries.indexOf(tolowercode);
                //alert(tolowercode);alert(ifhasCountryinList);
                if (ifhasCountryinList == -1) {
                    countryCode = 'nl';
                }

                callback(countryCode);
            });
        },
        separateDialCode: true,
        utilsScript: "js/intl-tel-input/js/utils.js" // just for formatting/placeholders etc
    });


    $('a.step').click(function (e) {
        e.preventDefault();
        var clicked = $(this).data('id').split('_')[1];
        var current = $('.BookingSteps a.step.active').data('id').split('_')[1];
        if (current > clicked) {
            $('.BookingFieldsMainDiv').hide();
            $('.BookingSteps a.step.active').removeClass('active');
            $('.BookingSteps a.step[data-id="step_' + clicked + '"]').addClass('active');
            $('.BookingFieldsMainDiv.booking' + clicked).show();
        }
    });

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
    }, "Please enter only letters");

    $('#contact_form1').validate({
        ignore: [],
        rules: {
            Klant1: {
                required: true
            },
            options: {
                required: true
            },
            Voorletters: {
                required: true,
            },
            Achternaam: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            countries: {
                required: true,
                minlength: 8,
                number: true
            },
//            phonenumber: {
//                required: true,
//                minlength: 5,
//                number: true
//            }
        },
        messages: {
            Klant1: {
                required: "Vergeet dit veld niet in te vullen",
            },
            options: {
                required: "Vergeet dit veld niet in te vullen"
            },
            Voorletters: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in."
            },
            Achternaam: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in."
            },
            email: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in.",
                email: "Vul een geldig e-mailadres in"
            },
            countries: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in.",
                number: "Alleen nummers zijn toegestaan"
            },
//            phonenumber: {
//                required: "Vergeet dit veld niet in te vullen",
//                minlength: "Please enter minimum {0} characters.",
//                number: "enter only numbers"
//            },
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            }
            if (element.attr("name") == "options") {
                element.parent().parent().parent().last().after(error);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $(".booking2").fadeIn();
            $(".booking1").fadeOut();
            $(window).scrollTop(0);
            $('.BookingSteps a.step.active').removeClass('active').next().addClass('active');
            return false;
        }
    });



    $('#contact_form2').validate({
        rules: {
            dpd1: {
                required: true,
                minlength: 5
            },
            dpd2: {
                required: true,
                minlenght: 5
            },
            countryList_3: {
                required: true
            },
            Postcode: {
                required: true,
                minlength: 3
            },
            Huisnummer: {
                required: true,
                number: true
            }/*,
             Toevoeging: {
             required: true,
             minlength: 5
             }*/,
            Straat: {
                required: true,
                lettersonly: true
            },
            Woonplaats: {
                required: true,
                lettersonly: true
            },
            Verzendtype1: {
                required: true
            },
            dpd3: {
                required: true
            },
            dpd4: {
                required: true
            },
            option_2: {
                required: true,
                minlength: 5,
                number: true
            },
            countryList2: {
                required: true
            },
            Hotel_2: {
                required: true,
                minlength: 2
            },
            Postcode_2: {
                required: true,
                minlength: 3
            },
            Huisnummer_2: {
                required: true,
                number: true
            },
            Addres_2: {
                required: true,
            },
            Straat_2: {
                required: true,
                lettersonly: true
            },
            Woonplaats_2: {
                required: true,
                lettersonly: true
            }
        },
        messages: {
            dpd1: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in."
            },
            dpd2: {
                required: "Vergeet dit veld niet in te vullen"
            },
            countryList_3: {
                required: "Vergeet dit veld niet in te vullen"
            },
            Postcode: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in"
            },
            Huisnummer: {
                required: "Vergeet dit veld niet in te vullen",
                number: "Vul een geldig nummer in"
            }/*,
             Toevoeging: {
             required: "Vergeet dit veld niet in te vullen"
             }*/,
            Straat: {
                required: "Vergeet dit veld niet in te vullen",
                lettersonly: "vul dan alleen letters"
            },
            Woonplaats: {
                required: "Vergeet dit veld niet in te vullen"
            },
            Verzendtype: {
                required: "Vergeet dit veld niet in te vullen"
            },
            dpd3: {
                required: "Vergeet dit veld niet in te vullen"
            },
            dpd4: {
                required: "Vergeet dit veld niet in te vullen"
            },
            option_2: {
                required: "Vergeet dit veld niet in te vullen"
            },
            countryList2: {
                required: "Vergeet dit veld niet in te vullen"
            },
            Hotel_2: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in"
            },
            Postcode_2: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in"
            },
            Huisnummer_2: {
                required: "Vergeet dit veld niet in te vullen",
                number: "Vul een geldig nummer in"
            },
            Toevoeging_1: {
                required: "Vergeet dit veld niet in te vullen"
            },
            Straat_2: {
                required: "Vergeet dit veld niet in te vullen",
                lettersonly: "vul dan alleen letters"
            },
            Woonplaats_2: {
                required: "Vergeet dit veld niet in te vullen"
            },
            Addres: {
                required: "Vergeet dit veld niet in te vullen"
            }
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {

            var from = $("#countryList_3").val();
            var to = $("#countryList2").val();//alert(from);alert(to);
            if (from == "NL" || to == "NL") {
                $(".booking3").fadeIn();
                $(".booking2").fadeOut();
                $(window).scrollTop(0);
                $('.BookingSteps a.step.active').removeClass('active').next().addClass('active');
                return false;
            } else {
                bootbox.alert("One of the country should be  Nederland");
            }

        }
    });


    $('#contact_form3').validate({
        rules: {
        },
        messages: {
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            //alert("ff");
            var returnVal = $("#Verzendtype").val();
            var destination_country = $("#countryList_3").val();
            var destination_countryTxt = $("#countryList_3 option[value=" + destination_country + "]").text();
            var pickup_country = $("#countryList2").val();
            var flyDate = $("#dpd3").val();

            //alert(flyDate);
            var checkin = $("#dpd1").val();
            var checkout = $("#dpd2").val();
            var Straat = $("#Straat").val();
            var Huisnummer = $("#Huisnummer").val(); //ADDRESS
            var Toevoeging = $("#Toevoeging").val();
            var countryList_3 = $("#countryList_3").val();
            var Straat2 = $("#Straat_2").val();
            var Huisnummer_2 = $("#Huisnummer_2").val();
            var Toevoeging_2 = $("#Toevoeging_2").val();
            var Hotel_2 = $("#Hotel_2").val();
            var Woonplaats_2 = $("#Woonplaats_2").val(); //residence
            var countryList2 = $("#countryList2").val();
            var countryList2Txt = $("#countryList2 option[value=" + countryList2 + "]").text();
            var dpd3 = $("#dpd3").val();
            var dpd4 = $("#dpd4").val();
            var Woonplaats = $("#Woonplaats").val();
            //alert("7");

            var dpd3D = flyDate.split('-').reverse().join('-');//alert(dpd3D);
            $.ajax({
                url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country + "&destination_country=" + destination_country + "&date=" + dpd3D + "&is_return=" + returnVal,
                success: function (response) {
                    var allData = response.data;

                    //alert(allData);
                    var html = "<p>Verzending</p><p>We komen je bagage afhalen op de " + Straat + " " + Huisnummer + " " + Toevoeging + ", in " + destination_countryTxt + ",  op " + dpd3 + " tussen 11:45 uur en 17:00 uur.</p><p>Aflevering</p><p>We leveren je bagage uiterlijk " + checkin + " af op de " + Straat2 + " " + Huisnummer_2 + " " + Toevoeging_2 + ",in " + countryList2Txt + " bij de receptie van " + Hotel_2 + ".</p><p>Retourzending</p><p>Vervolgens halen we vanaf " + checkout + " je bagage bij " + Hotel_2 + " weer op en zal dit uiterlijk " + dpd4 + " tussen 1145 en 1700 uur afgeleverd worden op de  " + Straat + " " + Huisnummer + " " + Toevoeging + "in " + Woonplaats + "..</p>";
                    $(".summaryText").html(html);
                }
            });
            $(".booking4").fadeIn();
            $(".booking3").fadeOut();
            //$('.BookingSteps a.step.active').removeClass('active');
            $(window).scrollTop(0);
        }
    });
    /*$("#contact_form1").bootstrapValidator({  
     feedbackIcons: {
     valid: 'glyphicon glyphicon-ok',
     invalid: 'glyphicon glyphicon-remove',
     validating: 'glyphicon glyphicon-refresh'
     },
     fields: {
     Klant1: {
     validators: {
     notEmpty: {
     message: 'Vergeet dit veld niet in te vullen'
     }
     }
     },
     options: {
     // The threshold option does not effect to the elements
     // which user cannot type in, such as radio, checkbox, select, etc.
     threshold: 5,
     validators: {
     notEmpty: {
     message: 'The summary is required'
     }
     }
     },
     Voorletters: {
     validators: {
     stringLength: {
     min: 2,
     },
     notEmpty: {
     message: 'Vergeet dit veld niet in te vullen'
     }
     }
     },
     phonenumber: {
     validators: {
     notEmpty: {
     message: 'Vergeet dit veld niet in te vullen'
     }
     }
     },
     },
     submitHandler: function(validator, form, submitButton) {
     //alert("f");
     $(".booking2").fadeIn();
     $(".booking1").fadeOut();
     },
     });*/


    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            },
            last_name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    },
                    emailAddress: {
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    },
                    phone: {
                        country: 'US',
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            },
            address: {
                validators: {
                    stringLength: {
                        min: 8,
                    },
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            },
            city: {
                validators: {
                    stringLength: {
                        min: 4,
                    },
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            },
            Klant: {
                validators: {
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    },
                    zipCode: {
                        country: 'US',
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            },
            comment: {
                validators: {
                    stringLength: {
                        min: 10,
                        max: 200,
                        message: 'Vergeet dit veld niet in te vullen'
                    },
                    notEmpty: {
                        message: 'Vergeet dit veld niet in te vullen'
                    }
                }
            }
        }
    })
            .on('success.form.bv', function (e) {
                $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

                // Prevent form submission
                e.preventDefault();

                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                $.post($form.attr('action'), $form.serialize(), function (result) {
                    console.log(result);
                }, 'json');
            });



    $('#contactForm').validate({
        rules: {
            naam: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true,
                minlength: 3
            }
        },
        messages: {
            naam: {
                required: "Vergeet dit veld niet in te vullen",
            },
            email: {
                required: "Vergeet dit veld niet in te vullen"
            },
            message: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Vul minimaal {0} tekens in"
            }
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            }
            if (element.attr("name") == "options") {
                element.parent().parent().parent().last().after(error);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var email = $("#email").val();
            var message = $("#message").val();
            var username = $("#naam").val();
            //alert("ff");
            $.ajax({
                url: "sendMail.php",
                type: "POST",
                data: "email=" + email + "&message=" + message+"&username="+username,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {
                    //alert(data);
                    if (data == "True") {
                        bootbox.alert("Je e-mail is verzonden");
                    } else {
                        bootbox.alert("error verzenden van e-mail");
                    }
                },
                failure: function (errMsg) {
                    bootbox.alert("error verzenden van e-mail");
                }
            });

        }
    });


});


$(document).ready(function () {

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/countries",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='countryList' class='form-control custom'>";
            html += "<option value=''>Waar ga je heen?</option>";
            $.each(allData, function (index, value) {
                if (country_code == value.code) {
                    html += "<option value='" + value.code + "' selected='selected'> " + value.name + "</option>";
                } else {
                    html += "<option value='" + value.code + "'> " + value.name + "</option>";
                }

            });
            html += "</select>";
            $("#countryDropdown").html(html);
        }
    });

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/countries",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='countryList2' name='countryList2' class='form-control custom'>";
            html += "<option value=''>Kies bestemming</option>";
            $.each(allData, function (index, value) {
                if (valTo == value.code) {
                    if (value.code == "NL") {
                        $('#Postcode_2').addClass('postcodeClass2');
                    } else {
                        $('#Postcode_2').removeClass('postcodeClass2');
                    }
                    html += "<option value='" + value.code + "' selected='selected'> " + value.name + "</option>";
                } else {
                    html += "<option value='" + value.code + "'> " + value.name + "</option>";
                }
            });
            html += "</select>";
            $("#countryDropdown_2").html(html);
        }
    });

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/countries",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='countryList_3' name='countryList_3' class='form-control custom'>";
            html += "<option value=''>Kies bestemming</option>";
            /*var testing = "<?php echo $htmlString; ?>";;alert(testing);*/
            //alert(valFrom);
            $.each(allData, function (index, value) {
                if (valFrom == value.code || value.code == "NL") {
                    if (value.code == "NL") {
                        $('#Postcode').addClass('postcodeClass');
                    } else {
                        $('#Postcode').removeClass('postcodeClass');
                    }
                    html += "<option value='" + value.code + "' selected='selected'> " + value.name + "</option>";
                } else {
                    html += "<option value='" + value.code + "'> " + value.name + "</option>";
                }
            });
            html += "</select>";
            $("#countryDropdown_3").html(html);



        }
    });

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/products",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.description.toLowerCase();
                var y = b.description.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";

            html += "<select id='productList' class='form-control custom'>";
            html += "<option value=''>Wat neem je mee?</option>";

            $.each(allData, function (index, value) {
                if (productList == value.code) {
                    html += "<option value='" + value.code + "' selected='selected'> " + value.description + "</option>";
                } else {
                    html += "<option value='" + value.code + "'> " + value.description + "</option>";
                }
            });
            html += "</select>";
            $("#productDropDown").html(html);
        }
    });
});


$(".quickBooking").on('click', function () {

    var countryList = $("#countryList").val();
    //alert(countryList);
    var productList = $("#productList").val();
    var dpd1Header = $("#dpd1Header").val();
    if (countryList == 0) {
        $(".dpd1Class").html("Selecteer land");
        //alert(dpd1Header);
    } else if (dpd1Header == "Wanneer vertrek je") {
        $(".dateHeaderClass").html("selecteer een datum");
    } else if (productList == 0) {
        $(".productClass").html("Selecteer bagage");
    } else {
        var list = "";
        for ($i = 1; $i <= 8; $i++) {
            if (productList == "SKU_" + $i) {
                list += "&sku_" + $i + "=1";
            } else
                list += "&sku_" + $i + "=0";
        }

        //alert(list);

        $.ajax({
            url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=0" + list,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                //alert("http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=0" + list);
                var allData = response.data;
                //alert(allData.price_in);
                var price = Math.round(allData.price_in);
                var Surcharge = Math.round(allData.price_delivery_in);
                if (Surcharge == 0)
                    Surcharge = "is gratis";
                else
                    Surcharge = Surcharge;
                window.location = 'tussenpagina.php?price=' + price + '&Surcharge=' + Surcharge + '&countryList=' + countryList + '&productList=' + productList + '&dpd1Header=' + dpd1Header;
            }
        });
    }

});


$(function () {

    $(document.body).on('click', '#dpd1Header', function () {
        $("#dpd1Header").datepicker('show');
    });
    $(document.body).on('change', 'select[name="SKU_Drp[]"]', function () {
        var data_name = $(this).attr('data-name');
        sku_values = {};
        $.each($(".col-sm-6 .commonContainer .block"), function (index, item) {
            var item_value = $(item).find('select option:selected').val();
            if (typeof sku_values[item_value] == "undefined") {
                sku_values[item_value] = 1;
            } else {
                sku_values[item_value] = sku_values[item_value] + 1;
            }
        });
        //console.log(sku_values);
        //ajax call
        var countryList = $("#countryList2").val();
        //alert("country"+countryList);
        var retour = $("#Verzendtype").val();
        var productList = $("#klant1").val();
        //alert("product"+productList);

        if (countryList == 0) {
            alert("Selecteer land");
        } else {
            var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                if (i == 0) {
                    queryString = index.toLowerCase() + '=' + item;
                    i++;
                } else {
                    queryString += '&' + index.toLowerCase() + '=' + item;
                }
                //console.log(index);
                //console.log(item);
            })

            //alert(queryString);

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + "&" + queryString,
                async: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    //alert(response);
                    var allData = response.data;
                    $(".koffer").empty();
                    //alert(allData.price_in);


                    $(".koffer").html('');
                    $.each(allData.products, function (index, item) {
                        //var name = $('option[value="'+item.sku+'"]').parent().eq(1).attr('data-name');

                        $(".koffer").append($("<li class='priceBoxKoffer'><span class='left'>" + item.amount + " x " + item.description + "</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                    });

                    var Surcharge = Math.round(allData.price_delivery_in);
                    $(".Surcharge").html(Surcharge);
                    /*$.each(allData.products, function (index, item) {
                     //alert("index"+index);alert("item"+item);alert(index.toSource());alert(item.toSource());
                     
                     $("."+data_name).html($("<li class='priceBoxKoffer'><span class='left'>" + item.amount + " x "+data_name+"</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                     })*/
                    //$("."+data_name).html("<li style='display:none'><span class='right subPrice'>" + Math.round(allData.price_in) + " €</span></li>");
                    var Surcharge = Math.round(allData.price_delivery_in);
                    if (Surcharge == 0)
                        Surcharge = "is gratis";
                    else
                        Surcharge = Surcharge;
                    $(".Surcharge").html(Surcharge);
                    $(".finalPrice").html(Math.round(allData.price_in));
                    //window.location = 'tussenpagina.php?price=' + price;
                }
            });
        }

        /*var totalPoints = 0;
         $(".priceUL li .subPrice").each(function (index) {
         var textVal = $(this).text();
         //alert( parseInt(textVal) );
         //alert(textVal.find('.subPrice').val());
         totalPoints += parseInt(textVal);
         //alert( totalPoints);
         $(".finalPrice").html(totalPoints);
         });*/

    });

    $(document.body).on('change', 'input[name="SKU_Qnt[]"]', function () {
        var value = $(this).val();
        var data_name = $(this).attr('data-name');

        if (value == 0) {
            $("." + data_name).html($(""));
            $(".col-sm-6 ." + data_name + "1").empty();
            $(".finalPrice").html($(""));
        } else {
            //alert(value);
            $(".col-sm-6 ." + data_name + "1").empty();
            for (var i = 0; i < value; i++) {
                var block = $("<div>", {"class": "block"});
                $(block).append($(".col-sm-6 ." + data_name + "1a").html());
                $(".col-sm-6 ." + data_name + "1").append(block);
            }
        }
        sku_values = {};
        $.each($(".col-sm-6 .commonContainer .block"), function (index, item) {
            var item_value = $(item).find('select option:selected').val();
            if (typeof sku_values[item_value] == "undefined") {
                sku_values[item_value] = 1;
            } else {
                sku_values[item_value] = sku_values[item_value] + 1;
                //$(".col-sm-6 .commonContainer .block").find('select option').attr('selected', true);
            }
        });
        console.log("ok");
        console.log(sku_values);
        //$(".col-sm-6 .commonContainer .block:first select option:selected").val()
        //
        //ajax call
        var countryList = $("#countryList2").val();
        //alert("country"+countryList);
        var retour = $("#Verzendtype").val();
        var productList = $("#klant1").val();
        //alert("product"+productList);

        if (typeof countryList == "undefined" && valTo == '') {
            //alert("Select country");
        } else {
            if (typeof countryList == "undefined") {
                countryList = valTo;
            }
            var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                /*if (i == 0) {
                 queryString = index.toLowerCase() + '=' + item;
                 i++;
                 } else {*/
                queryString += '&' + index.toLowerCase() + '=' + item;
                /*}
                 console.log(index);
                 console.log(item);*/
            })

            //alert(list);

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + queryString,
                async: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    //alert(response);
                    var allData = response.data;
                    //var notEmpty = jQuery.isEmptyObject(associative_array);
                    //alert(notEmpty);
                    //if(notEmpty == true){
                    $(".koffer").empty();
                    //}

                    $(".koffer").html('');
                    $.each(allData.products, function (index, item) {

                        //var name = $('option[value="'+item.sku+'"]').parent().eq(1).attr('data-name');

                        $(".koffer").append($("<li class='priceBoxKoffer'><span class='left'>" + item.amount + " x " + item.description + "</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                    });
                    var Surcharge = Math.round(allData.price_delivery_in);
                    if (Surcharge == 0)
                        Surcharge = "is gratis";
                    else
                        Surcharge = Surcharge;
                    $(".Surcharge").html(Surcharge);

                    $(".finalPrice").html(Math.round(allData.price_in));
                }
            });
        }

        /*var totalPoints = 0;
         $(".priceUL li .subPrice").each(function (index) {
         var textVal = $(this).text();
         //alert( parseInt(textVal) );
         //alert(textVal.find('.subPrice').val());
         totalPoints += parseInt(textVal);
         //alert( totalPoints);
         $(".finalPrice").html(totalPoints);
         });*/



    });
});


//var nowTemp = new Date();
//var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

var datedpd1 = new Date();
var newDatedpd1 = datedpd1.setDate(datedpd1.getDate() + 13);
var newDatedpd1set = datedpd1.setDate(datedpd1.getDate() + 1);
var checkinone = $('#dpd1').datepicker({
    format: 'dd-mm-yyyy',
    onRender: function (date) {
        return date.valueOf() <= newDatedpd1 ? 'disabled' : '';
    }
})
        .on('changeDate', function (ev) {
            if (ev.date.valueOf() >= checkinone.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                checkout.setValue(newDate);
            }
            var returnVal = 0;
            var destination_country = $("#countryList2").val();
            var pickup_country = $("#countryList_3").val();
            var flyDate = $("#dpd1").val();

            if (pickup_country == "") {
            } else if (destination_country == "") {
            } else if (flyDate == "") {
            } else {
                var newD = flyDate.split('-').reverse().join('-');
                //alert(newD);
                //alert("8");
                $.ajax({
                    url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country + "&destination_country=" + destination_country + "&date=" + newD + "&is_return=" + returnVal,
                    async: true,
                    success: function (response) {
                        var allData = response.data;
                        //alert(allData);				
                        $('#dpd3').empty();
                        $.each(allData, function (index, value) {
                            var newDtemp = value.split('-').reverse().join('-');
                            $("#dpd3").append("<option value=" + newDtemp + ">" + newDtemp + "</option>");

                        });

                        //var unavailableDates = ["2016-11-11"];

                        //alert(allData);
                        /*$('#dpd3').datepicker({
                         beforeShowDay:function(date){
                         dmy = date.getFullYear() + "-" +  (date.getMonth() + 1)  + "-" + date.getDate();
                         if ($.inArray(dmy, allData) == -1) {
                         return false;
                         } else {
                         return true;
                         }
                         },
                         format: 'yyyy-mm-dd'
                         }).show();	*/
                    }
                });
            }
            checkinone.hide();
            //$('#dpd2')[0].focus();
        })
        .data('datepicker');
//alert(directBooking);
if(directBooking != 1){
    $("#dpd1").datepicker("setValue", newDatedpd1set);
}    

var checkout = $('#dpd2').datepicker({
    format: 'dd-mm-yyyy',
    onRender: function (date) {
        return date.valueOf() <= checkinone.date.valueOf() ? 'disabled' : '';
    }
})
        .on('changeDate', function (ev) {
            var returnVal = 1;
            var destination_country = $("#countryList_3").val();
            var pickup_country = $("#countryList2").val();
            var flyDate = $("#dpd2").val();

            if (pickup_country == "") {
                /*$("#countryDropdown_31").closest('.form-group').removeClass('has-success').addClass('has-error');
                 $("#countryDropdown_31").removeClass('glyphicon-ok').addClass('glyphicon-remove');
                 $("#countryDropdown_3 select:first").focus();*/
                //bootbox.alert("please select Pickup country");
            } else if (destination_country == "") {
                /*$("#countryDropdown_21").closest('.form-group').removeClass('has-success').addClass('has-error');
                 $("#countryDropdown_21").removeClass('glyphicon-ok').addClass('glyphicon-remove');
                 $("#countryDropdown_2 select:first").focus();*/
                //bootbox.alert("please select Destination country");
            } else if (flyDate == "") {
                /*$("#dpd1").closest('.form-group').removeClass('has-success').addClass('has-error');
                 $("#dpd1").removeClass('glyphicon-ok').addClass('glyphicon-remove');
                 $("#dpd1").focus();*/
                //bootbox.alert("please select check in date");
            } else {
                var newDpd2 = flyDate.split('-').reverse().join('-');
                //alert("9");
                $.ajax({
                    url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country + "&destination_country=" + destination_country + "&date=" + newDpd2 + "&is_return=" + returnVal,
                    async: true,
                    success: function (response) {
                        var allData2 = response.data;
                        //var unavailableDates = ["2016-11-11"];

                        //alert(allData);
                        $('#dpd4').empty();
                        $.each(allData2, function (index, value) {
                            var newDtemp = value.split('-').reverse().join('-');
                            $("#dpd4").append("<option value=" + newDtemp + ">" + newDtemp + "</option>");

                        });
                    }
                });
            }
            checkout.hide();
        })
        .data('datepicker');

/*var checkin = $('#dpd3').datepicker({
 onRender: function (date) {
 return date.valueOf() < now.valueOf() ? 'disabled' : '';
 },
 format: 'yyyy-mm-dd'
 })
 .on('changeDate', function (ev) {
 if (ev.date.valueOf() > checkout.date.valueOf()) {
 var newDate = new Date(ev.date)
 newDate.setDate(newDate.getDate() + 1);
 checkout.setValue(newDate);
 }
 checkin.hide();
 $('#dpd4')[0].focus();
 })
 .data('datepicker');
 var checkout = $('#dpd4').datepicker({
 onRender: function (date) {
 return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
 },
 format: 'yyyy-mm-dd'
 })
 
 .on('changeDate', function (ev) {
 checkout.hide();
 }).data('datepicker');*/


$("#Verzendtype").on('change', function () {
    var type = $("#Verzendtype").val();
    if (type == 1) {
        $(".returnDateClass").css("display", "block");
    } else {
        $(".returnDateClass").css("display", "none");
    }
});


var date = new Date();
var newDate = date.setDate(date.getDate() + 13);
var newDateset = date.setDate(date.getDate() + 1);
var checkoutheader = $('#dpd1Header').datepicker({
    format: 'dd-mm-yyyy',
    onRender: function (date) {
        return date.valueOf() < newDate ? 'disabled' : '';
    }
})
        .on('changeDate', function (ev) {
            checkoutheader.hide();
        }).data('datepicker');

//$("#dpd1Header").datepicker("setValue", newDateset);

$.ajax({
    url: "http://fox.fireeagle.nl:8092/countries",
    success: function (response) {
        var allData = response.data;
        allData.sort(function (a, b) {
            var x = a.name.toLowerCase();
            var y = b.name.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        var html = "";
        html += "<select id='fromContry' name='fromContry' class='form-control custom'>";
        html += "<option value=''>Kies bestemming</option>";
        $.each(allData, function (index, value) {
            if (value.code == 'NL') {
                html += "<option value='" + value.code + "' selected='selected'> " + value.name + "</option>";
            } else {
                html += "<option value='" + value.code + "'> " + value.name + "</option>";
            }

        });
        html += "</select>";
        $("#fromContryDiv").html(html);
    }
});

$.ajax({
    url: "http://fox.fireeagle.nl:8092/countries",
    success: function (response) {
        var allData = response.data;
        allData.sort(function (a, b) {
            var x = a.name.toLowerCase();
            var y = b.name.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        var html = "";
        html += "<select id='toContry' name='toContry' class='form-control custom'>";
        html += "<option value=''>Kies bestemming</option>";
        $.each(allData, function (index, value) {
            if (country_code == value.code) {
                html += "<option value='" + value.code + "' selected='selected'> " + value.name + "</option>";
            } else {
                html += "<option value='" + value.code + "'> " + value.name + "</option>";
            }

        });
        html += "</select>";
        $("#toContryDiv").html(html);
    }
});


var dateco = new Date();
var newDateco = dateco.setDate(dateco.getDate() + 4);
var esimate = $('#esimatecheckOut').datepicker({
    onRender: function (date) {
        return date.valueOf() < newDateco ? 'disabled' : '';
    },
    format: 'dd-mm-yyyy'
})
        .on('changeDate', function (ev) {
            if (ev.date.valueOf() > esimatecheckout.date.valueOf()) {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate() + 1);
                esimatecheckout.setValue(newDate);
            }
            esimate.hide();
            //$('#esimateBackon')[0].focus();
        })
        .data('datepicker');
var esimatecheckout = $('#esimateBackon').datepicker({
    onRender: function (date) {
        return date.valueOf() <= esimate.date.valueOf() ? 'disabled' : '';
    },
    format: 'dd-mm-yyyy'
})

        .on('changeDate', function (ev) {
            //alert("khkhkh");
            var destCountry = $("#toContry").val();
            var SKU_dropdown = [];
            var SKU_quantity = [];
            var iterator = $("select[name='SKU_Drp[]']");
            for (var i = 0; i < iterator.length; i++) {
                var inp = iterator[i];
                //alert(inp.value);
                SKU_dropdown.push(inp.value);
            }

            var iterator2 = $("input[name='SKU_Qnt[]']");
            for (var i = 0; i < iterator2.length; i++) {
                var inp2 = iterator2[i];
                //alert(inp2.value);
                SKU_quantity.push(inp2.value);
            }

            var associative_array2 = new Object();
            for (var i = 0; i < SKU_dropdown.length; i++) {
                associative_array2[SKU_dropdown[i]] = SKU_quantity[i];
            }

            var queryString = '';
            $.each(associative_array2, function (index, item) {
                queryString += '&' + index.toLowerCase() + '=' + item;
            })

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + destCountry + "&retour=" + 1 + queryString,
                async: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    //alert(response);
                    var allData = response.data;
                    var Surcharge = Math.round(allData.price_delivery_in);
                    $(".Surcharge").html(Surcharge);
                    if (Surcharge == 0)
                        Surcharge = "is gratis";
                    else
                        Surcharge = Surcharge;
                    $(".finalPrice").html(Math.round(allData.price_in));
                }
            });
            esimatecheckout.hide();
        }).data('datepicker');



//estimate page functinos


var products = {};
$.ajax({
    url: "http://fox.fireeagle.nl:8092/products",
    success: function (response) {
        var allData = response.data;
        allData.sort(function (a, b) {
            var x = a.description.toLowerCase();
            var y = b.description.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        var html = "";
        html += "<select id='productListEST' class='form-control custom'>";
        html += "<option value=''>Selecteer bagage om te versturen</option>";
        $.each(allData, function (index, value) {
            products[value.code] = value.description;
            //if(productList == value.code){
            //    html += "<option value='" + value.code + "' selected='selected'> " + value.description + "</option>";
            //}else{
            html += "<option value='" + value.code + "'> " + value.description + "</option>";
            //}    
        });
        html += "</select>";
        $("#ProductEst").html(html);
    }
});

//console.log(products);

$(document).ready(function () {
    /*for ($i = 1; $i <= 8; $i++) {
     if(SKU == "SKU_1"){
     //alert(SKU);
     $(".addProducts").html("<div class='col-xs-9'><input class='form-control' readonly id='sku1ID' type='text' value='Koffer groot Hard Case'></div><div class='col-xs-3'><div class='form-group form-fields'><input type='number' id='sku1' class='form-control' min='1' value='1'></div></div>");
     //$('#productList option[value="SKU_8"]').attr('selected', true);
     }
     }*/

    var new_dropdown = $('#productListClone').clone();
    var typeID2 = SKU + "_div";
    new_dropdown.attr('id', typeID2);
    new_dropdown.attr('class', "changequantity");
    $(new_dropdown).css("display", "block");
    /*$.ajax({
     url: "http://fox.fireeagle.nl:8092/products",
     success: function (response) {
     var allData = response.data;
     allData.sort(function (a, b) {
     var x = a.description.toLowerCase();
     var y = b.description.toLowerCase();
     return x < y ? -1 : x > y ? 1 : 0;
     });
     $.each(allData, function (index, value) {
     if (SKU == value.code) {
     $(new_dropdown).find('.cloneDrp').val(value.description);
     }
     });
     }
     });*/
    $(new_dropdown).find('option[value="' + SKU + '"]').attr('selected', true).attr('disabled', false);
    $(new_dropdown).find('.cloneDrp').attr('name', "SKU_Drp[]");
    $(new_dropdown).find('.cloneDrp').attr('data-sku-name', SKU);
    $(new_dropdown).find('.inputFieldNumber').attr('id', SKU);
    $(new_dropdown).find('.inputFieldNumber').attr('name', "SKU_Qnt[]");
    $('.addProducts').append(new_dropdown);


    $(document.body).on("change", "#productListEST", function () {
        //var optionSelected = $("option:selected", this);
        var valueSelected = this.value;

        if ($('div#' + valueSelected + '_div').length > 0) {
            $('.inputFieldNumber#' + valueSelected).val(parseInt($('.inputFieldNumber#' + valueSelected).val()) + 1);
            $(".inputFieldNumber#" + valueSelected).trigger("change");
            return false;
        } else {
            var new_dropdown = $('#productListClone').clone();
            var typeID = valueSelected + "_div";
            new_dropdown.attr('id', typeID);
            new_dropdown.attr('class', "changequantity");
            $(new_dropdown).css("display", "block");
            /*$.ajax({
             url: "http://fox.fireeagle.nl:8092/products",
             success: function (response) {
             var allData = response.data;
             allData.sort(function (a, b) {
             var x = a.description.toLowerCase();
             var y = b.description.toLowerCase();
             return x < y ? -1 : x > y ? 1 : 0;
             });
             $.each(allData, function (index, value) {
             if (valueSelected == value.code) {
             $(new_dropdown).find('.cloneDrp').val(value.description);
             }
             });
             }
             });*/
            $(new_dropdown).find('option[value="' + valueSelected + '"]').attr('selected', true).attr('disabled', false);
            $(new_dropdown).find('.cloneDrp').attr('name', "SKU_Drp[]");
            $(new_dropdown).find('.cloneDrp').attr('data-sku-name', valueSelected);
            $(new_dropdown).find('.inputFieldNumber').attr('id', valueSelected);
            $(new_dropdown).find('.inputFieldNumber').attr('name', "SKU_Qnt[]");
            $('.addProducts').append(new_dropdown);


            var getID = $(".inputFieldNumber").attr('id');
            var value = $("#" + getID).val();
            var countryList = $("#toContry").val();
            var returnDate = $("#esimateBackon").val();
            var retour = 0;
            if (returnDate == "")
                retour = 0;
            else
                retour = 1;

            if (value == 0) {

            } else {
                if (countryList == 0) {
                    $("#toContry1").html("Selecteer land");
                    $("#" + getID).val(0);
                } else {
                    $("#toContry1").html("");
                }

                var sku_values = {};
                $.each($(".addProducts .changequantity"), function (index, item) {
                    var item_value = $(item).find('.cloneDrp').data('sku-name');
                    //var item_value = $(item).find('select option:selected').val();
                    var quantity_value = $(item).find('.inputFieldNumber').val();

                    //if (typeof sku_values[item_value] == "undefined") {
                    //  sku_values[item_value] = 1;
                    //} else {
                    sku_values[item_value] = quantity_value;
                    //}
                });


                console.log(sku_values);

                var queryString = '';
                $.each(sku_values, function (index, item) {
                    queryString += '&' + index.toLowerCase() + '=' + item;
                    console.log(index);
                    console.log(item);
                });

                //alert("queryString" + queryString);
                //ajax call

                $.ajax({
                    url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + queryString,
                    async: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        //alert(response);
                        var allData = response.data;
                        $(".kofferUL").empty();
                        $.each(allData.products, function (index, item) {
                            //alert("index"+index);alert("item"+item);alert(index.toSource());alert(item.toSource());

                            //$(".KitesurfUL").append($("<li class='priceBoxKitesurf'><span class='left'>" + item.amount + " x Kitesurf</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                        })
                        //alert(allData.price_in);
                        var Surcharge = Math.round(allData.price_delivery_in);
                        if (Surcharge == 0)
                            Surcharge = "is gratis";
                        else
                            Surcharge = Surcharge;
                        $(".Surcharge").html(Surcharge);
                        $(".kofferUL").append("<li style='display:none'><span class='right subPrice'>" + Math.round(allData.price_in) + " €</span></li>");
                        //window.location = 'tussenpagina.php?price=' + price;
                    }
                });
                var totalPoints = 0;
                $(".priceUL li .subPrice").each(function (index) {
                    var textVal = $(this).text();
                    //alert( parseInt(textVal) );
                    //alert(textVal.find('.subPrice').val());
                    totalPoints += parseInt(textVal);
                    //alert( totalPoints);
                    $(".finalPrice").html(totalPoints);
                });
            }
        }
        $("#productListEST").val("");
    });


    $(document.body).on("change", ".inputFieldNumber", function () {
        //$(".changequantity").change(function () {
        var getID = $(".inputFieldNumber").attr('id');
        //var value = $("#"+getID).val();
        var inputId = $(this).attr('id');
        var value = $(this).val();

        var countryList = $("#toContry").val();
        //alert("country"+countryList);
        //alert("value"+value);
        var returnDate = $("#esimateBackon").val();
        var retour = 0;
        if (returnDate == "")
            retour = 0;
        else
            retour = 1;
        //alert(value);
        if (value == 0) {
            var typeID = $(this).parents('div').parents('div').parents('div').attr('id');
            $('#' + typeID).remove();//alert('#'+typeID);
            //$(".KitesurfUL").html($(""));
            //$(".col-sm-6 .blockContainer4").empty();
            //$(".finalPrice").html($(""));
        } else {
            //alert(value);
            //$(".col-sm-6 .blockContainer4").empty();
            if (countryList == 0) {
                //alert("Select country");
                $("#toContry1").html("Selecteer land");
                $("#" + getID).val(0);
            } else {
                $("#toContry1").html("");
            }
        }

        var sku_values = {};
        $.each($(".addProducts .changequantity"), function (index, item) {
            var item_value = $(item).find('.cloneDrp').data('sku-name');
            //var item_value = $(item).find('select option:selected').val();
            var quantity_value = $(item).find('.inputFieldNumber').val();

//            var item_value = $(item).find('select option:selected').val();
//            var quantity_value = $(item).parents('.changequantity').find('.inputFieldNumber').val();
            sku_values[item_value] = quantity_value;
        });
        console.log(sku_values);
        //$(".col-sm-6 .commonContainer .block:first select option:selected").val()



        //ajax call


        if (countryList == 0) {
            //alert("Ssselect country");
            $("#toContry1").html("Selecteer land");
            $("#" + getID).val(0);
        } else {
            $("#toContry1").html("");
            var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                /*if (i == 0) {
                 queryString = index.toLowerCase() + '=' + item;
                 i++;
                 } else {*/
                queryString += '&' + index.toLowerCase() + '=' + item;
                //}
                console.log(index);
                console.log(item);
            })

            //alert("queryString"+queryString);

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + queryString,
                async: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    //alert(response);
                    var allData = response.data;
                    $(".kofferUL").empty();
                    $.each(allData.products, function (index, item) {
                        //alert("index"+index);alert("item"+item);alert(index.toSource());alert(item.toSource());

                        //$(".KitesurfUL").append($("<li class='priceBoxKitesurf'><span class='left'>" + item.amount + " x Kitesurf</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
                    })
                    //alert(allData.price_in);
                    var Surcharge = Math.round(allData.price_delivery_in);
                    if (Surcharge == 0)
                        Surcharge = "is gratis";
                    else
                        Surcharge = Surcharge;
                    $(".Surcharge").html(Surcharge);
                    $(".kofferUL").append("<li style='display:none'><span class='right subPrice'>" + Math.round(allData.price_in) + " €</span></li>");
                    //window.location = 'tussenpagina.php?price=' + price;
                }
            });
        }
        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            //alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });

    });


});

$(document).ready(function () {

    if (esimatecheckOut != "") {
        $("#dpd1").trigger("change");
    }

    if (esimateBackon != "") {
        $("#dpd2").trigger("change");
    }


    $.each(associative_array, function (index, value) {
        /*console.log("khushbu");
         console.log(index);
         console.log("shah");
         console.log(value);*/
        if (index == "SKU_1" || index == "SKU_2" || index == "SKU_8") {
            if ($("#Koffer_3").val() == '' || $("#Koffer_3").val() == 0) {
                $("#Koffer_3").val(value);
            } else {
                $("#Koffer_3").val(parseInt($("#Koffer_3").val()) + parseInt(value));
            }
            console.log(index);
            $("#Koffer_3").trigger("change");

        } else if (index == "SKU_5" || index == "SKU_6" || index == "SKU_7") {
            //alert(index);
            //$("#snowboard_3").val(value);
            if ($("#snowboard_3").val() == '' || $("#snowboard_3").val() == 0) {
                $("#snowboard_3").val(value);
            } else {
                $("#snowboard_3").val(parseInt($("#snowboard_3").val()) + parseInt(value));
            }
            $("#snowboard_3").trigger("change");
        } else if (index == "SKU_4") {
            //alert(index);
            //$("#GolfFiets_3").val(value);
            if ($("#GolfFiets_3").val() == '' || $("#GolfFiets_3").val() == 0) {
                $("#GolfFiets_3").val(value);
            } else {
                $("#GolfFiets_3").val(parseInt($("#GolfFiets_3").val()) + parseInt(value));
            }
            $("#GolfFiets_3").trigger("change");
        } else if (index == "SKU_3") {
            //alert(index);
            //$("#Kitesurf_3").val(value);
            if ($("#Kitesurf_3").val() == '' || $("#Kitesurf_3").val() == 0) {
                $("#Kitesurf_3").val(value);
            } else {
                $("#Kitesurf_3").val(parseInt($("#Kitesurf_3").val()) + parseInt(value));
            }
            $("#Kitesurf_3").trigger("change");
        }
        //$("input[name='SKU_Qnt[]'").trigger("change");

    });

});

//$( "#emailForm" ).submit(function( event ) {
/*$(".sendEmail").on('click', function () {
 
 });*/


$('#emailForm').validate({
    rules: {
        emailId: {
            required: true
        }
    },
    messages: {
        emailId: {
            required: "Vergeet dit veld niet in te vullen"
        }
    },
    highlight: function (element) {
        var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
    },
    unhighlight: function (element) {
        var id_attr = "#" + $(element).attr("id") + "1";
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.length) {
            error.insertAfter(element);
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
        $('#myModal').modal('toggle');
        var destCountry = $("#toContry").val();
        var email = $("#emailId").val();
        var SKU_dropdown = [];
        var SKU_quantity = [];
        var iterator = $("select[name='SKU_Drp[]']");
        for (var i = 0; i < iterator.length; i++) {
            var inp = iterator[i];
            //alert(inp.value);
            SKU_dropdown.push(inp.value);
        }

        var iterator2 = $("input[name='SKU_Qnt[]']");
        for (var i = 0; i < iterator2.length; i++) {
            var inp2 = iterator2[i];
            //alert(inp2.value);
            SKU_quantity.push(inp2.value);
        }

        var associative_array2 = new Object();
        for (var i = 0; i < SKU_dropdown.length; i++) {
            associative_array2[SKU_dropdown[i]] = SKU_quantity[i];
        }

        var associative_array3 = new Object();
        $.each(associative_array2, function (index, value) {
            var newIndex = index.split("_").join("");
            associative_array3[newIndex] = parseInt(value);
        });

        //alert(associative_array2.toSource());
        var markers = {"email_address": email,
            "destination_country": destCountry,
            "retour": true,
            "products": associative_array3
        };


        $.ajax({
            url: "http://fox.fireeagle.nl:8092/mailing/sendpricequote",
            type: "POST",
            data: JSON.stringify({"email_address": email,
                "destination_country": destCountry,
                "retour": true,
                "products": associative_array3
            }),
            contentType: "application/json",
            dataType: "json",
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (data) {//alert(data.data.Status);
                if (data.data.Status == "Accepted") {
                    bootbox.alert("Je e-mail is verzonden, je krijgt zo snel mogelijk een antwoord");
                } else {
                    bootbox.alert("error verzenden van e-mail");
                }
            },
            failure: function (errMsg) {
                //alert(errMsg);
            }
        });
    }
});


$('#contact_form4').validate({
    rules: {
        option_4: {
            required: true
        }
    },
    messages: {
        option_4: {
            required: "Vergeet dit veld niet in te vullen"
        }
    },
    highlight: function (element) {
        var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
    },
    unhighlight: function (element) {
        var id_attr = "#" + $(element).attr("id") + "1";
        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function (error, element) {
        if (element.attr("type") == "checkbox") {
            element.parent().after(error);
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) {
        $(".booking5").fadeIn();
        $(".booking4").fadeOut();
        //$('.BookingSteps a.step.active').removeClass('active');
        $(window).scrollTop(0);
    }
});

$(document).ready(function () {

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/pay/banks",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<option value=''>Selecteer je bank</option>";
            $.each(allData, function (index, value) {
                if (country_code == value.code) {
                    html += "<option value='" + value.id + "' selected='selected'> " + value.name + "</option>";
                } else {
                    html += "<option value='" + value.id + "'> " + value.name + "</option>";
                }

            });
            html += "</select>";
            $("#bankNames").append(html);
        }
    });


    $('#estimatePage').validate({
        rules: {
            esimateBackon: {
                required: true,
            },
            fromContry: {
                required: true,
            },
            toContry: {
                required: true
            }
        },
        messages: {
            esimateBackon: {
                required: "Vergeet dit veld niet in te vullen",
                minlength: "Please enter minimum {0} characters."
            },
            fromContry: {
                required: "Vergeet dit veld niet in te vullen"
            },
            toContry: {
                required: "Vergeet dit veld niet in te vullen"
            }
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var from = $("#fromContry").val();
            var to = $("#toContry").val();//alert(from);alert(to);
            if (from == "NL" || to == "NL") {
                $("#estimatePage").submit();
            } else {
                bootbox.alert("One of the country should be  Nederland");
            }

        }
    });

    $('#contact_form5').validate({
        rules: {
            bankNames: {
                required: true
            }
        },
        messages: {
            bankNames: {
                required: "Vergeet dit veld niet in te vullen"
            }
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.attr("type") == "checkbox") {
                element.parent().after(error);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            var salutation = $("input[name='options']").val();
            var Inserts = $("#Tussenvoegsels").val();
            var surname = $("#Achternaam").val();
            var initials = $("#Voorletters").val();
            var email = $("#email").val();
            var countries = $("#countries").val();

            var clientDepartureDate = $("#dpd1").val();/*alert(clientDepartureDate);"2016-12-16";*/
            var clientReturnDate = $("#dpd2").val();/*alert(clientReturnDate);"2016-12-24";*/
            var pickUpCountry = $("#countryList_3").val();
            var pickUpPostcode = $("#Postcode").val();
            var pickUpHouseNumber = $("#Huisnummer").val();
            var pickUpStreet = $("#Straat").val();
            var pickUpcity = $("#Woonplaats").val();
            var pickUpHouseNumberAdditional = $("#Toevoeging").val();
            var returnVal = $("#Verzendtype").val();
            if (returnVal == "on")
                returnVal = true;
            else
                returnVal = false;
            var dpd3 = $("#dpd3").val();/*alert(dpd3);"2016-12-12";*/
            var dpd4 = $("#dpd4").val();/*alert(dpd4);"2016-12-29";*/
            var deliverToneighbour = $("#option_2").val();//alert(deliverToneighbour);
            if (deliverToneighbour == "on")
                deliverToneighbour = true;
            else
                deliverToneighbour = false;
            var destinationCountry = $("#countryList2").val();
            var destinationHotel = $("#Hotel_2").val();
            var destinationPostcode = $("#Postcode_2").val();
            var destinationHouseNumber = "ddd15";//$("#Huisnummer_2").val();
            var destinationadditionals = "ddd16";//$("#Toevoeging_2").val();
            var destiationStreet = $("#Straat_2").val();
            var destinationResidence = $("#Woonplaats_2").val();		//notused	

            var associative_array3 = new Object();
            /*var SKU_dropdown = [];var SKU_quantity = [];
             var iterator = $("select[name='SKU_Drp[]']");
             for (var i = 0; i <iterator.length; i++) {
             var inp=iterator[i];
             //alert(inp.value);
             SKU_dropdown.push(inp.value);
             }
             
             var iterator2 = $("input[name='SKU_Qnt[]']");
             for (var i = 0; i <iterator2.length; i++) {
             var inp2=iterator2[i];
             //alert(inp2.value);
             SKU_quantity.push(inp2.value);
             }
             
             var associative_array3=new Object();
             for(var i=0;i<SKU_dropdown.length;i++){
             associative_array3[SKU_dropdown[i]]=SKU_quantity[i];
             }
             //alert(JSON.stringify(associative_array3));
             var product = JSON.stringify(associative_array3);*/

            if (directBooking == 1) {
                $.each(associative_array, function (index, value) {
                    var newIndex = index.toLowerCase();
                    associative_array3[newIndex] = parseInt(value);
                });
                console.log("test");
                console.log(associative_array3);
            } else {
                sku_values = {};
                $.each($(".col-sm-6 .commonContainer .block"), function (index, item) {
                    var item_value = $(item).find('select option:selected').val();
                    if (typeof sku_values[item_value] == "undefined") {
                        sku_values[item_value] = 1;
                    } else {
                        sku_values[item_value] = sku_values[item_value] + 1;
                    }
                });

                $.each(sku_values, function (index, item) {
                    var newIndex = index.toLowerCase();
                    associative_array3[newIndex] = parseInt(item);
                })
                console.log("okkkk");
                console.log(associative_array3);
            }

            var clientDDate = clientDepartureDate.split('-').reverse().join('-');
            var pkgPDate = dpd3.split('-').reverse().join('-');
            var clientReturnDateNew = clientReturnDate.split('-').reverse().join('-');
            var dpd4New = dpd4.split('-').reverse().join('-');
            var markers = {"customer": {"title": salutation, "first_name": initials, "preposition": Inserts, "last_name": surname, "email": email, "phone": countries, "address": {"city": pickUpcity, "zipcode": pickUpPostcode, "street": pickUpStreet, "housenumber": "230", "addition": pickUpHouseNumberAdditional, "country_code": pickUpCountry}, "communication": {"mail_opt_in": false, "email_opt_in": false}}, "booking": {"retour": returnVal, "coupon": null, "affiliate": null, "products": associative_array3, "pickup": {"client_departure_date": clientDDate, "package_pickup_date": pkgPDate, "details": {"title": salutation, "first_name": initials, "preposition": Inserts, "last_name": surname, "phone": countries, "address": {"city": pickUpcity, "zipcode": pickUpPostcode, "street": pickUpStreet, "housenumber": "230", "addition": pickUpHouseNumberAdditional, "country_code": pickUpCountry}}}, "destination": {"details": {"company": destinationadditionals, "title": salutation, "first_name": initials, "preposition": Inserts, "last_name": surname, "phone": countries, "address": {"city": destinationResidence, "zipcode": destinationPostcode, "street": destiationStreet, "housenumber": "230", "addition": destinationadditionals, "country_code": destinationCountry}}}, "return": {"can_deliver_at_neighbours": deliverToneighbour, "client_return_date": clientReturnDateNew, "package_delivery_date": dpd4New, "details": {"title": salutation, "first_name": initials, "preposition": Inserts, "last_name": surname, "phone": countries, "address": {"city": pickUpcity, "zipcode": pickUpPostcode, "street": pickUpStreet, "housenumber": "230", "addition": pickUpHouseNumberAdditional, "country_code": pickUpCountry}}}}};

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/website/addbooking",
                type: "POST",
                data: JSON.stringify(markers),
                contentType: "application/json",
                dataType: "json",
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (data) {//alert(data.toSource());
                    //alert("Your reference booking number :"+data.data.booking_number);
                    //bootbox.alert("Your reference booking number :"+data.data.booking_number,function(result) {
                    //alert(result);
                    //if (result=='undefined' || result == undefined || result == false) {
                    window.location = data.data.transaction.paymentURL;
                    // bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>' })
                    //}
                    //});

                },
                failure: function (errMsg) {
                    alert(errMsg);
                },
                statusCode: {
                    500: function (msg) {
                        //alert(msg.toSource());
                        bootbox.alert(msg.responseJSON.error.msg);
                        //alert(msg.responseJSON.error.msg);
                    }
                }
            });
        }
    });
});






$(document.body).on("change", "#toContry", function () {
    if ($(this).val() == 'NL' && $('#fromContry').val() == 'NL') {

    } else {
        if ($(this).val() == 'NL') {
            $('#fromContry').val('');
        } else {
            $('#fromContry').val('NL');
        }
    }



    //var valueSelected = $("#productListEST").val();
    callToCountry();
});




/*$(document.ready).on("change", "#dpd3", function () {
 alert("sdkfjsdfjhsdf");
 var returnVal = 0;
 var destination_country = $("#countryList_3").val();
 var pickup_country = $("#countryList2").val();
 var flyDate = $("#dpd1").val();
 
 $.ajax({
 url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country=" + pickup_country + "&destination_country=" + destination_country + "&date=" + flyDate + "&is_return=" + returnVal,
 async: true,
 success: function (response) {
 var allData = response.data;
 
 alert(allData);
 
 }
 });
 });*/


$(document.body).on("change", "#fromContry", function () {
    if ($(this).val() == 'NL' && $('#toContry').val() == 'NL') {

    } else {
        if ($(this).val() == 'NL') {
            $('#toContry').val('');
        } else {
            $('#toContry').val('NL');
            callToCountry();
        }
    }
});



function callToCountry() {
    var returnDate = $("#esimateBackon").val();
    var retour = 0;
    if (returnDate == "")
        retour = 0;
    else
        retour = 1;
    var countryList = $("#toContry").val();

    if (countryList == 0) {
        $("#toContry1").html("Selecteer land");
    } else {
        $("#toContry1").html("");
    }

    var sku_values = {};
    $.each($(".addProducts .block"), function (index, item) {
        var item_value = $(item).find('.cloneDrp').data('sku-name');
        var quantity_value = $(item).parents('.changequantity').find('.inputFieldNumber').val();
        sku_values[item_value] = quantity_value;
    });

    console.log(sku_values);

    var queryString = '';
    $.each(sku_values, function (index, item) {
        queryString += '&' + index.toLowerCase() + '=' + item;
        console.log(index);
        console.log(item);
    });

    //alert("queryString" + queryString);
    //ajax call

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + queryString,
        async: false,
        beforeSend: function () {
            $.blockUI();
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            //alert(response);
            var allData = response.data;
            $(".kofferUL").empty();
            $.each(allData.products, function (index, item) {
                //alert("index"+index);alert("item"+item);alert(index.toSource());alert(item.toSource());

                //$(".KitesurfUL").append($("<li class='priceBoxKitesurf'><span class='left'>" + item.amount + " x Kitesurf</span><span class='right'>" + Math.round(item.piece_price) + " €</span></li>"));
            })
            //alert(allData.price_in);
            var Surcharge = Math.round(allData.price_delivery_in);
            if (Surcharge == 0)
                Surcharge = "is gratis";
            else
                Surcharge = Surcharge;
            $(".Surcharge").html(Surcharge);
            $(".kofferUL").append("<li style='display:none'><span class='right subPrice'>" + Math.round(allData.price_in) + " €</span></li>");
            //window.location = 'tussenpagina.php?price=' + price;
        }
    });
    var totalPoints = 0;
    $(".priceUL li .subPrice").each(function (index) {
        var textVal = $(this).text();
        //alert( parseInt(textVal) );
        //alert(textVal.find('.subPrice').val());
        totalPoints += parseInt(textVal);
        //alert( totalPoints);
        $(".finalPrice").html(totalPoints);
    });
    $("#productListEST").val("");
}

$(document.body).on("change", "#VerzendtypeQuick", function () {
    //alert(this.value);
    if (this.value == 0) {
        $(".returnDateClass1").css("display", "none");
    } else {
        $(".returnDateClass1").css("display", "block");
    }
    callToCountry()
});



var placesAutocomplete = places({
    container: document.querySelector('#Addres')
});
placesAutocomplete.on('change', function resultSelected(e) {
    document.querySelector('#Straat_2').value = e.suggestion.name || '';
    document.querySelector('#Woonplaats_2').value = e.suggestion.city || '';
    setCountry = e.suggestion.countryCode || '';
    document.querySelector('#Postcode_2').value = e.suggestion.postcode || '';

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/countries",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='countryList2' name='countryList2' class='form-control custom'>";
            html += "<option value=''>Kies bestemming</option>";
            $.each(allData, function (index, value) {
                if (setCountry.toUpperCase() == value.code) {
                    html += "<option value='" + value.code + "' selected='selected'> " + value.name + "</option>";
                } else {
                    html += "<option value='" + value.code + "'> " + value.name + "</option>";
                }
            });
            html += "</select>";
            $("#countryDropdown_2").html(html);
        }
    });
});	