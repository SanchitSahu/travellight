var sku_values = {};
$(document).ready(function () {
    $('#contact_form1').validate({
        rules: {
            Klant1: {
                required: true
            },
            options: {
                required: true
            },
            Voorletters: {
                required: true,
                minlength: 2
            },
            Tussenvoegsels: {
                required: true,
                minlength: 5
            },
            Achternaam: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            phonenumber: {
                required: true,
                minlength: 5,
                number: true
            }
        },
        messages: {
            Klant1: {
                required: "Je bent vergeten dit veld in te vullen",
            },
            options: {
                required: "Je bent vergeten dit veld in te vullen"
            },
            Voorletters: {
                required: "Je bent vergeten dit veld in te vullen",
                minlength: "Please enter minimum {0} characters."
            },
            Tussenvoegsels: {
                required: "Je bent vergeten dit veld in te vullen",
                minlength: "Please enter minimum {0} characters."
            },
            Achternaam: {
                required: "Je bent vergeten dit veld in te vullen",
                minlength: "Please enter minimum {0} characters."
            },
            email: {
                required: "Je bent vergeten dit veld in te vullen",
                minlength: "Please enter minimum {0} characters."
            },
            phonenumber: {
                required: "Je bent vergeten dit veld in te vullen",
                minlength: "Please enter minimum {0} characters.",
                number: "enter only numbers"
            },
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            } if (element.attr("name") == "options") {
                element.parent().parent().parent().last().after(error);
            }else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $(".booking2").fadeIn();
            $(".booking1").fadeOut();
        }
    });



    $('#contact_form2').validate({
        rules: {
            dpd1: {
                required: true,
                minlength: 5
            },
            dpd2: {
                required: true,
                minlenght: 5
            },
            countryList_3: {
                required: true
            },
            Postcode: {
                required: true,
                minlength: 5,
                number:true
            },
            Huisnummer: {
                required: true,
                minlength: 5
            },
            Toevoeging: {
                required: true,
                minlength: 5
            },
            Straat: {
                required: true,
                minlength: 5
            },
            Woonplaats: {
                required: true,
                minlength: 5
            },
            Verzendtype1: {
                required: true
            },
            dpd3: {
                required: true
            },
            dpd4: {
                required: true
            },
            option_2: {
                required: true,
                minlength: 5,
                number: true
            },
            countryDropdown_2: {
                required: true,
                minlength: 5,
                number: true
            },
            Hotel_2: {
                required: true,
                minlength: 5
            },
            Postcode_2: {
                required: true,
                minlength: 5,
                number: true
            },
            Huisnummer_2: {
                required: true,
                minlength: 5
            },
            Toevoeging_2: {
                required: true,
                minlength: 5
            },
            Straat_2: {
                required: true,
                minlength: 5
            },
            Woonplaats_2: {
                required: true,
                minlength: 5
            }
        },
        messages: {
            dpd1: {
                required: "Please fill this detail",
                minlength: "Please enter minimum {0} characters."
            },
            dpd2: {
                required: "Please fill this detail"
            },
            countryList_3: {
                required: "Please fill this detail"
            },
            Postcode: {
                required: "Please fill this detail"
            },
            Huisnummer: {
                required: "Please fill this detail"
            },
            Toevoeging: {
                required: "Please fill this detail"
            },
            Straat: {
                required: "Please fill this detail"
            },
            Woonplaats: {
                required: "Please fill this detail"
            },
            Verzendtype: {
                required: "Please fill this detail"
            },
            dpd3: {
                required: "Please fill this detail"
            },
            dpd4: {
                required: "Please fill this detail"
            },
            option_2: {
                required: "Please fill this detail"
            },
            countryDropdown_2: {
                required: "Please fill this detail"
            },
            Hotel_2: {
                required: "Please fill this detail"
            },
            Postcode_2: {
                required: "Please fill this detail"
            },
            Huisnummer_2: {
                required: "Please fill this detail"
            },
            Toevoeging_1: {
                required: "Please fill this detail"
            },
            Straat_2: {
                required: "Please fill this detail"
            },
            Woonplaats_2: {
                required: "Please fill this detail"
            }
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            //alert("ff");
            $(".booking3").fadeIn();
            $(".booking2").fadeOut();
        }
    });
    
    
    $('#contact_form3').validate({
        rules: {
            
        },
        messages: {
            
        },
        highlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";//alert(id_attr);
            $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
        },
        unhighlight: function (element) {
            var id_attr = "#" + $(element).attr("id") + "1";
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.length) {
                error.insertAfter(element);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            //alert("ff");
            var returnVal = $("#Verzendtype").val();
            var destination_country = $("#countryList_3").val();
            var pickup_country = $("#countryList2").val();
            var flyDate = $("#dpd3").val();
            
            
            
            var Huisnummer = $("#Huisnummer").val(); //ADDRESS
            var countryList_3 = $("#countryList_3").val();
            var Huisnummer_2 = $("#Huisnummer_2").val();
            var Hotel_2 = $("#Hotel_2").val();
            var Woonplaats_2 = $("#Woonplaats_2").val(); //residence
            var countryList2 = $("#countryList2").val();
            var dpd3 = $("#dpd3").val();
            var dpd4 = $("#dpd4").val();
            
            $.ajax({
                url: "http://fox.fireeagle.nl:8092/leadtimes?pickup_country="+pickup_country+"&destination_country="+destination_country+"&date="+flyDate+"&is_return="+returnVal,
                async:true,
                success: function (response) {
                    var allData = response.data;
                    
                    //alert(allData);
                    var html="<p>Verzending</p><p>We komen je bagage afhalen op de "+Huisnummer+", "+countryList_3+",  op "+allData+" tussen 11:45 uur en 17:00 uur.</p><p>Aflevering</p><p>We leveren je bagage uiterlijk "+dpd3+" af op de "+Huisnummer_2+", bij de receptie van Hotel  "+Hotel_2+".</p><p>Retourzending</p><p>Vervolgens halen we vanaf "+Woonplaats_2+" je bagage bij "+dpd3+" weer op en zal dit uiterlijk xxx tussen 1145 en 1700 uur afgeleverd worden op de  "+Woonplaats_2+", "+countryList_3+"..</p>";
                    $(".summaryText").html(html);
                }
            });
            $(".booking4").fadeIn();
            $(".booking3").fadeOut();
        }
    });
    /*$("#contact_form1").bootstrapValidator({  
     feedbackIcons: {
     valid: 'glyphicon glyphicon-ok',
     invalid: 'glyphicon glyphicon-remove',
     validating: 'glyphicon glyphicon-refresh'
     },
     fields: {
     Klant1: {
     validators: {
     notEmpty: {
     message: 'Please fill this detail'
     }
     }
     },
     options: {
     // The threshold option does not effect to the elements
     // which user cannot type in, such as radio, checkbox, select, etc.
     threshold: 5,
     validators: {
     notEmpty: {
     message: 'The summary is required'
     }
     }
     },
     Voorletters: {
     validators: {
     stringLength: {
     min: 2,
     },
     notEmpty: {
     message: 'Please fill this detail'
     }
     }
     },
     phonenumber: {
     validators: {
     notEmpty: {
     message: 'Please fill this detail'
     }
     }
     },
     },
     submitHandler: function(validator, form, submitButton) {
     //alert("f");
     $(".booking2").fadeIn();
     $(".booking1").fadeOut();
     },
     });*/


    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please fill this detail'
                    }
                }
            },
            last_name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please fill this detail'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please fill this detail'
                    },
                    emailAddress: {
                        message: 'Please fill this detail'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Please fill this detail'
                    },
                    phone: {
                        country: 'US',
                        message: 'Please fill this detail'
                    }
                }
            },
            address: {
                validators: {
                    stringLength: {
                        min: 8,
                    },
                    notEmpty: {
                        message: 'Please fill this detail'
                    }
                }
            },
            city: {
                validators: {
                    stringLength: {
                        min: 4,
                    },
                    notEmpty: {
                        message: 'Please fill this detail'
                    }
                }
            },
            Klant: {
                validators: {
                    notEmpty: {
                        message: 'Please fill this detail'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please fill this detail'
                    },
                    zipCode: {
                        country: 'US',
                        message: 'Please fill this detail'
                    }
                }
            },
            comment: {
                validators: {
                    stringLength: {
                        min: 10,
                        max: 200,
                        message: 'Please fill this detail'
                    },
                    notEmpty: {
                        message: 'Please fill this detail'
                    }
                }
            }
        }
    })
            .on('success.form.bv', function (e) {
                $('#success_message').slideDown({opacity: "show"}, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

                // Prevent form submission
                e.preventDefault();

                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                $.post($form.attr('action'), $form.serialize(), function (result) {
                    console.log(result);
                }, 'json');
            });
});


$(document).ready(function () {
    $.ajax({
        url: "http://fox.fireeagle.nl:8092/countries",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='countryList' class='form-control'>";
            html += "<option value='0'>Kies bestemming</option>";
            $.each(allData, function (index, value) {
                html += "<option value='" + value.code + "'> " + value.name + "</option>";
            });
            html += "</select>";
            $("#countryDropdown").html(html);
        }
    });

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/countries",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='countryList2' class='form-control'>";
            html += "<option value='0'>Kies bestemming</option>";
            $.each(allData, function (index, value) {
                html += "<option value='" + value.code + "'> " + value.name + "</option>";
            });
            html += "</select>";
            $("#countryDropdown_2").html(html);
        }
    });

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/countries",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.name.toLowerCase();
                var y = b.name.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='countryList_3' class='form-control'>";
            html += "<option value='0'>Kies bestemming</option>";
            $.each(allData, function (index, value) {
                html += "<option value='" + value.code + "'> " + value.name + "</option>";
            });
            html += "</select>";
            $("#countryDropdown_3").html(html);
        }
    });

    $.ajax({
        url: "http://fox.fireeagle.nl:8092/products",
        success: function (response) {
            var allData = response.data;
            allData.sort(function (a, b) {
                var x = a.description.toLowerCase();
                var y = b.description.toLowerCase();
                return x < y ? -1 : x > y ? 1 : 0;
            });
            var html = "";
            html += "<select id='productList' class='form-control'>";
            html += "<option value='0'>met een fiets</option>";
            $.each(allData, function (index, value) {
                html += "<option value='" + value.code + "'> " + value.description + "</option>";
            });
            html += "</select>";
            $("#productDropDown").html(html);
        }
    });
});


$(".quickBooking").on('click', function () {

    var countryList = $("#countryList").val();
    //alert(countryList);
    var productList = $("#productList").val();
    if (countryList == 0) {
        alert("Select country");
    } else if (productList == 0) {
        alert("Select product");
    } else {
        var list = "";
        for ($i = 1; $i <= 8; $i++) {
            if (productList == "SKU_" + $i) {
                list += "&sku_" + $i + "=1";
            } else
                list += "&sku_" + $i + "=0";
        }

        //alert(list);

        $.ajax({
            url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=0" + list,
            beforeSend: function () {
                $.blockUI();
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                //alert(response);
                var allData = response.data;
                //alert(allData.price_in);
                var price = allData.price_in;

                window.location = 'tussenpagina.php?price=' + price;
            }
        });
    }

});





$(function () {
    $(document.body).on('change', 'select[name="Klant"]', function () {
        sku_values = {};
        $.each($(".col-sm-6 .blockContainer .block"), function (index, item) {
            var item_value = $(item).find('select option:selected').val();
            if (typeof sku_values[item_value] == "undefined") {
                sku_values[item_value] = 1;
            } else {
                sku_values[item_value] = sku_values[item_value] + 1;
            }
        });
        console.log(sku_values);
        //ajax call
        var countryList = 'BE';//$("#countryList").val();
        //alert(countryList);
        var retour = 1;//$("#productList").val();
        var productList = $("#klantDrp").val();
        //alert(productList);

        if (countryList == 0) {
            alert("Select country");
        } else {
            var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                if (i == 0) {
                    queryString = index.toLowerCase() + '=' + item;
                    i++;
                } else {
                    queryString += '&' + index.toLowerCase() + '=' + item;
                }
                //console.log(index);
                //console.log(item);
            })

            //alert(queryString);

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + "&" + queryString,
                async: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    //alert(response);
                    var allData = response.data;
                    //alert(allData.price_in);
                    $(".priceBoxKoffer").html($("<span class='left'>" + allData.products[0].amount + " x Koffer</span><span class='right subPrice'>" + allData.price_in + " €</span>"));
                    //window.location = 'tussenpagina.php?price=' + price;
                }
            });
        }

        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            //alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });

    });

    $(".col-sm-6 #Koffer_3").change(function () {
        var value = $(this).val();

        if (value == 0) {
            $(".priceBoxKoffer").html($(""));
            $(".col-sm-6 .blockContainer").empty();
            $(".finalPrice").html($(""));
        } else {
            //alert(value);
            $(".col-sm-6 .blockContainer").empty();
            for (var i = 0; i < value; i++) {
                var block = $("<div>", {class: "block"});
                $(block).append($(".col-sm-6 .businessSpecifics").html());
                $(".col-sm-6 .blockContainer").append(block);
            }

            sku_values = {};
            $.each($(".col-sm-6 .commonContainer .block"), function (index, item) {
                var item_value = $(item).find('select option:selected').val();
                if (typeof sku_values[item_value] == "undefined") {
                    sku_values[item_value] = 1;
                } else {
                    sku_values[item_value] = sku_values[item_value] + 1;
                }
            });
            console.log(sku_values);
            //$(".col-sm-6 .commonContainer .block:first select option:selected").val()



            //ajax call
            var countryList = 'BE';//$("#countryList").val();
            //alert(countryList);
            var retour = 1;//$("#productList").val();
            var productList = $("#klantDrp").val();
            //alert(productList);

            if (countryList == 0) {
                alert("Select country");
            } else {
                var list = sku_values;
                var i = 0;
                var queryString = '';
                $.each(sku_values, function (index, item) {
                    /*if (i == 0) {
                     queryString = index.toLowerCase() + '=' + item;
                     i++;
                     } else {*/
                    queryString = '&' + index.toLowerCase() + '=' + item;
                    /*}
                     console.log(index);
                     console.log(item);*/
                })

                //alert(list);

                $.ajax({
                    url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + queryString,
                    async: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        //alert(response);
                        var allData = response.data;
                        //alert(allData.price_in);
                        $(".priceBoxKoffer").html($("<span class='left'>" + allData.products[0].amount + " x Koffer</span><span class='right subPrice'>" + allData.price_in + " €</span>"));
                        //window.location = 'tussenpagina.php?price=' + price;
                    }
                });
            }
        }

        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            //alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });



    });

});


$(function () {
    $(document.body).on('change', 'select[name="SnowboardDrp"]', function () {
        sku_values = {};
        $.each($(".col-sm-6 .blockContainer2 .block"), function (index, item) {
            var item_value = $(item).find('select option:selected').val();
            if (typeof sku_values[item_value] == "undefined") {
                sku_values[item_value] = 1;
            } else {
                sku_values[item_value] = sku_values[item_value] + 1;
            }
        });
        console.log(sku_values);
        //ajax call
        var countryList = 'BE';//$("#countryList").val();
        //alert(countryList);
        var retour = 1;//$("#productList").val();
        var productList = $("#klantDrp").val();
        //alert(productList);

        if (countryList == 0) {
            alert("Select country");
        } else {
            var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                if (i == 0) {
                    queryString = index.toLowerCase() + '=' + item;
                    i++;
                } else {
                    queryString += '&' + index.toLowerCase() + '=' + item;
                }
                //console.log(index);
                //console.log(item);
            })

            //alert(queryString);

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + "&" + queryString,
                async: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    //alert(response);
                    var allData = response.data;
                    //alert(allData.price_in);
                    $(".priceBoxSnowboard").html($("<span class='left'>" + allData.products[0].amount + " x Snowboard</span><span class='right subPrice'>" + allData.price_in + " €</span>"));
                    //window.location = 'tussenpagina.php?price=' + price;
                }
            });
        }

        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            //alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });

    });
    
    $(".col-sm-6 #snowboard_3").change(function () {
        var value = $(this).val();
        if (value == 0) {
            $(".priceBoxSnowboard").html($(""));
            $(".col-sm-6 .blockContainer2").empty();
            $(".finalPrice").html($(""));
        } else {
            $(".col-sm-6 .blockContainer2").empty();
            for (var i = 0; i < value; i++) {
                var block = $("<div>", {class: "block"});
                $(block).append($(".col-sm-6 .businessSpecifics2").html());
                $(".col-sm-6 .blockContainer2").append(block);
            }


            sku_values = {};
            $.each($(".col-sm-6 .commonContainer .block"), function (index, item) {
                var item_value = $(item).find('select option:selected').val();
                if (typeof sku_values[item_value] == "undefined") {
                    sku_values[item_value] = 1;
                } else {
                    sku_values[item_value] = sku_values[item_value] + 1;
                }
            });
            console.log(sku_values);
            //$(".col-sm-6 .commonContainer .block:first select option:selected").val()



            //ajax call
            var countryList = 'BE';//$("#countryList").val();
            //alert(countryList);
            var retour = 1;//$("#productList").val();
            var productList = $("#SnowboardDrp").val();
            //alert(productList);

            if (countryList == 0) {
                alert("Select country");
            } else {
                var list = sku_values;
                var i = 0;
                var queryString = '';
                $.each(sku_values, function (index, item) {
                    /*if (i == 0) {
                     queryString = index.toLowerCase() + '=' + item;
                     i++;
                     } else {*/
                    queryString = '&' + index.toLowerCase() + '=' + item;
                    //}
                    //console.log(index);
                    //console.log(item);
                })

                //alert(list);

                $.ajax({
                    url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + queryString,
                    async: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        //alert(response);
                        var allData = response.data;
                        //alert(allData.price_in);
                        $(".priceBoxSnowboard").html($("<span class='left'>" + allData.products[0].amount + " x Snowboard</span><span class='right subPrice'>" + allData.price_in + " €</span>"));
                        //window.location = 'tussenpagina.php?price=' + price;
                    }
                });
            }
        }


        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            //alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });

    });

});


$(function () {
    $(document.body).on('change', 'select[name="GolfFiets"]', function () {
        sku_values = {};
        $.each($(".col-sm-6 .blockContainer3 .block"), function (index, item) {
            var item_value = $(item).find('select option:selected').val();
            if (typeof sku_values[item_value] == "undefined") {
                sku_values[item_value] = 1;
            } else {
                sku_values[item_value] = sku_values[item_value] + 1;
            }
        });
        console.log(sku_values);
        //ajax call
        var countryList = 'BE';//$("#countryList").val();
        //alert(countryList);
        var retour = 1;//$("#productList").val();
        var productList = $("#klantDrp").val();
        //alert(productList);

        if (countryList == 0) {
            alert("Select country");
        } else {
            var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                if (i == 0) {
                    queryString = index.toLowerCase() + '=' + item;
                    i++;
                } else {
                    queryString += '&' + index.toLowerCase() + '=' + item;
                }
                //console.log(index);
                //console.log(item);
            })

            //alert(queryString);

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + "&" + queryString,
                async: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    //alert(response);
                    var allData = response.data;
                    //alert(allData.price_in);
                    $(".priceBoxGolfFiets").html($("<span class='left'>" + allData.products[0].amount + " x GolfFiets</span><span class='right subPrice'>" + allData.price_in + " €</span>"));
                    //window.location = 'tussenpagina.php?price=' + price;
                }
            });
        }

        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            //alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });

    });
    $(".col-sm-6 #GolfFiets_3").change(function () {
        var value = $(this).val();

        if (value == 0) {
            $(".priceBoxGolfFiets").html($(""));
            $(".col-sm-6 .blockContainer3").empty();
            $(".finalPrice").html($(""));
        } else {
            $(".col-sm-6 .blockContainer3").empty();
            for (var i = 0; i < value; i++) {
                var block = $("<div>", {class: "block"});
                $(block).append($(".col-sm-6 .businessSpecifics3").html());
                $(".col-sm-6 .blockContainer3").append(block);
            }

            sku_values = {};
            $.each($(".col-sm-6 .commonContainer .block"), function (index, item) {
                var item_value = $(item).find('select option:selected').val();
                if (typeof sku_values[item_value] == "undefined") {
                    sku_values[item_value] = 1;
                } else {
                    sku_values[item_value] = sku_values[item_value] + 1;
                }
            });
            console.log(sku_values);
            //$(".col-sm-6 .commonContainer .block:first select option:selected").val()



            //ajax call
            var countryList = 'BE';//$("#countryList").val();
            //alert(countryList);
            var retour = 1;//$("#productList").val();
            var productList = $("#SnowboardDrp").val();
            //alert(productList);

            if (countryList == 0) {
                alert("Select country");
            } else {
                var list = sku_values;
                var i = 0;
                var queryString = '';
                $.each(sku_values, function (index, item) {
                    /*if (i == 0) {
                     queryString = index.toLowerCase() + '=' + item;
                     i++;
                     } else {*/
                    queryString = '&' + index.toLowerCase() + '=' + item;
                    //}
                    //console.log(index);
                    //console.log(item);
                })

                //alert(list);

                $.ajax({
                    url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + queryString,
                    async: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        //alert(response);
                        var allData = response.data;
                        //alert(allData.price_in);
                        $(".priceBoxGolfFiets").html($("<span class='left'>" + allData.products[0].amount + " x GolfFiets</span><span class='right subPrice'>" + allData.price_in + " €</span>"));
                        //window.location = 'tussenpagina.php?price=' + price;
                    }
                });
            }
        }

        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            // alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });
    });

});


$(function () {
    $(document.body).on('change', 'select[name="Kitesurf"]', function () {
        sku_values = {};
        $.each($(".col-sm-6 .blockContainer4 .block"), function (index, item) {
            var item_value = $(item).find('select option:selected').val();
            if (typeof sku_values[item_value] == "undefined") {
                sku_values[item_value] = 1;
            } else {
                sku_values[item_value] = sku_values[item_value] + 1;
            }
        });
        console.log(sku_values);
        //ajax call
        var countryList = 'BE';//$("#countryList").val();
        //alert(countryList);
        var retour = 1;//$("#productList").val();
        var productList = $("#klantDrp").val();
        //alert(productList);

        if (countryList == 0) {
            alert("Select country");
        } else {
            var list = sku_values;
            var i = 0;
            var queryString = '';
            $.each(sku_values, function (index, item) {
                if (i == 0) {
                    queryString = index.toLowerCase() + '=' + item;
                    i++;
                } else {
                    queryString += '&' + index.toLowerCase() + '=' + item;
                }
                //console.log(index);
                //console.log(item);
            })

            //alert(queryString);

            $.ajax({
                url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + "&" + queryString,
                async: false,
                beforeSend: function () {
                    $.blockUI();
                },
                complete: function () {
                    $.unblockUI();
                },
                success: function (response) {
                    //alert(response);
                    var allData = response.data;
                    //alert(allData.price_in);
                    $(".priceBoxKitesurf").html($("<span class='left'>" + allData.products[0].amount + " x Kitesurf</span><span class='right subPrice'>" + allData.price_in + " €</span>"));
                    //window.location = 'tussenpagina.php?price=' + price;
                }
            });
        }

        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            //alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });

    });
    $(".col-sm-6 #Kitesurf_3").change(function () {
        var value = $(this).val();
        if (value == 0) {
            $(".priceBoxKitesurf").html($(""));
            $(".col-sm-6 .blockContainer4").empty();
            $(".finalPrice").html($(""));
        } else {
            $(".col-sm-6 .blockContainer4").empty();
            for (var i = 0; i < value; i++) {
                var block = $("<div>", {class: "block"});
                $(block).append($(".col-sm-6 .businessSpecifics4").html());
                $(".col-sm-6 .blockContainer4").append(block);
            }

            sku_values = {};
            $.each($(".col-sm-6 .commonContainer .block"), function (index, item) {
                var item_value = $(item).find('select option:selected').val();
                if (typeof sku_values[item_value] == "undefined") {
                    sku_values[item_value] = 1;
                } else {
                    sku_values[item_value] = sku_values[item_value] + 1;
                }
            });
            console.log(sku_values);
            //$(".col-sm-6 .commonContainer .block:first select option:selected").val()



            //ajax call
            var countryList = 'BE';//$("#countryList").val();
            //alert(countryList);
            var retour = 1;//$("#productList").val();
            var productList = $("#SnowboardDrp").val();
            //alert(productList);

            if (countryList == 0) {
                alert("Select country");
            } else {
                var list = sku_values;
                var i = 0;
                var queryString = '';
                $.each(sku_values, function (index, item) {
                    /*if (i == 0) {
                     queryString = index.toLowerCase() + '=' + item;
                     i++;
                     } else {*/
                    queryString = '&' + index.toLowerCase() + '=' + item;
                    //}
                    //console.log(index);
                    //console.log(item);
                })

                //alert(list);

                $.ajax({
                    url: "http://fox.fireeagle.nl:8092/prices?destination_country=" + countryList + "&retour=" + retour + queryString,
                    async: false,
                    beforeSend: function () {
                        $.blockUI();
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        //alert(response);
                        var allData = response.data;
                        //alert(allData.price_in);
                        $(".priceBoxKitesurf").html($("<span class='left'>" + allData.products[0].amount + " x Kitesurf</span><span class='right subPrice'>" + allData.price_in + " €</span>"));
                        //window.location = 'tussenpagina.php?price=' + price;
                    }
                });
            }
        }

        var totalPoints = 0;
        $(".priceUL li .subPrice").each(function (index) {
            var textVal = $(this).text();
            //alert( parseInt(textVal) );
            //alert(textVal.find('.subPrice').val());
            totalPoints += parseInt(textVal);
            //alert( totalPoints);
            $(".finalPrice").html(totalPoints);
        });
    });

});


var nowTemp = new Date();
      var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
       
      var checkin = $('#dpd1').datepicker({
        format: 'yyyy-mm-dd',  
        onRender: function(date) {
          return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
      })
      .on('changeDate', function(ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
          var newDate = new Date(ev.date)
          newDate.setDate(newDate.getDate() + 1);
          checkout.setValue(newDate);
        }
        checkin.hide();
        $('#dpd2')[0].focus();
      })
      .data('datepicker');
      var checkout = $('#dpd2').datepicker({
        format: 'yyyy-mm-dd',  
        onRender: function(date) {
          return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
      })

      var checkin = $('#dpd3').datepicker({ 
        onRender: function(date) {
          return date.valueOf() < now.valueOf() ? 'disabled' : '';
        },
                format: 'yyyy-mm-dd'
      })
      .on('changeDate', function(ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
          var newDate = new Date(ev.date)
          newDate.setDate(newDate.getDate() + 1);
          checkout.setValue(newDate);
        }
        checkin.hide();
        $('#dpd4')[0].focus();
      })
      .data('datepicker');
      var checkout = $('#dpd4').datepicker({ 
        onRender: function(date) {
          return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        },
                format: 'yyyy-mm-dd'
      })
      
     .on('changeDate', function(ev) {
        checkout.hide();
      }).data('datepicker');
    