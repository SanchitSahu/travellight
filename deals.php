<?php include 'header.php'; ?>

    
    <div class="innerBanner">
      <img src="img/Aanbiedingen.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
					<center><h1>Aanbiedingen <small>Speciale aanbiedingen en acties</small></h1></center>
			</div>
          <div class="col-sm-10 col-sm-offset-1">
            <p class="text-justify txtDrk">Hier bij Travel Light willen wij het allerbeste maken van jouw vakantie; je hebt al een reservering gemaakt bij die super gave accomodatie & eindelijk ga je nu écht naar die bestemming die al jaren op je bucket list staat. Super leuk!  Maar... er is natuurlijk nog het minder leuke gedeelte van je reis: slepen met bagage. </p><p>
Om jouw reis nóg beter & onvergetelijker te maken, verzorgen wij met alle aandacht & liefde het transport van jouw bagage. In dit tijdperk van comfort vinden wij dat bagageloos reizen zeker de norm moet worden. </p><p>
Het transport van je bagage voor de eerste keer overlaten aan een ander, kan best spannend zijn. Uit ervaring weten wij echter dat mensen die eenmaal gebruik hebben gemaakt van Travel Light, niet meer anders willen en ons hun bagagevervoer nogmaals laten verzorgen: als je eenmaal hebt meegemaakt hoe goed het werkt, waarom zou je dan nog moeilijk doen? </p><p>
Om die reden hebben wij regelmatig speciale aanbiedingen & acties voor de mensen die geabonneerd zijn op onze nieuwsbrief & klanten die al eens bij ons hebben geboekt. Meldt je dus aan voor onze nieuwsbrief & hou jezelf daarmee op de hoogte met de beste deals & aanbiedingen! 
 </p>
          

           

          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    
    
  <?php include 'footer.php'; ?>