<?php include 'header.php'; ?>

    
    <div class="innerBanner">
      <img src="img/countryBanner.jpg">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">JOUW KOFFER STAAT AL KLAAR OP JE BESTEMMING</h2>
          
          <div class="col-sm-10 col-sm-offset-1">
            <p class="text-justify txtDrk">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. </p>
          

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                    <img src="img/country1.jpg">
                  </div>
                  <h4>BELGIE VANAF <span>€ 30,-</span></h4>
                </a>
              </div>
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                    <img src="img/country2.jpg">
                  </div>
                  <h4>FRANKRIJK VANAF <span>€ 35,-</span></h4>
                </a>
              </div>
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                    <img src="img/country3.jpg">
                  </div>
                  <h4>DUITSLAND VANAF <span>€ 40,-</span></h4>
                </a>
              </div>
            </div>

          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>