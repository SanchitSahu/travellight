<?php include 'header.php'; ?>

<div id="map" style="width:100%;height:400px"></div>

<section>
    <div class="container">
        <div class="row">
            <div class="page-header">
                <h1 id="contectH1">Contact <small>Heb je vragen of opmerkingen?</small></h1>
			</div>
                    </div>

        <div class="row contactus">

            <div class="col-sm-8">
                <form id="contactForm" action="" method="post">
                    <div class="form-group form-fields has-feedback ">
                        <input type="text" class="form-control" placeholder="Naam" name="naam" id="naam">
                        <span class="glyphicon form-control-feedback" id="naam1"></span> 
                    </div>
                    <div class="form-group form-fields has-feedback ">
                        <input type="text" class="form-control" placeholder="E-mail" name="email" id="email">
                        <span class="glyphicon form-control-feedback" id="email1"></span>
                    </div>
                    <div class="form-group form-fields has-feedback">
                        <input type="text" class="form-control" placeholder="Telefoonnummer">
                    </div>
                    <div class="form-group form-fields has-feedback ">
                        <textarea class="form-control" placeholder="Je vraag" rows="3" name="message" id="message"></textarea>
                        <span class="glyphicon form-control-feedback" id="message1"></span>
                    </div>
                    <div class="form-group form-contact">
                        <input type="submit" value="VERSTUUR" class="btn btn-primary size-default">
                    </div>
                </form>
            </div>

            <div class="col-sm-4">
                <address class="clearfix">
                    <span>TRAVEL-LIGHT HQ</span>
                    <p><i class="fa fa-map-marker" aria-hidden="true"></i><span>Gevers Deynootweg 93 - 3<br/>2586 BK<br/>Den Haag</span></p>
                    <p><i class="fa fa-phone" aria-hidden="true"></i><span>(+31) 085 555 0022</span></p>
                    <p><i class="fa fa-envelope" aria-hidden="true"></i><span><a href="mailto:klantenservice@travel-light.nl">klantenservice@travel-light.nl</a></span></p>
                </address>
            </div>


        </div>
    </div>
</section>







<section>
    <div class="container">
        <div class="row">
            <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
            <div class="col-xs-6 client text-center">
                <img alt="travelLight" src="img/client-1.png">
            </div>
            <div class="col-xs-6 client text-center">
                <img alt="travelLight" src="img/client-2.png">
            </div>
        </div>
    </div>
</section>


<?php include 'footer.php'; ?>