<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/winter/luxemburg.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Luxemburg <small>Geniet van de rust en reis zonder bagage.</small></h1>
			</div>
          
          <blockquote>
        Gemiddelde levertijd: 					3 werkdagen	<br>	

          </blockquote>
    
 <p class="text-justify txtDrk">

	            
	           Dit kleine landje heeft, voor zijn oppervlakte, best een hoop leuke plekken om te zien. De hoofdstad Luxemburg is erg mooi om te zien en er zijn, net zoals in de Belgische Ardennen, een hoop wandelroutes, grotten, kastelen, bossen en mooie wegen om een stuk op te fietsen.  <br></p>

<p>


<p>Heeft je werkgever een meeting geplanned in Luxemburg? Wij versturen je bagage zonder problemen binnen één werkweek naar een bedrijfspand of hotel.  
</p> <br><br>

            <div class="row blocks">
              <div class="col-sm-4 countryImg">
                  <div class="image">
                          <a href="/tussenpagina.php?price=155&countryList=LU&productList=SKU_8&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/bike_small.png"></a>
                  </div>
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=69&countryList=LU&productList=SKU_5&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/koffers_small.png"></a>
                  </div>
              </div>
              <div class="col-sm-4 countryImg">
                  <div class="image">
                    <a href="/tussenpagina.php?price=89&countryList=LU&productList=SKU_4&dpd1Header=Wanneer%20vertrek%20je"><img alt="travellight" src="img/golf_small.png"></a>
                  </div>
              </div>
            </div>

          </div>
        </div>

    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>