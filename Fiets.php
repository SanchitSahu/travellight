<?php include 'header.php'; ?>

    
    <div class="innerBanner">
      <img alt="travellight" src="img/bike.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Fiets <small>De expert op het gebied van het verzenden van fietsen.</small></h1>
			</div>
          
            <div class="col-sm-10 col-sm-offset-1">
                
                <p class="text-justify txtDrk">Travel Light werkt nauw samen met diverse partijen die als verlengstuk kunnen fungeren bij het verzenden van je fiets. Wij zijn ervan overtuigd dat iedere fietser zuinig is op zijn trotse bezit en deze goed wenst te onderhouden en beschermen. De meest optimale bescherming voor je fiets is de fietskoffer of de Fietsdoos. Travel Light is expert op het gebied van het transport en raden onze klanten aan om met onderstaande partners contact op te nemen voor het klaarmaken van transport. Wij werken samen met;</p>
            
                <ul>
                    <li><a href="http://www.mhverhuur.nl/">MH Verhuur - Specialist in fietskoffers</a></li>
                    <li><a href="http://www.giant-bicycles.com/nl-NL/dealers/">Giant Experience Stores</a></li>
                    <li><a href="http://www.wiel-rent.nl/verhuur/fietskoffers/">Fietsenhandel Wiel Rent</a></li>
                    <li><a href="http://www.vakantiefietser.nl/">De Vakantiefietser</a></li>
                    <li><a href="http://www.fietskoffer.nl/fietskoffer-huur/">Fietskoffer.nl</a></li>
                </ul>
                
                <div>Van toerfietsen tot E-bikes, wij versturen ze allemaal. Het totale pakket mag niet boven de 30kg uitkomen. Je dient zelf voor de verpakking te zorgen. In het geval van E-bikes komt het gewicht vaak boven de 30kg uit. Wij adviseren dan ook een koffer bij te boeken waar de batterij in kan naast andere onderdelen en/of kleding.</div>
                <div class="spacer20 clearfix"></div>
                <div class="text-center">
                    <p class="txtDrk"><b>Let op: Fietsen zonder koffer/doos worden NIET meegenomen en geld geen restitutie!</b></p>
                    <p class="txtDrk"><b>Onze voorkeur gaat uit naar het afhalen bij een zakelijk adres zoals fietsenmakers of fietsverenigingen o.i.d.</b></p>
                    <p class="txtDrk"><b>Wij accepteren geen dozen die groter zijn dan 180x95x22 cm en zwaarder wegen dan 30kg</b></p>
                </div>
                
                <div class="row">
                    <div class="col-6 col-6 col-sm-6 vdo">
                        <div class="image">
                            <img alt="travellight" src="img/300x300fietsdoos.png" class="img-responsive">
                        </div>  
                    </div>
                    <div class="col-6 col-6 col-sm-6 vdo">
                        <div class="image">
                            <img alt="travellight" src="img/300x300fietskoffer.png" class="img-responsive">
                        </div>  
                    </div>
                </div>
                <div class="spacer20 clearfix"></div>
                <div>
                  
                
  <strong>Tip: Stop geen losse onderdelen in de fietsdoos of koffer. Deze zullen gaan schudden tijdens het transport en kunnen schade veroorzaken. Voor optimale bescherming kun je het beste een doek om je fiets wikkelen en lege plekken zo veel mogelijk opvullen met zacht materiaal.</strong>

                </div>
                <div>
                    <h4>Transport Partners van Travel Light</h4>
                    <h5>MH Verhuur </h5>
                    <p>Ga jij op reis met je racefiets of mountainbike? Verstuur je geliefde fiets dan veilig met een fietskoffer van MH Verhuur! Deze Premium Travel Light partner is gespecialiseerd in het verhuren van fietskoffers en aanverwanten. Zij weten precies wat belangrijk is bij het versturen van je fiets. Al hun fietskoffers en fietstassen worden geleverd met handig stappenplan voor het inpakken, zodat je stressvrij op reis kan. </p>
                    <p>Reserveer een fietskoffer op <a href="www.mhverhuur.nl">www.mhverhuur.nl</a> en haal hem af op 1 van de 17 <a href="http://mhverhuur.nl/locaties">afhaallocaties</a>. </p>
                
                    <h5>Giant</h5>
                    <p class="text-justify txtDrk">In samenwerking met Giant kun je bij de volgende Experience Store terecht als pick-up & drop-off punt maar ook om advies te krijgen over hoe je het beste je fiets inpakt en een (Giant) doos of koffer kunt huren. </p>
                    <p class="text-justify txtDrk">Momenteel kun je terecht bij voor pick-up en Drop-off, advies, verhuur en verkoop:</p>
                    <p class="text-justify txtDrk">Giant Experience Store: <a href="http://www.gekvanfietsen.nl/home.html">Gek van fietsen</a> in <a href="https://www.google.nl/maps/search/http:%2F%2Fwww.gekvanfietsen.nl%2Fhome.html/@52.134136,4.658255,17z/data=%213m1%214b1">Alphen a/d Rijn</a></p>
                    
                    <h5>Wiel Rent</h5>
                    <p>De fietskoffers van Wiel Rent zijn geschikt voor racefietsen/ATB"s met een framemaat tot en met maat 66 cm. Ideaal om uw fiets veilig te vervoeren in het vliegtuig, trein of auto. Wiel Rent verhuurt ook fietskoffers aan grotere groepen. Fietskoffers worden geleverd met beschermend materiaal en gebruiksaanwijzing. Voor optimaal gebruiksgemak zijn de fietskoffers voorzien van wieltjes en een sleep-handvat. </p>
                    <p>klik op <a href="http://www.wiel-rent.nl/afhaalpunten/">Wiel-Rent.nl</a> om te zien waar je terecht kunt. </p>
                    
                    <h5>De Vakantiefietser</h5>
                    <p>De Vakantiefietser is een <a href="https://www.google.nl/maps/place/De+Vakantiefietser+B.V./@52.378082,4.880917,17z/data=!3m1!4b1!4m2!3m1!1s0x47c609d077a5a637:0xe912f44269513471">Amsterdamse</a> fietsenwinkel gespecialiseerd in vakantiefietsen, fietsaccessoires en uitrusting voor fietsvakanties zoals fietstassen, gidsen, kaarten, kleding, slaapmatjes en nog veel meer. </p>
                    <p>Hier kun je terecht bij voor pick-up en Drop-off, advies, verhuur en verkoop: <a href="http://www.vakantiefietser.nl/">Vakantiefietser</a></p>
                    
                    <h5>Fietskoffer.nl</h5>
                    <p>Wielrenners en mountainbikers willen op vakantie natuurlijk altijd hun eigen racefiets bij zich hebben. Maar hoe bescherm je de fiets het beste? Of je nu met het vliegtuig, in de trein of met de auto gaat, voor de beste bescherming van je fiets verstuur je hem in een fietskoffers. Kijk voor meer informatie op <a href="http://www.fietskoffer.nl/fietskoffer-huur/">Fietskoffer.nl</a>.</p>
                </div>
            </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>