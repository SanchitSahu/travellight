<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/kite.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Kites <small>Verstuur twee kites en je twintip kiteboard.</small></h1>
			</div>
          
          <div class="">
            <p class="text-justify txtDrk">Wij versturen pakketten waar met gemak twee kites en een twintip kiteboard in passen, zolang het totale gewicht niet boven de 30kg uitkomt. De lengte van je board kan niet langer zijn dan 145cm en de breedte niet meer is dan 44cm. Wij doen uiteraard niet moeilijk wanneer je meer wilt meenemen dan puur je materiaal, zolang het totale gewicht onder de 30 kg blijft. </p>
            <p class="text-justify txtDrk"><b>Let op:</b> Wij versturen voorlopig nog geen boards die buiten onze maximale maat vallen. Kiteboards zoals directional-, race- en foilboards vallen in bijna alle gevallen buiten onze maatvoering.</p>
            <p class="text-justify txtDrk">Voor meer informatie en uitleg over onze dienst bekijk onze inpak video's</p>
            
            <div class="text-center">
                <iframe class="iframeClass" width="560" height="315" src="https://www.youtube.com/embed/3GiKkcarpDQ?list=PLuAtrvFtSay3vkfCAEpMIuZCR8KhOfYsr" allowfullscreen></iframe>
            </div>
          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>