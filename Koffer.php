<?php include 'header.php'; ?>

    
    <div class="innerBanner">
        <img alt="travellight" src="img/koffers.png">
    </div>


    <section>
      <div class="container">
        <div class="row">
          <div class="page-header">
              <h1 class="pageH1">Koffer <small>Wij versturen je koffers van groot tot klein.</small></h1>
			</div>
          
          
          
          <div class="col-sm-10 col-sm-offset-1">
                
                <p class="text-justify txtDrk">Wij kennen bij koffers twee maten, namelijk een koffer van 20kg en 30kg. Bij het boeken kun je deze keuze maken. Let op: Momenteel kan je bij ons later niet nog meer kg bijboeken. Nu zijn we niet zo streng als bij de luchtvaart check-in, al wegen we in ons depot wel alles na. In het welkomstpakket leveren we voor zachte koffers en tassen standaard een beschermende koffer-zak mee. </p>
          
                <h3>Inpak instructie koffer-zak</h3>
                <p class="text-justify txtDrk">Tijdens het boeken krijg je automatisch een datum wanneer wij de bagage bij jou thuis komen halen. Zorg er dus voor dat de bagage klaarstaat op de afgesproken datum. De datum wordt bepaald aan de hand van jouw bestemming. Dit kan dus afwijken per land. <br><br>Nadat je de koffer hebt ingepakt zoals je dat normaal ook zou doen en zeker weet dat je er niets meer bij wilt stoppen, trek dan de zak (die je van ons krijgt) over de koffer heen. De zak heeft twee functies: het zorgt ervoor dat onze etiketjes beter blijven plakken en dat jouw koffer extra beschermd is.
In deze beschrijving laten wij je zien hoe je de zak over de koffer dient te plakken. Wij geven je ook een aantal voorbeelden hoe het niet moet. Let op: zet jouw bagage incl. zak klaar voordat onze TNT chauffeur je koffers komt halen. Het is niet de bedoeling dat je dat gezellig gaat doen wanneer onze chauffeur er is.</p>

                <h3>Hoe het dus niet moet</h3>
                <div class="image">
                    <img alt="travellight" src="img/KofferFout.png" class="img-responsive">
                </div>
                <div class="spacer20 clearfix"></div>
                <p class="text-justify txtDrk">De meegeleverde zak moet je strak met tape (niet meegeleverd) om de koffer heen plakken. Tenminste één handvat en eventuele wieltjes maak je vrij zodat je de koffer nog wel kunt rijden of tillen. Wel zo makkelijk voor jou, onze chauffeurs en uiteindelijk je hotel. </p>
                
                <h3>Hoe het dus wel moet</h3>
                <div class="image">
                    <img alt="travellight" src="img/KofferGoed.png" class="img-responsive">
                </div>
                <div class="spacer20 clearfix"></div>
                <p class="text-justify txtDrk">Plak de etiketten en de persoonlijke noot naar de receptie, nadat je de koffer hebt verpakt, op de koffer(zak). Neem de tweede meegeleverde zak, retourdocumenten en resterende tape mee op vakantie voor de terugreis. De zak wordt in principe maar één keer gebruikt en mag bij aankomst worden weggegooid. Gebruik voor de terugreis alsjeblieft de tweede, nieuwe zak en verpak je koffer op dezelfde manier. Retour werkt het zelfde als heen. De retourdocumenten zitten overigens verzameld in deze envelop. </p>
                
                
                
                <div class="text-center">
                    <div class="image">
                        <img alt="travellight" src="img/KofferLabels.png" class="img-responsive">
                    </div>
                    <div class="spacer20 clearfix"></div>
                    <p class="txtDrk"><b>Tot slot.</b> Wat zeggen de wijze mannen over ons?</p>
                    <p class="txtDrk">"He who would travel happily must travel light." </p>
                    <span><b>- Antoine de Saint-Exupéry -</b></span>
                </div>
                
            <!--<div class="row blocks">
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                    <img src="img/country1.jpg">
                  </div>
                  <h4>BELGIE VANAF <span>€ 30,-</span></h4>
                </a>
              </div>
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                    <img src="img/country2.jpg">
                  </div>
                  <h4>FRANKRIJK VANAF <span>€ 35,-</span></h4>
                </a>
              </div>
              <div class="col-sm-4 countryImg">
                <a href="">
                  <div class="image">
                    <img src="img/country3.jpg">
                  </div>
                  <h4>DUITSLAND VANAF <span>€ 40,-</span></h4>
                </a>
              </div>
            </div>-->

          </div>
        </div>

        
      </div>
    </section>


    
    <section class="white">
      <div class="container">
        <div class="row">
          <h2 class="MdTitle">ONZE PARTNERS</h2>
        </div>
        <div class="row">
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-1.png">
          </div>
          <div class="col-xs-6 client text-center">
            <img alt="travellight" src="img/client-2.png">
          </div>
        </div>
      </div>
    </section>




    <?php include 'footer.php'; ?>